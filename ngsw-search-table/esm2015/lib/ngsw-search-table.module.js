/**
 * @fileoverview added by tsickle
 * Generated from: lib/ngsw-search-table.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { NgswSearchTableComponent } from './ngsw-search-table.component';
import { CamelCaseToRegularStringPipe } from './pipe/camel-case-to-regular-string.pipe';
import { GetKeyValueFromObjectPipe } from './pipe/get-key-value-from-object.pipe';
import { SearchFieldDirective } from './directives/search-field.directive';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule, MatButtonModule, MatButtonToggleModule, MatCardModule, MatCheckboxModule, MatDatepickerModule, MatDialogModule, MatIconModule, MatInputModule, MatPaginatorModule, MatRadioModule, MatRippleModule, MatSelectModule, MatSortModule, MatTableModule, MatToolbarModule, MatTooltipModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { NgswSearchTableService } from './ngsw-search-table.service';
export class NgswSearchTableModule {
}
NgswSearchTableModule.decorators = [
    { type: NgModule, args: [{
                declarations: [NgswSearchTableComponent, CamelCaseToRegularStringPipe,
                    GetKeyValueFromObjectPipe,
                    SearchFieldDirective],
                imports: [
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    MatAutocompleteModule,
                    MatButtonModule,
                    MatButtonToggleModule,
                    MatCardModule,
                    MatCheckboxModule,
                    MatDatepickerModule,
                    MatDialogModule,
                    MatIconModule,
                    MatInputModule,
                    MatPaginatorModule,
                    MatRadioModule,
                    MatRippleModule,
                    MatSelectModule,
                    MatSortModule,
                    MatTableModule,
                    MatToolbarModule,
                    MatTooltipModule
                ],
                exports: [NgswSearchTableComponent, CamelCaseToRegularStringPipe, SearchFieldDirective],
                providers: [NgswSearchTableService]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmdzdy1zZWFyY2gtdGFibGUubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmdzdy1zZWFyY2gtdGFibGUvIiwic291cmNlcyI6WyJsaWIvbmdzdy1zZWFyY2gtdGFibGUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUN6RSxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUN4RixPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUNsRixPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUMzRSxPQUFPLEVBQUUsV0FBVyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDbEUsT0FBTyxFQUFFLHFCQUFxQixFQUFFLGVBQWUsRUFBRSxxQkFBcUIsRUFBRSxhQUFhLEVBQUUsaUJBQWlCLEVBQUUsbUJBQW1CLEVBQUUsZUFBZSxFQUFFLGFBQWEsRUFBRSxjQUFjLEVBQWlCLGtCQUFrQixFQUFFLGNBQWMsRUFBRSxlQUFlLEVBQUUsZUFBZSxFQUFFLGFBQWEsRUFBRSxjQUFjLEVBQUUsZ0JBQWdCLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNqVyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFpQ3JFLE1BQU0sT0FBTyxxQkFBcUI7OztZQTdCakMsUUFBUSxTQUFDO2dCQUNSLFlBQVksRUFBRSxDQUFDLHdCQUF3QixFQUFFLDRCQUE0QjtvQkFDbkUseUJBQXlCO29CQUN6QixvQkFBb0IsQ0FBQztnQkFDdkIsT0FBTyxFQUFFO29CQUNQLFlBQVk7b0JBQ1osV0FBVztvQkFDWCxtQkFBbUI7b0JBQ25CLHFCQUFxQjtvQkFDckIsZUFBZTtvQkFDZixxQkFBcUI7b0JBQ3JCLGFBQWE7b0JBQ2IsaUJBQWlCO29CQUNqQixtQkFBbUI7b0JBQ25CLGVBQWU7b0JBQ2YsYUFBYTtvQkFDYixjQUFjO29CQUNkLGtCQUFrQjtvQkFDbEIsY0FBYztvQkFDZCxlQUFlO29CQUNmLGVBQWU7b0JBQ2YsYUFBYTtvQkFDYixjQUFjO29CQUNkLGdCQUFnQjtvQkFDaEIsZ0JBQWdCO2lCQUNqQjtnQkFDRCxPQUFPLEVBQUUsQ0FBQyx3QkFBd0IsRUFBRSw0QkFBNEIsRUFBRSxvQkFBb0IsQ0FBQztnQkFDdkYsU0FBUyxFQUFFLENBQUMsc0JBQXNCLENBQUM7YUFDcEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTmdzd1NlYXJjaFRhYmxlQ29tcG9uZW50IH0gZnJvbSAnLi9uZ3N3LXNlYXJjaC10YWJsZS5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ2FtZWxDYXNlVG9SZWd1bGFyU3RyaW5nUGlwZSB9IGZyb20gJy4vcGlwZS9jYW1lbC1jYXNlLXRvLXJlZ3VsYXItc3RyaW5nLnBpcGUnO1xuaW1wb3J0IHsgR2V0S2V5VmFsdWVGcm9tT2JqZWN0UGlwZSB9IGZyb20gJy4vcGlwZS9nZXQta2V5LXZhbHVlLWZyb20tb2JqZWN0LnBpcGUnO1xuaW1wb3J0IHsgU2VhcmNoRmllbGREaXJlY3RpdmUgfSBmcm9tICcuL2RpcmVjdGl2ZXMvc2VhcmNoLWZpZWxkLmRpcmVjdGl2ZSc7XG5pbXBvcnQgeyBGb3Jtc01vZHVsZSwgUmVhY3RpdmVGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IE1hdEF1dG9jb21wbGV0ZU1vZHVsZSwgTWF0QnV0dG9uTW9kdWxlLCBNYXRCdXR0b25Ub2dnbGVNb2R1bGUsIE1hdENhcmRNb2R1bGUsIE1hdENoZWNrYm94TW9kdWxlLCBNYXREYXRlcGlja2VyTW9kdWxlLCBNYXREaWFsb2dNb2R1bGUsIE1hdEljb25Nb2R1bGUsIE1hdElucHV0TW9kdWxlLCBNYXRMaXN0TW9kdWxlLCBNYXRQYWdpbmF0b3JNb2R1bGUsIE1hdFJhZGlvTW9kdWxlLCBNYXRSaXBwbGVNb2R1bGUsIE1hdFNlbGVjdE1vZHVsZSwgTWF0U29ydE1vZHVsZSwgTWF0VGFibGVNb2R1bGUsIE1hdFRvb2xiYXJNb2R1bGUsIE1hdFRvb2x0aXBNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgTmdzd1NlYXJjaFRhYmxlU2VydmljZSB9IGZyb20gJy4vbmdzdy1zZWFyY2gtdGFibGUuc2VydmljZSc7XG5cblxuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtOZ3N3U2VhcmNoVGFibGVDb21wb25lbnQsIENhbWVsQ2FzZVRvUmVndWxhclN0cmluZ1BpcGUsXG4gICAgR2V0S2V5VmFsdWVGcm9tT2JqZWN0UGlwZSxcbiAgICBTZWFyY2hGaWVsZERpcmVjdGl2ZV0sXG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGUsXG4gICAgRm9ybXNNb2R1bGUsXG4gICAgUmVhY3RpdmVGb3Jtc01vZHVsZSxcbiAgICBNYXRBdXRvY29tcGxldGVNb2R1bGUsXG4gICAgTWF0QnV0dG9uTW9kdWxlLFxuICAgIE1hdEJ1dHRvblRvZ2dsZU1vZHVsZSxcbiAgICBNYXRDYXJkTW9kdWxlLFxuICAgIE1hdENoZWNrYm94TW9kdWxlLFxuICAgIE1hdERhdGVwaWNrZXJNb2R1bGUsXG4gICAgTWF0RGlhbG9nTW9kdWxlLFxuICAgIE1hdEljb25Nb2R1bGUsXG4gICAgTWF0SW5wdXRNb2R1bGUsXG4gICAgTWF0UGFnaW5hdG9yTW9kdWxlLFxuICAgIE1hdFJhZGlvTW9kdWxlLFxuICAgIE1hdFJpcHBsZU1vZHVsZSxcbiAgICBNYXRTZWxlY3RNb2R1bGUsXG4gICAgTWF0U29ydE1vZHVsZSxcbiAgICBNYXRUYWJsZU1vZHVsZSxcbiAgICBNYXRUb29sYmFyTW9kdWxlLFxuICAgIE1hdFRvb2x0aXBNb2R1bGVcbiAgXSxcbiAgZXhwb3J0czogW05nc3dTZWFyY2hUYWJsZUNvbXBvbmVudCwgQ2FtZWxDYXNlVG9SZWd1bGFyU3RyaW5nUGlwZSwgU2VhcmNoRmllbGREaXJlY3RpdmVdLFxuICBwcm92aWRlcnM6IFtOZ3N3U2VhcmNoVGFibGVTZXJ2aWNlXVxufSlcbmV4cG9ydCBjbGFzcyBOZ3N3U2VhcmNoVGFibGVNb2R1bGUgeyB9XG4iXX0=