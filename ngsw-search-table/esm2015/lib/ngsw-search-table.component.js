/**
 * @fileoverview added by tsickle
 * Generated from: lib/ngsw-search-table.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, EventEmitter, Output, ViewChild, ContentChildren, QueryList, Renderer2, ElementRef } from '@angular/core';
import { MatPaginatorIntl, MatDatepicker } from '@angular/material';
import { SearchFieldDirective } from './directives/search-field.directive';
import { FormControl } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
export class ColumnSearch {
    /**
     * @param {?} searchValue
     * @param {?} searchColumnName
     */
    constructor(searchValue, searchColumnName) {
        this.searchValue = searchValue;
        this.searchColumnName = searchColumnName;
    }
}
if (false) {
    /** @type {?} */
    ColumnSearch.prototype.searchValue;
    /** @type {?} */
    ColumnSearch.prototype.searchColumnName;
}
export class NgswSearchTableComponent {
    /**
     * @param {?} httpClient
     * @param {?} matPaginatorIntl
     * @param {?} renderer
     */
    constructor(httpClient, matPaginatorIntl, renderer) {
        this.httpClient = httpClient;
        this.matPaginatorIntl = matPaginatorIntl;
        this.renderer = renderer;
        this.isExcelImport = false;
        this.totalElements = 5;
        this.recordPerPageList = [1, 5, 10, 25, 50, 100];
        this.page = 0;
        this.size = 10;
        this.showAction = true;
        this.retriveValue = ['currencyName', 'firstName', 'hotelName', 'supplierName'];
        this.excelBtnStatus = false;
        this.showAssignButton = false;
        this.showRefuseButton = false;
        this.showGlobelSearch = false;
        this.uploadedFiles = new EventEmitter();
        this.sortBy = new EventEmitter();
        this.pageChange = new EventEmitter();
        this.actionOnTableRecord = new EventEmitter();
        this.excel = new EventEmitter();
        this.searchKey = new EventEmitter();
        this.exportToExcel = new EventEmitter();
        this.columnLengthChanges = new EventEmitter();
        this.isPreviewActionBtn = true;
        this.isAccountActionBtn = false;
        this.isEditActionBtn = false;
        this.checked = false;
        this.sortOrder = { active: '', hover: '', direction: '' };
        this.selectedTableRecordList = (/** @type {?} */ ([]));
        this.searchField = new FormControl('');
        this.filesToUpload = [];
        this.columnFilterMap = new Map();
        // console.log('this.actionButtons', this.actionButtons)
        this.matPaginatorIntl.itemsPerPageLabel = '';
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set searchValue(value) {
        // console.log('value', value, '--->');
        if (!(value instanceof ColumnSearch) || !value.searchColumnName) {
            return;
        }
        this.searchIntoRow(value.searchColumnName, value.searchValue);
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        console.log('changes', changes);
        if (changes.hasOwnProperty('dataTable') && !!this.dataTable && this.dataTable.tableBody['number']) {
            this.page = this.dataTable.tableBody['number'] + 1;
            // console.log('this.page', this.page);
        }
        if (!!this.clickOnTableFields && !!this.dataTable && this.clickOnTableFields.length > 0) {
            this.clickOnTableFields.forEach((/**
             * @param {?} clickableField
             * @return {?}
             */
            clickableField => {
                for (let index = 0; index < this.dataTable.headerRow.length; index++) {
                    if (this.dataTable.headerRow[index].title === clickableField.title) {
                        this.dataTable.headerRow[index].icon = clickableField.icon;
                        this.dataTable.headerRow[index].isClickable = true;
                        this.dataTable.headerRow[index].iconClass = clickableField.iconClass;
                        break;
                    }
                }
            }));
        }
        if (changes.hasOwnProperty('dataTable') && this.dataTable && changes.dataTable.currentValue) {
            // if (!changes.dataTable.previousValue) {
            this.dataTableCopy = JSON.parse(JSON.stringify(this.dataTable));
            console.log('changes this.dataTableCopy', this.dataTableCopy);
            // }
        }
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.actionButtons !== undefined) {
            this.actionButtonsLength = this.actionButtons.length;
        }
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        this.pageChange.emit({ page: this.page, size: this.size });
    }
    /**
     * @return {?}
     */
    ngAfterContentInit() {
        // console.log('searchFieldChildren', this.searchFieldChildren);
        this.searchFieldChildren.map((/**
         * @param {?} res
         * @return {?}
         */
        res => {
            // console.log('res => searchFieldChildren ', res);
            if (res && res.searchFieldRef && res.searchFieldRef.nodeName === 'INPUT') {
                /** @type {?} */
                let simple = this.renderer.listen(res.searchFieldRef, 'keyup', (/**
                 * @param {?} evt
                 * @return {?}
                 */
                (evt) => {
                    // console.log('Clicking the button', evt);
                    this.searchIntoRow(res.searchColumnName, evt.srcElement.value);
                }));
            }
            if (res && res.searchFieldRef && res.searchFieldRef.nodeName === 'SELECT') {
                /** @type {?} */
                let simple = this.renderer.listen(res.searchFieldRef, 'change', (/**
                 * @param {?} evt
                 * @return {?}
                 */
                (evt) => {
                    // console.log('change', evt);
                    this.searchIntoRow(res.searchColumnName, evt.srcElement.value);
                }));
            }
            if (res && res.searchFieldRef && res.searchFieldRef instanceof MatDatepicker) {
                // console.log('MatDatepicker');
                res[(/** @type {?} */ ('searchFieldRef'))].closedStream.subscribe((/**
                 * @param {?} res
                 * @return {?}
                 */
                res => {
                    // console.log('datepicker closedStream', res);
                }));
            }
        }));
    }
    /**
     * @private
     * @return {?}
     */
    initDataTable() {
        return (/** @type {?} */ ({ headerRow: [], tableBody: {} }));
    }
    /**
     * @private
     * @return {?}
     */
    initActionButtons() {
        /** @type {?} */
        let actionBtns = (/** @type {?} */ ([]));
        actionBtns.push((/** @type {?} */ ({
            toolTipText: 'Edit',
            matIcon: 'edit'
        })));
        return actionBtns;
    }
    /**
     * @param {?} a
     * @param {?} b
     * @param {?} isAsc
     * @return {?}
     */
    compare(a, b, isAsc) {
        return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    }
    /**
     * @param {?} hoverField
     * @return {?}
     */
    enter(hoverField) {
        if (hoverField === this.sortOrder.active) {
            return;
        }
        this.sortOrder.hover = hoverField;
    }
    /**
     * @param {?} hoverField
     * @return {?}
     */
    leave(hoverField) {
        this.sortOrder.hover = '';
    }
    /**
     * @param {?} searchText
     * @param {?=} event
     * @return {?}
     */
    searchIntoTable(searchText, event) {
        // console.log(' this.dataTableCopy', this.dataTableCopy);
        // console.log(' this.dataTable',  this.dataTable);
        if (!searchText) {
            this.dataTable.tableBody.content = JSON.parse(JSON.stringify(this.dataTableCopy.tableBody.content));
            return;
        }
        this.dataTable.tableBody.content = this.dataTable.tableBody.content.filter((/**
         * @param {?} category
         * @return {?}
         */
        (category) => {
            // console.log('category', category);
            /** @type {?} */
            let searchResult;
            for (const key in category) {
                if (category.hasOwnProperty(key) && typeof category[key] === 'string') {
                    // console.log('if');
                    /** @type {?} */
                    const element = category[key];
                    // console.log('category[key]', category[key]);
                    searchResult = category[key].toLowerCase().indexOf(searchText.toLowerCase()) > -1;
                    if (searchResult) {
                        break;
                    }
                }
                else {
                    // console.log('else');
                    if (typeof category[key] === 'number') {
                        /** @type {?} */
                        const element = category[key];
                        // console.log('category[key]', category[key]);
                        searchResult = category[key].toString().toLowerCase().indexOf(searchText.toLowerCase()) > -1;
                        if (searchResult) {
                            break;
                        }
                    }
                }
            }
            return searchResult;
        }));
    }
    /**
     * @param {?} searchColumn
     * @param {?} searchText
     * @return {?}
     */
    searchIntoRow(searchColumn, searchText) {
        console.log(' this.dataTableCopy', this.dataTableCopy);
        // console.log('searchText, searchColumn', searchText, searchColumn, event, event.target['value']);
        this.columnFilterMap.set(searchColumn, searchText);
        if (!searchText) {
            this.columnFilterMap.delete(searchColumn);
        }
        if (this.columnFilterMap.size === 0) {
            this.dataTable.tableBody.content = JSON.parse(JSON.stringify(this.dataTableCopy.tableBody.content));
            return;
        }
        // console.log('this.columnFilterMap', this.columnFilterMap, this.columnFilterMap.size);
        this.dataTable.tableBody.content = this.dataTableCopy.tableBody.content.filter((/**
         * @param {?} category
         * @return {?}
         */
        (category) => {
            // console.log('category', category);
            /** @type {?} */
            let validFilterCount = 0;
            for (const key in category) {
                /** @type {?} */
                let searchResult;
                // if (category.hasOwnProperty(key) && key !== searchColumn) {
                //   return;
                // }
                // console.log('key', key);
                if (category.hasOwnProperty(key) && typeof category[key] === 'string') {
                    // console.log('if');
                    /** @type {?} */
                    const element = category[key];
                    // console.log('category[key]', key, category[key]);
                    if (this.columnFilterMap.has(key)) {
                        searchResult = category[key].toLowerCase().indexOf(this.columnFilterMap.get(key).toLowerCase()) > -1;
                        // console.log('searchResult', searchResult);
                    }
                    if (searchResult) {
                        validFilterCount++;
                        // console.log('validFilterCount, this.columnFilterMap.size', validFilterCount, this.columnFilterMap.size);
                        // break;
                    }
                }
                else {
                    // console.log('else');
                    if (typeof category[key] === 'number') {
                        /** @type {?} */
                        const element = category[key];
                        // console.log('category[key]', category[key]);
                        if (this.columnFilterMap.has(key)) {
                            searchResult = category[key].toString().toLowerCase().indexOf(this.columnFilterMap.get(key).toLowerCase()) > -1;
                        }
                        if (searchResult) {
                            validFilterCount++;
                            // break;
                        }
                    }
                }
            }
            return validFilterCount === this.columnFilterMap.size;
        }));
    }
    /**
     * @param {?} orderField
     * @param {?=} startSortOrder
     * @return {?}
     */
    sortClicked(orderField, startSortOrder) {
        // console.log('orderField', orderField);
        startSortOrder.stopPropagation();
        this.sortOrder = (/** @type {?} */ (orderField));
        /** @type {?} */
        const data = this.dataTable.tableBody.content.slice();
        // console.log('data', data);
        if (!orderField.active || orderField.direction === '') {
            this.sortedData = data;
            return;
        }
        this.sortedData = data.sort((/**
         * @param {?} a
         * @param {?} b
         * @return {?}
         */
        (a, b) => {
            /** @type {?} */
            const isAsc = orderField.direction === 'asc';
            return this.compare(a[orderField.active], b[orderField.active], isAsc);
        }));
        // console.log(' this.sortedData', this.sortedData);
        this.dataTable.tableBody.content = this.sortedData;
        // if (orderField === 'DESC' || orderField === 'ASC') {
        //   const paginator: InfoForGetPagination = {
        //     startSortOrder,
        //     orderField,
        //     userId: this.loginUser['user'].userId,
        //     page: this.dataTable.tableBody['number'],
        //     size: this.dataTable.tableBody['size']
        //   } as InfoForGetPagination;
        //   this.sortBy.emit(paginator);
        // }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    emitPageChange(event) {
        // console.log('event', event);
        if (event) {
            /** @type {?} */
            const paginator = (/** @type {?} */ ({
                page: event.pageIndex,
                size: event.pageSize
            }));
            this.dataTableCopy = null;
            this.searchField.reset();
            this.pageChange.emit(paginator);
        }
    }
    /**
     * @param {?} property
     * @param {?} direction
     * @return {?}
     */
    isActiveSortBtn(property, direction) {
        if (this.dataTable.tableBody['sort'][0].direction === direction &&
            this.dataTable.tableBody['sort'][0].property === property) {
            return true;
        }
        return false;
    }
    /**
     * @param {?} record
     * @param {?} btnAction
     * @param {?} recordIndex
     * @return {?}
     */
    emitActionOnTableRecord(record, btnAction, recordIndex) {
        // console.log('emited currency edit record', record);
        if (record) {
            /** @type {?} */
            const data = {
                record,
                btnAction,
                tableName: this.tableTitle,
                recordIndex
            };
            this.actionOnTableRecord.emit(data);
        }
    }
    /**
     * @private
     * @param {?} tableTitle
     * @return {?}
     */
    getDownloadFileType(tableTitle) {
        /** @type {?} */
        const splitTitel = tableTitle.split(' ');
        splitTitel.pop();
        splitTitel[0] = splitTitel[0].toLowerCase();
        return splitTitel.join('');
    }
    /**
     * @param {?} excelTitel
     * @return {?}
     */
    emitDownloadExcel(excelTitel) {
        this.excel.emit({ excelTitel, tableData: this.selectedTableRecordList });
    }
    /**
     * @param {?} event
     * @param {?} key
     * @return {?}
     */
    emitSearchKey(event, key) {
        // console.log('event', event);
        // console.log('key', key);
        if (key && event['keyCode'] === 13) {
            this.searchKey.emit(key);
        }
    }
    /**
     * @param {?} key
     * @param {?=} tableRecord
     * @return {?}
     */
    checkIsDate(key, tableRecord) {
        this.isRecordDate = false;
        switch (key) {
            case 'checkIn':
                this.isRecordDate = true;
                break;
            case 'checkOut':
                this.isRecordDate = true;
                break;
            case 'createdDate':
                this.isRecordDate = true;
                break;
            case 'endDate':
                this.isRecordDate = true;
                break;
            case 'pickUpDate':
                this.isRecordDate = true;
                break;
            case 'startDate':
                this.isRecordDate = true;
                break;
            default:
                this.isRecordDate = false;
                break;
        }
        return this.isRecordDate;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    allTableRecordSelectionChanges(event) {
        if (event.checked) {
            this.selectedTableRecordList = this.dataTable.tableBody.content;
            // console.log('this.selectedTableRecordList', this.selectedTableRecordList);
            return;
        }
        this.selectedTableRecordList = (/** @type {?} */ ([]));
        // console.log('this.selectedTableRecordList', this.selectedTableRecordList);
    }
    /**
     * @param {?} event
     * @param {?} selectedTableRecord
     * @param {?} recordIndex
     * @return {?}
     */
    tableRecordSelectionChanges(event, selectedTableRecord, recordIndex) {
        // console.log('event, selectedTableRecord', event, selectedTableRecord);
        if (event.checked) {
            this.selectedTableRecordList.push(selectedTableRecord);
            // console.log('this.selectedTableRecordList', this.selectedTableRecordList);
            return;
        }
        this.selectedTableRecordList = this.removeUnselectedTableRecord(selectedTableRecord, this.selectedTableRecordList);
        // console.log('deleted List', this.selectedTableRecordList);
    }
    /**
     * @param {?} removableRecord
     * @param {?} originalRecordList
     * @return {?}
     */
    removeUnselectedTableRecord(removableRecord, originalRecordList) {
        return originalRecordList.filter((/**
         * @param {?} existedRecord
         * @return {?}
         */
        (existedRecord) => {
            if (removableRecord && existedRecord) {
                return !(existedRecord['id'] === removableRecord['id']);
            }
            if (!removableRecord) {
                return true;
            }
            return false;
        }));
    }
    /**
     * @param {?} importFileRef
     * @return {?}
     */
    uploadFile(importFileRef) {
        // console.log('importFileRef', importFileRef);
        if (importFileRef) {
            importFileRef.click();
        }
    }
    /**
     * @param {?} files
     * @return {?}
     */
    handleUploadedFile(files) {
        // console.log('files', files);
        for (const key in files) {
            if (files.hasOwnProperty(key) && key !== 'length') {
                this.filesToUpload[0] = files.item(0);
            }
        }
        this.uploadedFiles.emit(this.filesToUpload);
        // console.log('this.filesToUpload', this.filesToUpload);
    }
    /**
     * @return {?}
     */
    downloadExport() {
        // console.log('this.exportLink', this.exportLink);
        this.httpClient.get(this.exportLink, { headers: new HttpHeaders().set('Accept', 'application/vnd.ms-excel') }).subscribe((/**
         * @param {?} res
         * @return {?}
         */
        res => {
            // console.log('res', res);
        }));
    }
    /**
     * @return {?}
     */
    clickExportToExcel() {
        this.exportToExcel.emit(true);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    columnLengthChanged(event) {
        // console.log('columnValueChanges', event);
        this.columnLengthChanges.emit(event.target.value);
    }
}
NgswSearchTableComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngsw-search-table',
                template: "<div class=\"main-content mb-4\">\n  <div class=\"container-fluid\">\n    <div class=\"row\">\n      <div class=\"col-md-12 px-0\">\n        <div class=\"ca rd\">\n          <div class=\"d-flex flex-column flex-md-row justify-content-md-between align-items-md-end table_header\">\n            <div class=\"d-flex align-items-md-center w-100\">\n              <h6 class=\"text-capitalize table_title font_weight_bold mb-0\">{{tableTitle}} :</h6>\n              <div style=\"margin-left: 10px;line-height: 0.5;\">\n                <span style=\"margin-right: 5px\"><input type=\"radio\" name=\"showTableColumn\" value=\"All\"\n                    (change)=\"columnLengthChanged($event)\" checked> All</span>\n                <span style=\"margin-right: 5px\"><input type=\"radio\" name=\"showTableColumn\" value=\"Partial\"\n                    (change)=\"columnLengthChanged($event)\"> Partial</span>\n              </div>\n              <div class=\"table_footer pagination\"\n                *ngIf=\"dataTable && dataTable.tableBody && dataTable.tableBody['content'].length > 0\">\n                <mat-paginator [length]=\"totalElements\" [pageSize]=\"size\" [pageSizeOptions]=\"recordPerPageList\"\n                  (page)=\"emitPageChange($event)\" showFirstLastButtons>\n                </mat-paginator>\n              </div>\n            </div>\n            <div class=\"toolbar\">\n              <!--        Here you can write extra buttons/actions for the toolbar              -->\n              <div class=\"row justify-content-end\">\n                <div class=\"col-sm-12 d-flex flex-column flex-md-row justify-content-md-end align-items-md-center\">\n                  <!-- <div class=\"table_footer\" *ngIf=\"dataTable && dataTable.tableBody && dataTable.tableBody['content'].length > 0\">\n                    <mat-paginator [length]=\"totalElements\" [pageSize]=\"10\" [pageSizeOptions]=\"recordPerPageList\" (page)=\"emitPageChange($event)\">\n                    </mat-paginator>\n                  </div> -->\n                  <div>\n                    <!-- <a *ngIf=\"!!downloadTempleteLink\" [href]=\"downloadTempleteLink\" target=\"_blank\"\n                      rel=\"noopener noreferrer\">Download Template</a>\n                    <span *ngIf=\"!!exportLink\">/</span>\n\n                    <a *ngIf=\"!!exportLink\" [href]=\"exportLink\" target=\"_blank\" rel=\"noopener noreferrer\">Export</a>\n                     <span>/</span>\n                    <a *ngIf=\"isExcelImport\" class=\"href_color\" (click)=\"uploadFile(importFile)\">Import</a>\n                    <input type=\"file\" accept=\".xlsx,.xlsm,.xls,.ods\" style=\"display: none\" #importFile\n                      (change)=\"handleUploadedFile($event.target.files)\"> -->\n                    <div *ngIf=\"isExcelImport\">\n                      <a (click)=\"clickExportToExcel()\" style=\"cursor: pointer;color: #007bff\">Export to Excel</a>\n                    </div>\n                  </div>\n                  <mat-form-field class=\"w-100 pb-1 pb_0 w-md-auto ml-md-2\" style=\"max-width:250px\" appearance=\"outline\"\n                    [style.fontSize.px]=\"10\" *ngIf=\"showGlobelSearch\">\n                    <mat-label>Search</mat-label>\n                    <input #search matInput placeholder=\"Search\" (keyup)=\"searchIntoTable(search.value, $event)\"\n                      [formControl]=\"searchField\">\n                    <mat-icon matSuffix>search</mat-icon>\n                  </mat-form-field>\n                </div>\n                <!-- <div class=\"table_footer\" *ngIf=\"dataTable && dataTable.tableBody && dataTable.tableBody['content'].length > 0\">\n                  <mat-paginator [length]=\"totalElements\" [pageSize]=\"10\" [pageSizeOptions]=\"recordPerPageList\" (page)=\"emitPageChange($event)\">\n                  </mat-paginator>\n                </div> -->\n              </div>\n            </div>\n          </div>\n          <div class=\"table_body\" *ngIf=\"dataTable\">\n            <div class=\"material-datatables table-responsive\">\n              <table id=\"datatables\" class=\"table table-bordered table-hover\" cellspacing=\"0\" width=\"100%\"\n                style=\"width:100%\" *ngIf=\"dataTable.headerRow && dataTable.headerRow.length > 0\">\n                <thead>\n                  <tr>\n                    <th class=\"text-center font-weight-normal\" valign=\"baseline\">Sr.No.</th>\n                    <th class=\"text-center font-weight-normal\" valign=\"baseline\"\n                      *ngFor=\"let headerRow of dataTable.headerRow; let i=index\" (mouseenter)=\"enter(headerRow.title)\"\n                      (mouseleave)=\"leave(headerRow.title)\" [ngClass]=\"{'d-none':headerRow.title ==='id'}\">\n                      <!-- (click)=\"sortClicked({active: headerRow,direction: 'desc'}, $event)\" -->\n                      <div class=\"d-flex justify-content-center\">\n                        <span\n                          class=\"nowrap sort_icon\">{{ headerRow.title | camelCaseToRegularString:tableTitle | getKeyValueFromObject: retriveValue}}</span>\n                        <span style=\"width: 24px;height: 24px;\">\n                          <i class=\"material-icons cursor_pointer\"\n                            *ngIf=\"sortOrder.direction ==='desc' &&  headerRow.title ===sortOrder.active || headerRow.title ===sortOrder.hover\"\n                            (click)=\"sortClicked({active: headerRow.title,direction: 'asc'}, $event)\">\n                            keyboard_arrow_down\n                          </i>\n                          <i class=\"material-icons cursor_pointer\"\n                            *ngIf=\"sortOrder.direction ==='asc' &&  headerRow.title ===sortOrder.active\"\n                            (click)=\"sortClicked({active: headerRow.title,direction: 'desc'}, $event)\">\n                            keyboard_arrow_up\n                          </i>\n                        </span>\n                      </div>\n                    </th>\n                    <!-- ====================================\n                             Action column names\n                    ======================================-->\n\n                    <!-- ================ END ================== -->\n                    <th class=\"disabled-sorting text-center font-weight-normal\" *ngIf=\"showAssignButton\">\n                      Assign\n                    </th>\n                    <th class=\"disabled-sorting text-center font-weight-normal\" *ngIf=\"showRefuseButton\">\n                      Refuse\n                    </th>\n\n                    <th [attr.colspan]=\"actionButtons?.length\" class=\"disabled-sorting text-center font-weight-normal\"\n                      *ngIf=\"actionButtons!== undefined && actionButtons?.length > 0\">\n                      Actions\n                    </th>\n\n\n                    <!-- <th class=\"disabled-sorting text-center\" *ngIf=\"actionButtons.length > 0\" colspan=\"actionButtonsLength\">Actions</th> -->\n                  </tr>\n                </thead>\n                <tbody *ngIf=\"dataTable.tableBody && dataTable.tableBody['content']\">\n                  <tr>\n                    <td></td>\n                    <ng-content select=\"td\" (keyup)=\"searchIntoRow(hr, $event)\"></ng-content>\n                    <!-- <td *ngFor=\"let hr of dataTable.headerRow\" [ngClass]=\"{'d-none':hr.title ==='id'}\" class=\"text-center\">\n                      <input type=\"text\" (keyup)=\"searchIntoRow(hr, $event)\">\n                    </td> -->\n                  </tr>\n                  <tr *ngFor=\"let row of dataTable.tableBody['content']; let recordIndex = index\">\n                    <td class=\"text-left\">{{recordIndex+1}}</td>\n                    <td class=\"text-left\" *ngFor=\"let hr of dataTable.headerRow\"\n                      [ngClass]=\"{'d-none':hr.title ==='id'}\">\n                      <span *ngIf=\"!hr.isClickable\">{{row[hr['title']]}}</span>\n                      <!-- | getKeyValueFromObject: retriveValue  -->\n                      <i *ngIf=\"!!hr.isClickable && !!hr.iconClass && row[hr['title']]!='-'\" class=\"cursor_pointer\"\n                        [ngClass]=\"row[hr['title']]=='Y'? 'active '+hr.iconClass:'in_active '+hr.iconClass\"\n                        (click)=\"emitActionOnTableRecord(row, hr['title'],recordIndex)\">{{hr.icon}}</i>\n                      <i *ngIf=\"!!hr.isClickable && !!hr.iconClass && row[hr['title']]=='-' \"\n                        (click)=\"emitActionOnTableRecord(row, hr['title'],recordIndex)\">{{row[hr['title']]}}</i>\n\n                      <a *ngIf=\"!!hr.isClickable && !hr.iconClass\" style=\"text-decoration: underline\"\n                        class=\"cursor_pointer href_color\"\n                        (click)=\"emitActionOnTableRecord(row, hr['title'],recordIndex)\">{{row[hr['title']]}}</a>\n                      <!-- [ngClass]=\"row[hr['title']]=='Y'? 'active':'in_active'\" -->\n\n                      <!-- <span *ngIf=\"isRecordDate\">{{row[hr] | date: 'd/M/yy':'UTC' }}</span> -->\n                    </td>\n                    <!-- ====================================\n                             Action row data\n                  ======================================-->\n                    <td class=\"td-actions text-left\" *ngIf=\"showAssignButton\">\n                      <a class=\"action_href cursor_pointer\" [ngClass]=\"{'disabledTd':row['assignStatus']==='N'}\"\n                        (click)=\"emitActionOnTableRecord(row, 'assign', recordIndex)\">Assign\n                      </a>\n                    </td>\n                    <td class=\"td-actions text-left\" *ngIf=\"showRefuseButton\">\n                      <a class=\"action_href cursor_pointer\" [ngClass]=\"{'disabledTd':row['assignStatus']==='N'}\"\n                        (click)=\"emitActionOnTableRecord(row, 'refuse', recordIndex)\">Refuse\n                      </a>\n                    </td>\n\n                    <td class=\"td-actions text-left\" *ngFor=\"let btn of actionButtons;let i=index\">\n                      <i *ngIf=\"!!btn.matIcon\" class=\"material-icons cursor_pointer\" [ngClass]=\"btn.matIcon\"\n                        [matTooltip]=\"btn.toolTipText\"\n                        (click)=\"emitActionOnTableRecord(row, btn.title, recordIndex)\">{{btn.matIcon}}</i>\n                      <a *ngIf=\"!btn.matIcon\" class=\"action_href cursor_pointer\" [ngClass]=\"btn.title\"\n                        (click)=\"emitActionOnTableRecord(row, btn.title, recordIndex)\">{{btn.title}}</a>\n\n                    </td>\n                    <!-- ================ END ================== -->\n                  </tr>\n                </tbody>\n              </table>\n            </div>\n          </div>\n          <!-- <div class=\"table_footer\"\n            *ngIf=\"dataTable && dataTable.tableBody && dataTable.tableBody['content'].length > 0\">\n            <mat-paginator [length]=\"totalElements\" [pageSize]=\"10\" [pageSizeOptions]=\"recordPerPageList\"\n              (page)=\"emitPageChange($event)\">\n            </mat-paginator>\n          </div> -->\n          <!-- end content-->\n        </div>\n        <!--  end card  -->\n      </div>\n      <!-- end col-md-12 -->\n    </div>\n    <!-- end row -->\n  </div>\n</div>\n<div *ngIf=\"!dataTable || !dataTable.tableBody || dataTable.tableBody['content'].length <= 0\"\n  class=\"text-center alert alert-info\">\n  Table Content Not Found\n</div>\n<!-- <notifier-container></notifier-container> -->",
                providers: [MatPaginatorIntl],
                styles: [":host ::ng-deep mat-paginator .mat-paginator-range-label{display:none}:host .main-content{font-size:14px}:host .href_color{color:#007bff;text-decoration:none;background-color:transparent}:host .href_color:hover{color:#0056b3;text-decoration:underline;cursor:pointer}:host .page_size{width:85px}:host .page_size ::ng-deep .mat-form-field-wrapper .mat-select-trigger{width:100%}:host .page_size ::ng-deep .mat-form-field-wrapper .mat-select-arrow-wrapper{display:none}:host .material-icons{font-size:16px}:host .active{color:green}:host .in_active{color:red}:host .table_header{display:flex;flex-wrap:wrap}:host .table_header .toolbar{flex:2}.btn_sort{cursor:pointer;max-height:24px}.cursor_pointer{cursor:pointer}.active_sort{background-color:rgba(0,0,0,.16)}.nowrap{white-space:nowrap}.border_all{border:1px solid silver}.material-datatables{border-top-left-radius:.5rem!important}.table{border:1px solid #d5d3d3;background-color:#fff}.table ::ng-deep mat-checkbox label{margin-bottom:0}.table thead tr:first-child th{background-color:#e9e9e9;color:#444;padding:5px .75rem}.table thead th{font-size:14px}.table tbody tr td{padding-top:.5rem;padding-bottom:.5rem;font-size:14px}.table tbody .action_href{color:#007bff;text-decoration:underline}.table .action_btn_wrapper{max-width:60px}.font-weight-normal{font-weight:500!important}.disabledTd{cursor:not-allowed;pointer-events:none;color:gray!important}.pagination{margin-left:auto}"]
            }] }
];
/** @nocollapse */
NgswSearchTableComponent.ctorParameters = () => [
    { type: HttpClient },
    { type: MatPaginatorIntl },
    { type: Renderer2 }
];
NgswSearchTableComponent.propDecorators = {
    downloadTempleteLink: [{ type: Input }],
    exportLink: [{ type: Input }],
    isExcelImport: [{ type: Input }],
    totalElements: [{ type: Input }],
    hideCheckbox: [{ type: Input }],
    clickOnTableFields: [{ type: Input }],
    dataTable: [{ type: Input }],
    recordPerPageList: [{ type: Input }],
    loginUser: [{ type: Input }],
    page: [{ type: Input }],
    showAction: [{ type: Input }],
    actionButtons: [{ type: Input }],
    userRole: [{ type: Input }],
    tableTitle: [{ type: Input }],
    retriveValue: [{ type: Input }],
    reportUrl: [{ type: Input }],
    excelBtnStatus: [{ type: Input, args: ['excelBtnStatus',] }],
    showAssignButton: [{ type: Input }],
    showRefuseButton: [{ type: Input }],
    showGlobelSearch: [{ type: Input }],
    searchValue: [{ type: Input }],
    uploadedFiles: [{ type: Output }],
    sortBy: [{ type: Output }],
    pageChange: [{ type: Output }],
    actionOnTableRecord: [{ type: Output }],
    excel: [{ type: Output }],
    searchKey: [{ type: Output }],
    exportToExcel: [{ type: Output }],
    columnLengthChanges: [{ type: Output }],
    downloadExcel: [{ type: ViewChild, args: ['downloadExcel', { static: false },] }],
    searchFieldChildren: [{ type: ContentChildren, args: [SearchFieldDirective, { descendants: true },] }]
};
if (false) {
    /** @type {?} */
    NgswSearchTableComponent.prototype.downloadTempleteLink;
    /** @type {?} */
    NgswSearchTableComponent.prototype.exportLink;
    /** @type {?} */
    NgswSearchTableComponent.prototype.isExcelImport;
    /** @type {?} */
    NgswSearchTableComponent.prototype.totalElements;
    /** @type {?} */
    NgswSearchTableComponent.prototype.hideCheckbox;
    /** @type {?} */
    NgswSearchTableComponent.prototype.clickOnTableFields;
    /** @type {?} */
    NgswSearchTableComponent.prototype.dataTable;
    /** @type {?} */
    NgswSearchTableComponent.prototype.recordPerPageList;
    /**
     * @type {?}
     * @private
     */
    NgswSearchTableComponent.prototype.dataTableCopy;
    /** @type {?} */
    NgswSearchTableComponent.prototype.loginUser;
    /** @type {?} */
    NgswSearchTableComponent.prototype.page;
    /** @type {?} */
    NgswSearchTableComponent.prototype.size;
    /** @type {?} */
    NgswSearchTableComponent.prototype.showAction;
    /** @type {?} */
    NgswSearchTableComponent.prototype.actionButtons;
    /** @type {?} */
    NgswSearchTableComponent.prototype.userRole;
    /** @type {?} */
    NgswSearchTableComponent.prototype.tableTitle;
    /** @type {?} */
    NgswSearchTableComponent.prototype.retriveValue;
    /** @type {?} */
    NgswSearchTableComponent.prototype.reportUrl;
    /** @type {?} */
    NgswSearchTableComponent.prototype.excelBtnStatus;
    /** @type {?} */
    NgswSearchTableComponent.prototype.showAssignButton;
    /** @type {?} */
    NgswSearchTableComponent.prototype.showRefuseButton;
    /** @type {?} */
    NgswSearchTableComponent.prototype.showGlobelSearch;
    /** @type {?} */
    NgswSearchTableComponent.prototype.uploadedFiles;
    /** @type {?} */
    NgswSearchTableComponent.prototype.sortBy;
    /** @type {?} */
    NgswSearchTableComponent.prototype.pageChange;
    /** @type {?} */
    NgswSearchTableComponent.prototype.actionOnTableRecord;
    /** @type {?} */
    NgswSearchTableComponent.prototype.excel;
    /** @type {?} */
    NgswSearchTableComponent.prototype.searchKey;
    /** @type {?} */
    NgswSearchTableComponent.prototype.exportToExcel;
    /** @type {?} */
    NgswSearchTableComponent.prototype.columnLengthChanges;
    /** @type {?} */
    NgswSearchTableComponent.prototype.downloadExcel;
    /** @type {?} */
    NgswSearchTableComponent.prototype.fileDownloadUrl;
    /** @type {?} */
    NgswSearchTableComponent.prototype.searchFieldChildren;
    /** @type {?} */
    NgswSearchTableComponent.prototype.isPreviewActionBtn;
    /** @type {?} */
    NgswSearchTableComponent.prototype.isAccountActionBtn;
    /** @type {?} */
    NgswSearchTableComponent.prototype.isEditActionBtn;
    /** @type {?} */
    NgswSearchTableComponent.prototype.isRecordDate;
    /** @type {?} */
    NgswSearchTableComponent.prototype.checked;
    /** @type {?} */
    NgswSearchTableComponent.prototype.sortOrder;
    /** @type {?} */
    NgswSearchTableComponent.prototype.selectedTableRecordList;
    /** @type {?} */
    NgswSearchTableComponent.prototype.sortedData;
    /** @type {?} */
    NgswSearchTableComponent.prototype.searchField;
    /** @type {?} */
    NgswSearchTableComponent.prototype.selected;
    /** @type {?} */
    NgswSearchTableComponent.prototype.actionButtonsLength;
    /** @type {?} */
    NgswSearchTableComponent.prototype.filesToUpload;
    /**
     * @type {?}
     * @private
     */
    NgswSearchTableComponent.prototype.columnFilterMap;
    /**
     * @type {?}
     * @private
     */
    NgswSearchTableComponent.prototype.httpClient;
    /**
     * @type {?}
     * @private
     */
    NgswSearchTableComponent.prototype.matPaginatorIntl;
    /**
     * @type {?}
     * @private
     */
    NgswSearchTableComponent.prototype.renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmdzdy1zZWFyY2gtdGFibGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmdzdy1zZWFyY2gtdGFibGUvIiwic291cmNlcyI6WyJsaWIvbmdzdy1zZWFyY2gtdGFibGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBc0QsS0FBSyxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLGVBQWUsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFpQixVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDeE0sT0FBTyxFQUFFLGdCQUFnQixFQUFFLGFBQWEsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBRXBFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQzNFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3QyxPQUFPLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQy9ELE1BQU0sT0FBTyxZQUFZOzs7OztJQUN2QixZQUFtQixXQUFtQixFQUFTLGdCQUF3QjtRQUFwRCxnQkFBVyxHQUFYLFdBQVcsQ0FBUTtRQUFTLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBUTtJQUFJLENBQUM7Q0FDN0U7OztJQURhLG1DQUEwQjs7SUFBRSx3Q0FBK0I7O0FBU3pFLE1BQU0sT0FBTyx3QkFBd0I7Ozs7OztJQWlGbkMsWUFBb0IsVUFBc0IsRUFBVSxnQkFBa0MsRUFBVSxRQUFtQjtRQUEvRixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQVUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUFVLGFBQVEsR0FBUixRQUFRLENBQVc7UUE3RTFHLGtCQUFhLEdBQVksS0FBSyxDQUFDO1FBQy9CLGtCQUFhLEdBQVcsQ0FBQyxDQUFDO1FBUW5DLHNCQUFpQixHQUFrQixDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFLcEQsU0FBSSxHQUFHLENBQUMsQ0FBQztRQUNoQixTQUFJLEdBQUcsRUFBRSxDQUFDO1FBRUgsZUFBVSxHQUFHLElBQUksQ0FBQztRQVFsQixpQkFBWSxHQUFrQixDQUFDLGNBQWMsRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFFLGNBQWMsQ0FBQyxDQUFDO1FBSXpGLG1CQUFjLEdBQUcsS0FBSyxDQUFDO1FBRXZCLHFCQUFnQixHQUFHLEtBQUssQ0FBQztRQUV6QixxQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFDaEIscUJBQWdCLEdBQUcsS0FBSyxDQUFDO1FBVS9CLGtCQUFhLEdBQUcsSUFBSSxZQUFZLEVBQWUsQ0FBQztRQUVuRCxXQUFNLEdBQUcsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUVwQyxlQUFVLEdBQUcsSUFBSSxZQUFZLEVBQXdCLENBQUM7UUFFdEQsd0JBQW1CLEdBQUcsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUVqRCxVQUFLLEdBQUcsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUVuQyxjQUFTLEdBQUcsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUV2QyxrQkFBYSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7UUFDckMsd0JBQW1CLEdBQUcsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQU1wRCx1QkFBa0IsR0FBRyxJQUFJLENBQUM7UUFDMUIsdUJBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQzNCLG9CQUFlLEdBQUcsS0FBSyxDQUFDO1FBRXhCLFlBQU8sR0FBRyxLQUFLLENBQUM7UUFDaEIsY0FBUyxHQUFjLEVBQUUsTUFBTSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsQ0FBQztRQUN2RSw0QkFBdUIsR0FBRyxtQkFBQSxFQUFFLEVBQWlCLENBQUM7UUFFOUMsZ0JBQVcsR0FBRyxJQUFJLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUdsQyxrQkFBYSxHQUFHLEVBQUUsQ0FBQztRQUNYLG9CQUFlLEdBQUcsSUFBSSxHQUFHLEVBQUUsQ0FBQztRQUdsQyx3REFBd0Q7UUFDeEQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGlCQUFpQixHQUFHLEVBQUUsQ0FBQztJQUMvQyxDQUFDOzs7OztJQTdDRCxJQUNJLFdBQVcsQ0FBQyxLQUFtQjtRQUNqQyx1Q0FBdUM7UUFDdkMsSUFBSSxDQUFDLENBQUMsS0FBSyxZQUFZLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixFQUFFO1lBQy9ELE9BQU87U0FDUjtRQUNELElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLGdCQUFnQixFQUFFLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUVoRSxDQUFDOzs7OztJQXNDRCxXQUFXLENBQUMsT0FBc0I7UUFDaEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDaEMsSUFBSSxPQUFPLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ2pHLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ25ELHVDQUF1QztTQUN4QztRQUNELElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUN2RixJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTzs7OztZQUFDLGNBQWMsQ0FBQyxFQUFFO2dCQUMvQyxLQUFLLElBQUksS0FBSyxHQUFHLENBQUMsRUFBRSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFFO29CQUNwRSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssS0FBSyxjQUFjLENBQUMsS0FBSyxFQUFFO3dCQUNsRSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLEdBQUcsY0FBYyxDQUFDLElBQUksQ0FBQzt3QkFDM0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQzt3QkFDbkQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsU0FBUyxHQUFHLGNBQWMsQ0FBQyxTQUFTLENBQUM7d0JBQ3JFLE1BQU07cUJBQ1A7aUJBQ0Y7WUFDSCxDQUFDLEVBQUMsQ0FBQztTQUNKO1FBQ0QsSUFBSSxPQUFPLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksT0FBTyxDQUFDLFNBQVMsQ0FBQyxZQUFZLEVBQUU7WUFDM0YsMENBQTBDO1lBQzFDLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2hFLE9BQU8sQ0FBQyxHQUFHLENBQUMsNEJBQTRCLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBRTlELElBQUk7U0FDTDtJQUNILENBQUM7Ozs7SUFDRCxRQUFRO1FBQ04sSUFBSSxJQUFJLENBQUMsYUFBYSxLQUFLLFNBQVMsRUFBRTtZQUNwQyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7U0FDdEQ7SUFDSCxDQUFDOzs7O0lBRUQsZUFBZTtRQUNiLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQzdELENBQUM7Ozs7SUFDRCxrQkFBa0I7UUFDaEIsZ0VBQWdFO1FBQ2hFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHOzs7O1FBQUMsR0FBRyxDQUFDLEVBQUU7WUFDakMsbURBQW1EO1lBQ25ELElBQUksR0FBRyxJQUFJLEdBQUcsQ0FBQyxjQUFjLElBQUksR0FBRyxDQUFDLGNBQWMsQ0FBQyxRQUFRLEtBQUssT0FBTyxFQUFFOztvQkFDcEUsTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsT0FBTzs7OztnQkFBRSxDQUFDLEdBQUcsRUFBRSxFQUFFO29CQUNyRSwyQ0FBMkM7b0JBQzNDLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLEdBQUcsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ2pFLENBQUMsRUFBQzthQUNIO1lBQ0QsSUFBSSxHQUFHLElBQUksR0FBRyxDQUFDLGNBQWMsSUFBSSxHQUFHLENBQUMsY0FBYyxDQUFDLFFBQVEsS0FBSyxRQUFRLEVBQUU7O29CQUNyRSxNQUFNLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLGNBQWMsRUFBRSxRQUFROzs7O2dCQUFFLENBQUMsR0FBRyxFQUFFLEVBQUU7b0JBQ3RFLDhCQUE4QjtvQkFDOUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsR0FBRyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDakUsQ0FBQyxFQUFDO2FBQ0g7WUFDRCxJQUFJLEdBQUcsSUFBSSxHQUFHLENBQUMsY0FBYyxJQUFJLEdBQUcsQ0FBQyxjQUFjLFlBQVksYUFBYSxFQUFFO2dCQUM1RSxnQ0FBZ0M7Z0JBQ2hDLEdBQUcsQ0FBQyxtQkFBQSxnQkFBZ0IsRUFBTyxDQUFDLENBQUMsWUFBWSxDQUFDLFNBQVM7Ozs7Z0JBQUMsR0FBRyxDQUFDLEVBQUU7b0JBQ3hELCtDQUErQztnQkFDakQsQ0FBQyxFQUFDLENBQUE7YUFDSDtRQUNILENBQUMsRUFBQyxDQUFBO0lBRUosQ0FBQzs7Ozs7SUFDTyxhQUFhO1FBQ25CLE9BQU8sbUJBQUEsRUFBRSxTQUFTLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsRUFBYSxDQUFDO0lBQ3ZELENBQUM7Ozs7O0lBQ08saUJBQWlCOztZQUNuQixVQUFVLEdBQXdCLG1CQUFBLEVBQUUsRUFBdUI7UUFDL0QsVUFBVSxDQUFDLElBQUksQ0FBQyxtQkFBQTtZQUNkLFdBQVcsRUFBRSxNQUFNO1lBQ25CLE9BQU8sRUFBRSxNQUFNO1NBQ2hCLEVBQWdCLENBQUMsQ0FBQztRQUNuQixPQUFPLFVBQVUsQ0FBQztJQUNwQixDQUFDOzs7Ozs7O0lBQ0QsT0FBTyxDQUFDLENBQWtCLEVBQUUsQ0FBa0IsRUFBRSxLQUFjO1FBQzVELE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUM3QyxDQUFDOzs7OztJQUNELEtBQUssQ0FBQyxVQUFrQjtRQUN0QixJQUFJLFVBQVUsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRTtZQUN4QyxPQUFPO1NBQ1I7UUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssR0FBRyxVQUFVLENBQUM7SUFDcEMsQ0FBQzs7Ozs7SUFDRCxLQUFLLENBQUMsVUFBa0I7UUFDdEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO0lBQzVCLENBQUM7Ozs7OztJQUNNLGVBQWUsQ0FBQyxVQUFrQixFQUFFLEtBQVc7UUFDcEQsMERBQTBEO1FBQzFELG1EQUFtRDtRQUNuRCxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ3BHLE9BQU87U0FDUjtRQUNELElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsTUFBTTs7OztRQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7OztnQkFFbEYsWUFBcUI7WUFDekIsS0FBSyxNQUFNLEdBQUcsSUFBSSxRQUFRLEVBQUU7Z0JBQzFCLElBQUksUUFBUSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsSUFBSSxPQUFPLFFBQVEsQ0FBQyxHQUFHLENBQUMsS0FBSyxRQUFRLEVBQUU7OzswQkFFL0QsT0FBTyxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUM7b0JBQzdCLCtDQUErQztvQkFDL0MsWUFBWSxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQ2xGLElBQUksWUFBWSxFQUFFO3dCQUNoQixNQUFNO3FCQUNQO2lCQUNGO3FCQUFNO29CQUNMLHVCQUF1QjtvQkFDdkIsSUFBSSxPQUFPLFFBQVEsQ0FBQyxHQUFHLENBQUMsS0FBSyxRQUFRLEVBQUU7OzhCQUMvQixPQUFPLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQzt3QkFDN0IsK0NBQStDO3dCQUMvQyxZQUFZLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzt3QkFDN0YsSUFBSSxZQUFZLEVBQUU7NEJBQ2hCLE1BQU07eUJBQ1A7cUJBQ0Y7aUJBQ0Y7YUFDRjtZQUNELE9BQU8sWUFBWSxDQUFDO1FBQ3RCLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7O0lBQ00sYUFBYSxDQUFDLFlBQW9CLEVBQUUsVUFBa0I7UUFDM0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDdkQsbUdBQW1HO1FBQ25HLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxVQUFVLENBQUMsQ0FBQztRQUVuRCxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2YsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDM0M7UUFDRCxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxLQUFLLENBQUMsRUFBRTtZQUNuQyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDcEcsT0FBTztTQUNSO1FBQ0Qsd0ZBQXdGO1FBQ3hGLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsTUFBTTs7OztRQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7OztnQkFFdEYsZ0JBQWdCLEdBQVcsQ0FBQztZQUNoQyxLQUFLLE1BQU0sR0FBRyxJQUFJLFFBQVEsRUFBRTs7b0JBQ3RCLFlBQXFCO2dCQUN6Qiw4REFBOEQ7Z0JBQzlELFlBQVk7Z0JBQ1osSUFBSTtnQkFDSiwyQkFBMkI7Z0JBRTNCLElBQUksUUFBUSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsSUFBSSxPQUFPLFFBQVEsQ0FBQyxHQUFHLENBQUMsS0FBSyxRQUFRLEVBQUU7OzswQkFFL0QsT0FBTyxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUM7b0JBQzdCLG9EQUFvRDtvQkFDcEQsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRTt3QkFDakMsWUFBWSxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzt3QkFDckcsNkNBQTZDO3FCQUM5QztvQkFDRCxJQUFJLFlBQVksRUFBRTt3QkFDaEIsZ0JBQWdCLEVBQUUsQ0FBQzt3QkFDbkIsMkdBQTJHO3dCQUMzRyxTQUFTO3FCQUNWO2lCQUNGO3FCQUFNO29CQUNMLHVCQUF1QjtvQkFDdkIsSUFBSSxPQUFPLFFBQVEsQ0FBQyxHQUFHLENBQUMsS0FBSyxRQUFRLEVBQUU7OzhCQUMvQixPQUFPLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQzt3QkFDN0IsK0NBQStDO3dCQUMvQyxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFOzRCQUNqQyxZQUFZLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO3lCQUNqSDt3QkFDRCxJQUFJLFlBQVksRUFBRTs0QkFDaEIsZ0JBQWdCLEVBQUUsQ0FBQzs0QkFDbkIsU0FBUzt5QkFDVjtxQkFDRjtpQkFDRjthQUNGO1lBRUQsT0FBTyxnQkFBZ0IsS0FBSyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQztRQUN4RCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7OztJQUNNLFdBQVcsQ0FBQyxVQUFxQixFQUFFLGNBQWlEO1FBQ3pGLHlDQUF5QztRQUN6QyxjQUFjLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDakMsSUFBSSxDQUFDLFNBQVMsR0FBRyxtQkFBQSxVQUFVLEVBQWEsQ0FBQzs7Y0FFbkMsSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUU7UUFDckQsNkJBQTZCO1FBRTdCLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxJQUFJLFVBQVUsQ0FBQyxTQUFTLEtBQUssRUFBRSxFQUFFO1lBQ3JELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQ3ZCLE9BQU87U0FDUjtRQUNELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLElBQUk7Ozs7O1FBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUU7O2tCQUM3QixLQUFLLEdBQUcsVUFBVSxDQUFDLFNBQVMsS0FBSyxLQUFLO1lBQzVDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDekUsQ0FBQyxFQUFDLENBQUM7UUFDSCxvREFBb0Q7UUFDcEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDbkQsdURBQXVEO1FBQ3ZELDhDQUE4QztRQUM5QyxzQkFBc0I7UUFDdEIsa0JBQWtCO1FBQ2xCLDZDQUE2QztRQUM3QyxnREFBZ0Q7UUFDaEQsNkNBQTZDO1FBQzdDLCtCQUErQjtRQUMvQixpQ0FBaUM7UUFDakMsSUFBSTtJQUNOLENBQUM7Ozs7O0lBQ00sY0FBYyxDQUFDLEtBQStDO1FBQ25FLCtCQUErQjtRQUUvQixJQUFJLEtBQUssRUFBRTs7a0JBQ0gsU0FBUyxHQUF5QixtQkFBQTtnQkFDdEMsSUFBSSxFQUFFLEtBQUssQ0FBQyxTQUFTO2dCQUNyQixJQUFJLEVBQUUsS0FBSyxDQUFDLFFBQVE7YUFDckIsRUFBd0I7WUFDekIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7WUFDMUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUN6QixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUNqQztJQUNILENBQUM7Ozs7OztJQUNNLGVBQWUsQ0FBQyxRQUFhLEVBQUUsU0FBYztRQUNsRCxJQUNFLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsS0FBSyxTQUFTO1lBQzNELElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsS0FBSyxRQUFRLEVBQ3pEO1lBQ0EsT0FBTyxJQUFJLENBQUM7U0FDYjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQzs7Ozs7OztJQUNNLHVCQUF1QixDQUFDLE1BQWMsRUFBRSxTQUFpQixFQUFFLFdBQW1CO1FBRW5GLHNEQUFzRDtRQUN0RCxJQUFJLE1BQU0sRUFBRTs7a0JBQ0osSUFBSSxHQUF3QjtnQkFDaEMsTUFBTTtnQkFDTixTQUFTO2dCQUNULFNBQVMsRUFBRSxJQUFJLENBQUMsVUFBVTtnQkFDMUIsV0FBVzthQUNaO1lBQ0QsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNyQztJQUNILENBQUM7Ozs7OztJQUNPLG1CQUFtQixDQUFDLFVBQWtCOztjQUN0QyxVQUFVLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7UUFDeEMsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQ2pCLFVBQVUsQ0FBQyxDQUFDLENBQUMsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDNUMsT0FBTyxVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQzdCLENBQUM7Ozs7O0lBQ00saUJBQWlCLENBQUMsVUFBa0I7UUFDekMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLElBQUksQ0FBQyx1QkFBdUIsRUFBRSxDQUFDLENBQUM7SUFDM0UsQ0FBQzs7Ozs7O0lBQ00sYUFBYSxDQUFDLEtBQStCLEVBQUUsR0FBVztRQUMvRCwrQkFBK0I7UUFDL0IsMkJBQTJCO1FBQzNCLElBQUksR0FBRyxJQUFJLEtBQUssQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLEVBQUU7WUFDbEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDMUI7SUFDSCxDQUFDOzs7Ozs7SUFDTSxXQUFXLENBQUMsR0FBVyxFQUFFLFdBQW9CO1FBQ2xELElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzFCLFFBQVEsR0FBRyxFQUFFO1lBQ1gsS0FBSyxTQUFTO2dCQUNaLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO2dCQUN6QixNQUFNO1lBQ1IsS0FBSyxVQUFVO2dCQUNiLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO2dCQUN6QixNQUFNO1lBQ1IsS0FBSyxhQUFhO2dCQUNoQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztnQkFDekIsTUFBTTtZQUNSLEtBQUssU0FBUztnQkFDWixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztnQkFDekIsTUFBTTtZQUNSLEtBQUssWUFBWTtnQkFDZixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztnQkFDekIsTUFBTTtZQUNSLEtBQUssV0FBVztnQkFDZCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztnQkFDekIsTUFBTTtZQUNSO2dCQUNFLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO2dCQUMxQixNQUFNO1NBQ1Q7UUFDRCxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDM0IsQ0FBQzs7Ozs7SUFDRCw4QkFBOEIsQ0FBQyxLQUF3QjtRQUNyRCxJQUFJLEtBQUssQ0FBQyxPQUFPLEVBQUU7WUFDakIsSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQztZQUNoRSw2RUFBNkU7WUFDN0UsT0FBTztTQUNSO1FBQ0QsSUFBSSxDQUFDLHVCQUF1QixHQUFHLG1CQUFBLEVBQUUsRUFBaUIsQ0FBQztRQUNuRCw2RUFBNkU7SUFDL0UsQ0FBQzs7Ozs7OztJQUNELDJCQUEyQixDQUFDLEtBQXdCLEVBQUUsbUJBQTJCLEVBQUUsV0FBZ0I7UUFDakcseUVBQXlFO1FBQ3pFLElBQUksS0FBSyxDQUFDLE9BQU8sRUFBRTtZQUNqQixJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7WUFDdkQsNkVBQTZFO1lBQzdFLE9BQU87U0FDUjtRQUNELElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUMsMkJBQTJCLENBQUMsbUJBQW1CLEVBQUUsSUFBSSxDQUFDLHVCQUF1QixDQUFDLENBQUM7UUFDbkgsNkRBQTZEO0lBQy9ELENBQUM7Ozs7OztJQUNELDJCQUEyQixDQUFDLGVBQXVCLEVBQUUsa0JBQWlDO1FBQ3BGLE9BQU8sa0JBQWtCLENBQUMsTUFBTTs7OztRQUFDLENBQUMsYUFBcUIsRUFBRSxFQUFFO1lBQ3pELElBQUksZUFBZSxJQUFJLGFBQWEsRUFBRTtnQkFDcEMsT0FBTyxDQUFDLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2FBQ3pEO1lBQ0QsSUFBSSxDQUFDLGVBQWUsRUFBRTtnQkFDcEIsT0FBTyxJQUFJLENBQUM7YUFDYjtZQUNELE9BQU8sS0FBSyxDQUFDO1FBQ2YsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUNELFVBQVUsQ0FBQyxhQUErQjtRQUN4QywrQ0FBK0M7UUFDL0MsSUFBSSxhQUFhLEVBQUU7WUFDakIsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ3ZCO0lBQ0gsQ0FBQzs7Ozs7SUFDRCxrQkFBa0IsQ0FBQyxLQUFlO1FBQ2hDLCtCQUErQjtRQUMvQixLQUFLLE1BQU0sR0FBRyxJQUFJLEtBQUssRUFBRTtZQUN2QixJQUFJLEtBQUssQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLElBQUksR0FBRyxLQUFLLFFBQVEsRUFBRTtnQkFDakQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ3ZDO1NBQ0Y7UUFDRCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDNUMseURBQXlEO0lBQzNELENBQUM7Ozs7SUFDRCxjQUFjO1FBQ1osbURBQW1EO1FBQ25ELElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxPQUFPLEVBQUUsSUFBSSxXQUFXLEVBQUUsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLDBCQUEwQixDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVM7Ozs7UUFBQyxHQUFHLENBQUMsRUFBRTtZQUM3SCwyQkFBMkI7UUFDN0IsQ0FBQyxFQUFDLENBQUE7SUFDSixDQUFDOzs7O0lBRUQsa0JBQWtCO1FBQ2hCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO0lBQy9CLENBQUM7Ozs7O0lBQ0QsbUJBQW1CLENBQUMsS0FBSztRQUN2Qiw0Q0FBNEM7UUFDNUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3BELENBQUM7OztZQTdhRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLG1CQUFtQjtnQkFDN0IsZzRXQUFpRDtnQkFFakQsU0FBUyxFQUFFLENBQUMsZ0JBQWdCLENBQUM7O2FBQzlCOzs7O1lBVlEsVUFBVTtZQUpWLGdCQUFnQjtZQURtSCxTQUFTOzs7bUNBa0JsSixLQUFLO3lCQUNMLEtBQUs7NEJBQ0wsS0FBSzs0QkFDTCxLQUFLOzJCQUNMLEtBQUs7aUNBRUwsS0FBSzt3QkFFTCxLQUFLO2dDQUVMLEtBQUs7d0JBR0wsS0FBSzttQkFFTCxLQUFLO3lCQUdMLEtBQUs7NEJBRUwsS0FBSzt1QkFFTCxLQUFLO3lCQUVMLEtBQUs7MkJBRUwsS0FBSzt3QkFFTCxLQUFLOzZCQUVMLEtBQUssU0FBQyxnQkFBZ0I7K0JBRXRCLEtBQUs7K0JBRUwsS0FBSzsrQkFFTCxLQUFLOzBCQUNMLEtBQUs7NEJBU0wsTUFBTTtxQkFDTixNQUFNO3lCQUVOLE1BQU07a0NBRU4sTUFBTTtvQkFFTixNQUFNO3dCQUVOLE1BQU07NEJBRU4sTUFBTTtrQ0FFTixNQUFNOzRCQUVOLFNBQVMsU0FBQyxlQUFlLEVBQUUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFO2tDQUU1QyxlQUFlLFNBQUMsb0JBQW9CLEVBQUUsRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFOzs7O0lBL0Q1RCx3REFBc0M7O0lBQ3RDLDhDQUE0Qjs7SUFDNUIsaURBQXdDOztJQUN4QyxpREFBbUM7O0lBQ25DLGdEQUNzQjs7SUFDdEIsc0RBQ3dDOztJQUN4Qyw2Q0FDNEI7O0lBQzVCLHFEQUMyRDs7Ozs7SUFDM0QsaURBQWlDOztJQUNqQyw2Q0FDeUI7O0lBQ3pCLHdDQUNnQjs7SUFDaEIsd0NBQVU7O0lBQ1YsOENBQ3lCOztJQUN6QixpREFDMEM7O0lBQzFDLDRDQUN3Qjs7SUFDeEIsOENBQzBCOztJQUMxQixnREFDZ0c7O0lBQ2hHLDZDQUN5Qjs7SUFDekIsa0RBQzhCOztJQUM5QixvREFDZ0M7O0lBQ2hDLG9EQUNnQzs7SUFDaEMsb0RBQXlDOztJQVV6QyxpREFBMEQ7O0lBQzFELDBDQUMyQzs7SUFDM0MsOENBQzZEOztJQUM3RCx1REFDd0Q7O0lBQ3hELHlDQUMwQzs7SUFDMUMsNkNBQzhDOztJQUM5QyxpREFDK0M7O0lBQy9DLHVEQUEyRDs7SUFFM0QsaURBQWdGOztJQUNoRixtREFBK0I7O0lBQy9CLHVEQUFxSDs7SUFFckgsc0RBQWlDOztJQUNqQyxzREFBa0M7O0lBQ2xDLG1EQUErQjs7SUFDL0IsZ0RBQTZCOztJQUM3QiwyQ0FBdUI7O0lBQ3ZCLDZDQUF1RTs7SUFDdkUsMkRBQThDOztJQUM5Qyw4Q0FBcUI7O0lBQ3JCLCtDQUFrQzs7SUFDbEMsNENBQWM7O0lBQ2QsdURBQTRCOztJQUM1QixpREFBbUI7Ozs7O0lBQ25CLG1EQUFvQzs7Ozs7SUFFeEIsOENBQThCOzs7OztJQUFFLG9EQUEwQzs7Ozs7SUFBRSw0Q0FBMkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCwgQWZ0ZXJDb250ZW50SW5pdCwgT25DaGFuZ2VzLCBJbnB1dCwgRXZlbnRFbWl0dGVyLCBPdXRwdXQsIFZpZXdDaGlsZCwgQ29udGVudENoaWxkcmVuLCBRdWVyeUxpc3QsIFJlbmRlcmVyMiwgU2ltcGxlQ2hhbmdlcywgRWxlbWVudFJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTWF0UGFnaW5hdG9ySW50bCwgTWF0RGF0ZXBpY2tlciB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbmltcG9ydCB7IFRhYmxlSGVhZGluZywgRGF0YVRhYmxlLCBBY3Rpb25CdXR0b24sIFRhYmxlU29ydCwgSW5mb0ZvckdldFBhZ2luYXRpb24sIEFjdGlvbk9uVGFibGVSZWNvcmQsIENvbHVtblNlYXJjaEludGVyZmFjZSB9IGZyb20gJy4vbmdzdy1zZWFyY2gtdGFibGUtZHRvJztcbmltcG9ydCB7IFNlYXJjaEZpZWxkRGlyZWN0aXZlIH0gZnJvbSAnLi9kaXJlY3RpdmVzL3NlYXJjaC1maWVsZC5kaXJlY3RpdmUnO1xuaW1wb3J0IHsgRm9ybUNvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwSGVhZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmV4cG9ydCBjbGFzcyBDb2x1bW5TZWFyY2ggaW1wbGVtZW50cyBDb2x1bW5TZWFyY2hJbnRlcmZhY2Uge1xuICBjb25zdHJ1Y3RvcihwdWJsaWMgc2VhcmNoVmFsdWU6IHN0cmluZywgcHVibGljIHNlYXJjaENvbHVtbk5hbWU6IHN0cmluZykgeyB9XG59XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25nc3ctc2VhcmNoLXRhYmxlJyxcbiAgdGVtcGxhdGVVcmw6ICcuL25nc3ctc2VhcmNoLXRhYmxlLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vbmdzdy1zZWFyY2gtdGFibGUuY29tcG9uZW50LnNjc3MnXSxcbiAgcHJvdmlkZXJzOiBbTWF0UGFnaW5hdG9ySW50bF1cbn0pXG5leHBvcnQgY2xhc3MgTmdzd1NlYXJjaFRhYmxlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBBZnRlclZpZXdJbml0LCBPbkNoYW5nZXMsIEFmdGVyQ29udGVudEluaXQge1xuXG4gIEBJbnB1dCgpIGRvd25sb2FkVGVtcGxldGVMaW5rOiBzdHJpbmc7XG4gIEBJbnB1dCgpIGV4cG9ydExpbms6IHN0cmluZztcbiAgQElucHV0KCkgaXNFeGNlbEltcG9ydDogYm9vbGVhbiA9IGZhbHNlO1xuICBASW5wdXQoKSB0b3RhbEVsZW1lbnRzOiBudW1iZXIgPSA1O1xuICBASW5wdXQoKVxuICBoaWRlQ2hlY2tib3g6IGJvb2xlYW47XG4gIEBJbnB1dCgpXG4gIGNsaWNrT25UYWJsZUZpZWxkczogQXJyYXk8VGFibGVIZWFkaW5nPjtcbiAgQElucHV0KClcbiAgcHVibGljIGRhdGFUYWJsZTogRGF0YVRhYmxlO1xuICBASW5wdXQoKVxuICByZWNvcmRQZXJQYWdlTGlzdDogQXJyYXk8bnVtYmVyPiA9IFsxLCA1LCAxMCwgMjUsIDUwLCAxMDBdO1xuICBwcml2YXRlIGRhdGFUYWJsZUNvcHk6IERhdGFUYWJsZTtcbiAgQElucHV0KClcbiAgcHVibGljIGxvZ2luVXNlcjogb2JqZWN0O1xuICBASW5wdXQoKVxuICBwdWJsaWMgcGFnZSA9IDA7XG4gIHNpemUgPSAxMDtcbiAgQElucHV0KClcbiAgcHVibGljIHNob3dBY3Rpb24gPSB0cnVlO1xuICBASW5wdXQoKVxuICBwdWJsaWMgYWN0aW9uQnV0dG9uczogQXJyYXk8QWN0aW9uQnV0dG9uPjtcbiAgQElucHV0KClcbiAgcHVibGljIHVzZXJSb2xlOiBzdHJpbmc7XG4gIEBJbnB1dCgpXG4gIHB1YmxpYyB0YWJsZVRpdGxlOiBzdHJpbmc7XG4gIEBJbnB1dCgpXG4gIHB1YmxpYyByZXRyaXZlVmFsdWU6IEFycmF5PHN0cmluZz4gPSBbJ2N1cnJlbmN5TmFtZScsICdmaXJzdE5hbWUnLCAnaG90ZWxOYW1lJywgJ3N1cHBsaWVyTmFtZSddO1xuICBASW5wdXQoKVxuICBwdWJsaWMgcmVwb3J0VXJsOiBzdHJpbmc7XG4gIEBJbnB1dCgnZXhjZWxCdG5TdGF0dXMnKVxuICBwdWJsaWMgZXhjZWxCdG5TdGF0dXMgPSBmYWxzZTtcbiAgQElucHV0KClcbiAgcHVibGljIHNob3dBc3NpZ25CdXR0b24gPSBmYWxzZTtcbiAgQElucHV0KClcbiAgcHVibGljIHNob3dSZWZ1c2VCdXR0b24gPSBmYWxzZTtcbiAgQElucHV0KCkgcHVibGljIHNob3dHbG9iZWxTZWFyY2ggPSBmYWxzZTtcbiAgQElucHV0KClcbiAgc2V0IHNlYXJjaFZhbHVlKHZhbHVlOiBDb2x1bW5TZWFyY2gpIHtcbiAgICAvLyBjb25zb2xlLmxvZygndmFsdWUnLCB2YWx1ZSwgJy0tLT4nKTtcbiAgICBpZiAoISh2YWx1ZSBpbnN0YW5jZW9mIENvbHVtblNlYXJjaCkgfHwgIXZhbHVlLnNlYXJjaENvbHVtbk5hbWUpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdGhpcy5zZWFyY2hJbnRvUm93KHZhbHVlLnNlYXJjaENvbHVtbk5hbWUsIHZhbHVlLnNlYXJjaFZhbHVlKTtcblxuICB9XG4gIEBPdXRwdXQoKSB1cGxvYWRlZEZpbGVzID0gbmV3IEV2ZW50RW1pdHRlcjxBcnJheTxGaWxlPj4oKTtcbiAgQE91dHB1dCgpXG4gIHB1YmxpYyBzb3J0QnkgPSBuZXcgRXZlbnRFbWl0dGVyPG9iamVjdD4oKTtcbiAgQE91dHB1dCgpXG4gIHB1YmxpYyBwYWdlQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxJbmZvRm9yR2V0UGFnaW5hdGlvbj4oKTtcbiAgQE91dHB1dCgpXG4gIHB1YmxpYyBhY3Rpb25PblRhYmxlUmVjb3JkID0gbmV3IEV2ZW50RW1pdHRlcjxvYmplY3Q+KCk7XG4gIEBPdXRwdXQoKVxuICBwdWJsaWMgZXhjZWwgPSBuZXcgRXZlbnRFbWl0dGVyPG9iamVjdD4oKTtcbiAgQE91dHB1dCgpXG4gIHB1YmxpYyBzZWFyY2hLZXkgPSBuZXcgRXZlbnRFbWl0dGVyPHN0cmluZz4oKTtcbiAgQE91dHB1dCgpXG4gIHB1YmxpYyBleHBvcnRUb0V4Y2VsID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG4gIEBPdXRwdXQoKSBjb2x1bW5MZW5ndGhDaGFuZ2VzID0gbmV3IEV2ZW50RW1pdHRlcjxzdHJpbmc+KCk7XG5cbiAgQFZpZXdDaGlsZCgnZG93bmxvYWRFeGNlbCcsIHsgc3RhdGljOiBmYWxzZSB9KSBwdWJsaWMgZG93bmxvYWRFeGNlbDogRWxlbWVudFJlZjtcbiAgcHVibGljIGZpbGVEb3dubG9hZFVybDogc3RyaW5nO1xuICBAQ29udGVudENoaWxkcmVuKFNlYXJjaEZpZWxkRGlyZWN0aXZlLCB7IGRlc2NlbmRhbnRzOiB0cnVlIH0pIHNlYXJjaEZpZWxkQ2hpbGRyZW4gITogUXVlcnlMaXN0PFNlYXJjaEZpZWxkRGlyZWN0aXZlPjtcblxuICBwdWJsaWMgaXNQcmV2aWV3QWN0aW9uQnRuID0gdHJ1ZTtcbiAgcHVibGljIGlzQWNjb3VudEFjdGlvbkJ0biA9IGZhbHNlO1xuICBwdWJsaWMgaXNFZGl0QWN0aW9uQnRuID0gZmFsc2U7XG4gIHB1YmxpYyBpc1JlY29yZERhdGU6IGJvb2xlYW47XG4gIHB1YmxpYyBjaGVja2VkID0gZmFsc2U7XG4gIHB1YmxpYyBzb3J0T3JkZXI6IFRhYmxlU29ydCA9IHsgYWN0aXZlOiAnJywgaG92ZXI6ICcnLCBkaXJlY3Rpb246ICcnIH07XG4gIHNlbGVjdGVkVGFibGVSZWNvcmRMaXN0ID0gW10gYXMgQXJyYXk8b2JqZWN0PjtcbiAgc29ydGVkRGF0YTogb2JqZWN0W107XG4gIHNlYXJjaEZpZWxkID0gbmV3IEZvcm1Db250cm9sKCcnKTtcbiAgc2VsZWN0ZWQ6IGFueTtcbiAgYWN0aW9uQnV0dG9uc0xlbmd0aDogbnVtYmVyO1xuICBmaWxlc1RvVXBsb2FkID0gW107XG4gIHByaXZhdGUgY29sdW1uRmlsdGVyTWFwID0gbmV3IE1hcCgpO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cENsaWVudDogSHR0cENsaWVudCwgcHJpdmF0ZSBtYXRQYWdpbmF0b3JJbnRsOiBNYXRQYWdpbmF0b3JJbnRsLCBwcml2YXRlIHJlbmRlcmVyOiBSZW5kZXJlcjIpIHtcbiAgICAvLyBjb25zb2xlLmxvZygndGhpcy5hY3Rpb25CdXR0b25zJywgdGhpcy5hY3Rpb25CdXR0b25zKVxuICAgIHRoaXMubWF0UGFnaW5hdG9ySW50bC5pdGVtc1BlclBhZ2VMYWJlbCA9ICcnO1xuICB9XG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpOiB2b2lkIHtcbiAgICBjb25zb2xlLmxvZygnY2hhbmdlcycsIGNoYW5nZXMpO1xuICAgIGlmIChjaGFuZ2VzLmhhc093blByb3BlcnR5KCdkYXRhVGFibGUnKSAmJiAhIXRoaXMuZGF0YVRhYmxlICYmIHRoaXMuZGF0YVRhYmxlLnRhYmxlQm9keVsnbnVtYmVyJ10pIHtcbiAgICAgIHRoaXMucGFnZSA9IHRoaXMuZGF0YVRhYmxlLnRhYmxlQm9keVsnbnVtYmVyJ10gKyAxO1xuICAgICAgLy8gY29uc29sZS5sb2coJ3RoaXMucGFnZScsIHRoaXMucGFnZSk7XG4gICAgfVxuICAgIGlmICghIXRoaXMuY2xpY2tPblRhYmxlRmllbGRzICYmICEhdGhpcy5kYXRhVGFibGUgJiYgdGhpcy5jbGlja09uVGFibGVGaWVsZHMubGVuZ3RoID4gMCkge1xuICAgICAgdGhpcy5jbGlja09uVGFibGVGaWVsZHMuZm9yRWFjaChjbGlja2FibGVGaWVsZCA9PiB7XG4gICAgICAgIGZvciAobGV0IGluZGV4ID0gMDsgaW5kZXggPCB0aGlzLmRhdGFUYWJsZS5oZWFkZXJSb3cubGVuZ3RoOyBpbmRleCsrKSB7XG4gICAgICAgICAgaWYgKHRoaXMuZGF0YVRhYmxlLmhlYWRlclJvd1tpbmRleF0udGl0bGUgPT09IGNsaWNrYWJsZUZpZWxkLnRpdGxlKSB7XG4gICAgICAgICAgICB0aGlzLmRhdGFUYWJsZS5oZWFkZXJSb3dbaW5kZXhdLmljb24gPSBjbGlja2FibGVGaWVsZC5pY29uO1xuICAgICAgICAgICAgdGhpcy5kYXRhVGFibGUuaGVhZGVyUm93W2luZGV4XS5pc0NsaWNrYWJsZSA9IHRydWU7XG4gICAgICAgICAgICB0aGlzLmRhdGFUYWJsZS5oZWFkZXJSb3dbaW5kZXhdLmljb25DbGFzcyA9IGNsaWNrYWJsZUZpZWxkLmljb25DbGFzcztcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfVxuICAgIGlmIChjaGFuZ2VzLmhhc093blByb3BlcnR5KCdkYXRhVGFibGUnKSAmJiB0aGlzLmRhdGFUYWJsZSAmJiBjaGFuZ2VzLmRhdGFUYWJsZS5jdXJyZW50VmFsdWUpIHtcbiAgICAgIC8vIGlmICghY2hhbmdlcy5kYXRhVGFibGUucHJldmlvdXNWYWx1ZSkge1xuICAgICAgdGhpcy5kYXRhVGFibGVDb3B5ID0gSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeSh0aGlzLmRhdGFUYWJsZSkpO1xuICAgICAgY29uc29sZS5sb2coJ2NoYW5nZXMgdGhpcy5kYXRhVGFibGVDb3B5JywgdGhpcy5kYXRhVGFibGVDb3B5KTtcblxuICAgICAgLy8gfVxuICAgIH1cbiAgfVxuICBuZ09uSW5pdCgpIHtcbiAgICBpZiAodGhpcy5hY3Rpb25CdXR0b25zICE9PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMuYWN0aW9uQnV0dG9uc0xlbmd0aCA9IHRoaXMuYWN0aW9uQnV0dG9ucy5sZW5ndGg7XG4gICAgfVxuICB9XG5cbiAgbmdBZnRlclZpZXdJbml0KCkge1xuICAgIHRoaXMucGFnZUNoYW5nZS5lbWl0KHsgcGFnZTogdGhpcy5wYWdlLCBzaXplOiB0aGlzLnNpemUgfSk7XG4gIH1cbiAgbmdBZnRlckNvbnRlbnRJbml0KCk6IHZvaWQge1xuICAgIC8vIGNvbnNvbGUubG9nKCdzZWFyY2hGaWVsZENoaWxkcmVuJywgdGhpcy5zZWFyY2hGaWVsZENoaWxkcmVuKTtcbiAgICB0aGlzLnNlYXJjaEZpZWxkQ2hpbGRyZW4ubWFwKHJlcyA9PiB7XG4gICAgICAvLyBjb25zb2xlLmxvZygncmVzID0+IHNlYXJjaEZpZWxkQ2hpbGRyZW4gJywgcmVzKTtcbiAgICAgIGlmIChyZXMgJiYgcmVzLnNlYXJjaEZpZWxkUmVmICYmIHJlcy5zZWFyY2hGaWVsZFJlZi5ub2RlTmFtZSA9PT0gJ0lOUFVUJykge1xuICAgICAgICBsZXQgc2ltcGxlID0gdGhpcy5yZW5kZXJlci5saXN0ZW4ocmVzLnNlYXJjaEZpZWxkUmVmLCAna2V5dXAnLCAoZXZ0KSA9PiB7XG4gICAgICAgICAgLy8gY29uc29sZS5sb2coJ0NsaWNraW5nIHRoZSBidXR0b24nLCBldnQpO1xuICAgICAgICAgIHRoaXMuc2VhcmNoSW50b1JvdyhyZXMuc2VhcmNoQ29sdW1uTmFtZSwgZXZ0LnNyY0VsZW1lbnQudmFsdWUpO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICAgIGlmIChyZXMgJiYgcmVzLnNlYXJjaEZpZWxkUmVmICYmIHJlcy5zZWFyY2hGaWVsZFJlZi5ub2RlTmFtZSA9PT0gJ1NFTEVDVCcpIHtcbiAgICAgICAgbGV0IHNpbXBsZSA9IHRoaXMucmVuZGVyZXIubGlzdGVuKHJlcy5zZWFyY2hGaWVsZFJlZiwgJ2NoYW5nZScsIChldnQpID0+IHtcbiAgICAgICAgICAvLyBjb25zb2xlLmxvZygnY2hhbmdlJywgZXZ0KTtcbiAgICAgICAgICB0aGlzLnNlYXJjaEludG9Sb3cocmVzLnNlYXJjaENvbHVtbk5hbWUsIGV2dC5zcmNFbGVtZW50LnZhbHVlKTtcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgICBpZiAocmVzICYmIHJlcy5zZWFyY2hGaWVsZFJlZiAmJiByZXMuc2VhcmNoRmllbGRSZWYgaW5zdGFuY2VvZiBNYXREYXRlcGlja2VyKSB7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKCdNYXREYXRlcGlja2VyJyk7XG4gICAgICAgIHJlc1snc2VhcmNoRmllbGRSZWYnIGFzIGFueV0uY2xvc2VkU3RyZWFtLnN1YnNjcmliZShyZXMgPT4ge1xuICAgICAgICAgIC8vIGNvbnNvbGUubG9nKCdkYXRlcGlja2VyIGNsb3NlZFN0cmVhbScsIHJlcyk7XG4gICAgICAgIH0pXG4gICAgICB9XG4gICAgfSlcblxuICB9XG4gIHByaXZhdGUgaW5pdERhdGFUYWJsZSgpOiBEYXRhVGFibGUge1xuICAgIHJldHVybiB7IGhlYWRlclJvdzogW10sIHRhYmxlQm9keToge30gfSBhcyBEYXRhVGFibGU7XG4gIH1cbiAgcHJpdmF0ZSBpbml0QWN0aW9uQnV0dG9ucygpIHtcbiAgICBsZXQgYWN0aW9uQnRuczogQXJyYXk8QWN0aW9uQnV0dG9uPiA9IFtdIGFzIEFycmF5PEFjdGlvbkJ1dHRvbj47XG4gICAgYWN0aW9uQnRucy5wdXNoKHtcbiAgICAgIHRvb2xUaXBUZXh0OiAnRWRpdCcsXG4gICAgICBtYXRJY29uOiAnZWRpdCdcbiAgICB9IGFzIEFjdGlvbkJ1dHRvbik7XG4gICAgcmV0dXJuIGFjdGlvbkJ0bnM7XG4gIH1cbiAgY29tcGFyZShhOiBudW1iZXIgfCBzdHJpbmcsIGI6IG51bWJlciB8IHN0cmluZywgaXNBc2M6IGJvb2xlYW4pIHtcbiAgICByZXR1cm4gKGEgPCBiID8gLTEgOiAxKSAqIChpc0FzYyA/IDEgOiAtMSk7XG4gIH1cbiAgZW50ZXIoaG92ZXJGaWVsZDogc3RyaW5nKSB7XG4gICAgaWYgKGhvdmVyRmllbGQgPT09IHRoaXMuc29ydE9yZGVyLmFjdGl2ZSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB0aGlzLnNvcnRPcmRlci5ob3ZlciA9IGhvdmVyRmllbGQ7XG4gIH1cbiAgbGVhdmUoaG92ZXJGaWVsZDogc3RyaW5nKSB7XG4gICAgdGhpcy5zb3J0T3JkZXIuaG92ZXIgPSAnJztcbiAgfVxuICBwdWJsaWMgc2VhcmNoSW50b1RhYmxlKHNlYXJjaFRleHQ6IHN0cmluZywgZXZlbnQ/OiBhbnkpIHtcbiAgICAvLyBjb25zb2xlLmxvZygnIHRoaXMuZGF0YVRhYmxlQ29weScsIHRoaXMuZGF0YVRhYmxlQ29weSk7XG4gICAgLy8gY29uc29sZS5sb2coJyB0aGlzLmRhdGFUYWJsZScsICB0aGlzLmRhdGFUYWJsZSk7XG4gICAgaWYgKCFzZWFyY2hUZXh0KSB7XG4gICAgICB0aGlzLmRhdGFUYWJsZS50YWJsZUJvZHkuY29udGVudCA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkodGhpcy5kYXRhVGFibGVDb3B5LnRhYmxlQm9keS5jb250ZW50KSk7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHRoaXMuZGF0YVRhYmxlLnRhYmxlQm9keS5jb250ZW50ID0gdGhpcy5kYXRhVGFibGUudGFibGVCb2R5LmNvbnRlbnQuZmlsdGVyKChjYXRlZ29yeSkgPT4ge1xuICAgICAgLy8gY29uc29sZS5sb2coJ2NhdGVnb3J5JywgY2F0ZWdvcnkpO1xuICAgICAgbGV0IHNlYXJjaFJlc3VsdDogYm9vbGVhbjtcbiAgICAgIGZvciAoY29uc3Qga2V5IGluIGNhdGVnb3J5KSB7XG4gICAgICAgIGlmIChjYXRlZ29yeS5oYXNPd25Qcm9wZXJ0eShrZXkpICYmIHR5cGVvZiBjYXRlZ29yeVtrZXldID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIC8vIGNvbnNvbGUubG9nKCdpZicpO1xuICAgICAgICAgIGNvbnN0IGVsZW1lbnQgPSBjYXRlZ29yeVtrZXldO1xuICAgICAgICAgIC8vIGNvbnNvbGUubG9nKCdjYXRlZ29yeVtrZXldJywgY2F0ZWdvcnlba2V5XSk7XG4gICAgICAgICAgc2VhcmNoUmVzdWx0ID0gY2F0ZWdvcnlba2V5XS50b0xvd2VyQ2FzZSgpLmluZGV4T2Yoc2VhcmNoVGV4dC50b0xvd2VyQ2FzZSgpKSA+IC0xO1xuICAgICAgICAgIGlmIChzZWFyY2hSZXN1bHQpIHtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAvLyBjb25zb2xlLmxvZygnZWxzZScpO1xuICAgICAgICAgIGlmICh0eXBlb2YgY2F0ZWdvcnlba2V5XSA9PT0gJ251bWJlcicpIHtcbiAgICAgICAgICAgIGNvbnN0IGVsZW1lbnQgPSBjYXRlZ29yeVtrZXldO1xuICAgICAgICAgICAgLy8gY29uc29sZS5sb2coJ2NhdGVnb3J5W2tleV0nLCBjYXRlZ29yeVtrZXldKTtcbiAgICAgICAgICAgIHNlYXJjaFJlc3VsdCA9IGNhdGVnb3J5W2tleV0udG9TdHJpbmcoKS50b0xvd2VyQ2FzZSgpLmluZGV4T2Yoc2VhcmNoVGV4dC50b0xvd2VyQ2FzZSgpKSA+IC0xO1xuICAgICAgICAgICAgaWYgKHNlYXJjaFJlc3VsdCkge1xuICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiBzZWFyY2hSZXN1bHQ7XG4gICAgfSk7XG4gIH1cbiAgcHVibGljIHNlYXJjaEludG9Sb3coc2VhcmNoQ29sdW1uOiBzdHJpbmcsIHNlYXJjaFRleHQ6IHN0cmluZykge1xuICAgIGNvbnNvbGUubG9nKCcgdGhpcy5kYXRhVGFibGVDb3B5JywgdGhpcy5kYXRhVGFibGVDb3B5KTtcbiAgICAvLyBjb25zb2xlLmxvZygnc2VhcmNoVGV4dCwgc2VhcmNoQ29sdW1uJywgc2VhcmNoVGV4dCwgc2VhcmNoQ29sdW1uLCBldmVudCwgZXZlbnQudGFyZ2V0Wyd2YWx1ZSddKTtcbiAgICB0aGlzLmNvbHVtbkZpbHRlck1hcC5zZXQoc2VhcmNoQ29sdW1uLCBzZWFyY2hUZXh0KTtcblxuICAgIGlmICghc2VhcmNoVGV4dCkge1xuICAgICAgdGhpcy5jb2x1bW5GaWx0ZXJNYXAuZGVsZXRlKHNlYXJjaENvbHVtbik7XG4gICAgfVxuICAgIGlmICh0aGlzLmNvbHVtbkZpbHRlck1hcC5zaXplID09PSAwKSB7XG4gICAgICB0aGlzLmRhdGFUYWJsZS50YWJsZUJvZHkuY29udGVudCA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkodGhpcy5kYXRhVGFibGVDb3B5LnRhYmxlQm9keS5jb250ZW50KSk7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIC8vIGNvbnNvbGUubG9nKCd0aGlzLmNvbHVtbkZpbHRlck1hcCcsIHRoaXMuY29sdW1uRmlsdGVyTWFwLCB0aGlzLmNvbHVtbkZpbHRlck1hcC5zaXplKTtcbiAgICB0aGlzLmRhdGFUYWJsZS50YWJsZUJvZHkuY29udGVudCA9IHRoaXMuZGF0YVRhYmxlQ29weS50YWJsZUJvZHkuY29udGVudC5maWx0ZXIoKGNhdGVnb3J5KSA9PiB7XG4gICAgICAvLyBjb25zb2xlLmxvZygnY2F0ZWdvcnknLCBjYXRlZ29yeSk7XG4gICAgICBsZXQgdmFsaWRGaWx0ZXJDb3VudDogbnVtYmVyID0gMDtcbiAgICAgIGZvciAoY29uc3Qga2V5IGluIGNhdGVnb3J5KSB7XG4gICAgICAgIGxldCBzZWFyY2hSZXN1bHQ6IGJvb2xlYW47XG4gICAgICAgIC8vIGlmIChjYXRlZ29yeS5oYXNPd25Qcm9wZXJ0eShrZXkpICYmIGtleSAhPT0gc2VhcmNoQ29sdW1uKSB7XG4gICAgICAgIC8vICAgcmV0dXJuO1xuICAgICAgICAvLyB9XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKCdrZXknLCBrZXkpO1xuXG4gICAgICAgIGlmIChjYXRlZ29yeS5oYXNPd25Qcm9wZXJ0eShrZXkpICYmIHR5cGVvZiBjYXRlZ29yeVtrZXldID09PSAnc3RyaW5nJykge1xuICAgICAgICAgIC8vIGNvbnNvbGUubG9nKCdpZicpO1xuICAgICAgICAgIGNvbnN0IGVsZW1lbnQgPSBjYXRlZ29yeVtrZXldO1xuICAgICAgICAgIC8vIGNvbnNvbGUubG9nKCdjYXRlZ29yeVtrZXldJywga2V5LCBjYXRlZ29yeVtrZXldKTtcbiAgICAgICAgICBpZiAodGhpcy5jb2x1bW5GaWx0ZXJNYXAuaGFzKGtleSkpIHtcbiAgICAgICAgICAgIHNlYXJjaFJlc3VsdCA9IGNhdGVnb3J5W2tleV0udG9Mb3dlckNhc2UoKS5pbmRleE9mKHRoaXMuY29sdW1uRmlsdGVyTWFwLmdldChrZXkpLnRvTG93ZXJDYXNlKCkpID4gLTE7XG4gICAgICAgICAgICAvLyBjb25zb2xlLmxvZygnc2VhcmNoUmVzdWx0Jywgc2VhcmNoUmVzdWx0KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKHNlYXJjaFJlc3VsdCkge1xuICAgICAgICAgICAgdmFsaWRGaWx0ZXJDb3VudCsrO1xuICAgICAgICAgICAgLy8gY29uc29sZS5sb2coJ3ZhbGlkRmlsdGVyQ291bnQsIHRoaXMuY29sdW1uRmlsdGVyTWFwLnNpemUnLCB2YWxpZEZpbHRlckNvdW50LCB0aGlzLmNvbHVtbkZpbHRlck1hcC5zaXplKTtcbiAgICAgICAgICAgIC8vIGJyZWFrO1xuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAvLyBjb25zb2xlLmxvZygnZWxzZScpO1xuICAgICAgICAgIGlmICh0eXBlb2YgY2F0ZWdvcnlba2V5XSA9PT0gJ251bWJlcicpIHtcbiAgICAgICAgICAgIGNvbnN0IGVsZW1lbnQgPSBjYXRlZ29yeVtrZXldO1xuICAgICAgICAgICAgLy8gY29uc29sZS5sb2coJ2NhdGVnb3J5W2tleV0nLCBjYXRlZ29yeVtrZXldKTtcbiAgICAgICAgICAgIGlmICh0aGlzLmNvbHVtbkZpbHRlck1hcC5oYXMoa2V5KSkge1xuICAgICAgICAgICAgICBzZWFyY2hSZXN1bHQgPSBjYXRlZ29yeVtrZXldLnRvU3RyaW5nKCkudG9Mb3dlckNhc2UoKS5pbmRleE9mKHRoaXMuY29sdW1uRmlsdGVyTWFwLmdldChrZXkpLnRvTG93ZXJDYXNlKCkpID4gLTE7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoc2VhcmNoUmVzdWx0KSB7XG4gICAgICAgICAgICAgIHZhbGlkRmlsdGVyQ291bnQrKztcbiAgICAgICAgICAgICAgLy8gYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB2YWxpZEZpbHRlckNvdW50ID09PSB0aGlzLmNvbHVtbkZpbHRlck1hcC5zaXplO1xuICAgIH0pO1xuICB9XG4gIHB1YmxpYyBzb3J0Q2xpY2tlZChvcmRlckZpZWxkOiBUYWJsZVNvcnQsIHN0YXJ0U29ydE9yZGVyPzogeyBzdG9wUHJvcGFnYXRpb246ICgpID0+IHZvaWQ7IH0pIHtcbiAgICAvLyBjb25zb2xlLmxvZygnb3JkZXJGaWVsZCcsIG9yZGVyRmllbGQpO1xuICAgIHN0YXJ0U29ydE9yZGVyLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgIHRoaXMuc29ydE9yZGVyID0gb3JkZXJGaWVsZCBhcyBUYWJsZVNvcnQ7XG5cbiAgICBjb25zdCBkYXRhID0gdGhpcy5kYXRhVGFibGUudGFibGVCb2R5LmNvbnRlbnQuc2xpY2UoKTtcbiAgICAvLyBjb25zb2xlLmxvZygnZGF0YScsIGRhdGEpO1xuXG4gICAgaWYgKCFvcmRlckZpZWxkLmFjdGl2ZSB8fCBvcmRlckZpZWxkLmRpcmVjdGlvbiA9PT0gJycpIHtcbiAgICAgIHRoaXMuc29ydGVkRGF0YSA9IGRhdGE7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHRoaXMuc29ydGVkRGF0YSA9IGRhdGEuc29ydCgoYSwgYikgPT4ge1xuICAgICAgY29uc3QgaXNBc2MgPSBvcmRlckZpZWxkLmRpcmVjdGlvbiA9PT0gJ2FzYyc7XG4gICAgICByZXR1cm4gdGhpcy5jb21wYXJlKGFbb3JkZXJGaWVsZC5hY3RpdmVdLCBiW29yZGVyRmllbGQuYWN0aXZlXSwgaXNBc2MpO1xuICAgIH0pO1xuICAgIC8vIGNvbnNvbGUubG9nKCcgdGhpcy5zb3J0ZWREYXRhJywgdGhpcy5zb3J0ZWREYXRhKTtcbiAgICB0aGlzLmRhdGFUYWJsZS50YWJsZUJvZHkuY29udGVudCA9IHRoaXMuc29ydGVkRGF0YTtcbiAgICAvLyBpZiAob3JkZXJGaWVsZCA9PT0gJ0RFU0MnIHx8IG9yZGVyRmllbGQgPT09ICdBU0MnKSB7XG4gICAgLy8gICBjb25zdCBwYWdpbmF0b3I6IEluZm9Gb3JHZXRQYWdpbmF0aW9uID0ge1xuICAgIC8vICAgICBzdGFydFNvcnRPcmRlcixcbiAgICAvLyAgICAgb3JkZXJGaWVsZCxcbiAgICAvLyAgICAgdXNlcklkOiB0aGlzLmxvZ2luVXNlclsndXNlciddLnVzZXJJZCxcbiAgICAvLyAgICAgcGFnZTogdGhpcy5kYXRhVGFibGUudGFibGVCb2R5WydudW1iZXInXSxcbiAgICAvLyAgICAgc2l6ZTogdGhpcy5kYXRhVGFibGUudGFibGVCb2R5WydzaXplJ11cbiAgICAvLyAgIH0gYXMgSW5mb0ZvckdldFBhZ2luYXRpb247XG4gICAgLy8gICB0aGlzLnNvcnRCeS5lbWl0KHBhZ2luYXRvcik7XG4gICAgLy8gfVxuICB9XG4gIHB1YmxpYyBlbWl0UGFnZUNoYW5nZShldmVudDogeyBwYWdlSW5kZXg6IG51bWJlcjsgcGFnZVNpemU6IG51bWJlcjsgfSkge1xuICAgIC8vIGNvbnNvbGUubG9nKCdldmVudCcsIGV2ZW50KTtcblxuICAgIGlmIChldmVudCkge1xuICAgICAgY29uc3QgcGFnaW5hdG9yOiBJbmZvRm9yR2V0UGFnaW5hdGlvbiA9IHtcbiAgICAgICAgcGFnZTogZXZlbnQucGFnZUluZGV4LFxuICAgICAgICBzaXplOiBldmVudC5wYWdlU2l6ZVxuICAgICAgfSBhcyBJbmZvRm9yR2V0UGFnaW5hdGlvbjtcbiAgICAgIHRoaXMuZGF0YVRhYmxlQ29weSA9IG51bGw7XG4gICAgICB0aGlzLnNlYXJjaEZpZWxkLnJlc2V0KCk7XG4gICAgICB0aGlzLnBhZ2VDaGFuZ2UuZW1pdChwYWdpbmF0b3IpO1xuICAgIH1cbiAgfVxuICBwdWJsaWMgaXNBY3RpdmVTb3J0QnRuKHByb3BlcnR5OiBhbnksIGRpcmVjdGlvbjogYW55KSB7XG4gICAgaWYgKFxuICAgICAgdGhpcy5kYXRhVGFibGUudGFibGVCb2R5Wydzb3J0J11bMF0uZGlyZWN0aW9uID09PSBkaXJlY3Rpb24gJiZcbiAgICAgIHRoaXMuZGF0YVRhYmxlLnRhYmxlQm9keVsnc29ydCddWzBdLnByb3BlcnR5ID09PSBwcm9wZXJ0eVxuICAgICkge1xuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuICBwdWJsaWMgZW1pdEFjdGlvbk9uVGFibGVSZWNvcmQocmVjb3JkOiBvYmplY3QsIGJ0bkFjdGlvbjogc3RyaW5nLCByZWNvcmRJbmRleDogbnVtYmVyXG4gICkge1xuICAgIC8vIGNvbnNvbGUubG9nKCdlbWl0ZWQgY3VycmVuY3kgZWRpdCByZWNvcmQnLCByZWNvcmQpO1xuICAgIGlmIChyZWNvcmQpIHtcbiAgICAgIGNvbnN0IGRhdGE6IEFjdGlvbk9uVGFibGVSZWNvcmQgPSB7XG4gICAgICAgIHJlY29yZCxcbiAgICAgICAgYnRuQWN0aW9uLFxuICAgICAgICB0YWJsZU5hbWU6IHRoaXMudGFibGVUaXRsZSxcbiAgICAgICAgcmVjb3JkSW5kZXhcbiAgICAgIH07XG4gICAgICB0aGlzLmFjdGlvbk9uVGFibGVSZWNvcmQuZW1pdChkYXRhKTtcbiAgICB9XG4gIH1cbiAgcHJpdmF0ZSBnZXREb3dubG9hZEZpbGVUeXBlKHRhYmxlVGl0bGU6IHN0cmluZykge1xuICAgIGNvbnN0IHNwbGl0VGl0ZWwgPSB0YWJsZVRpdGxlLnNwbGl0KCcgJyk7XG4gICAgc3BsaXRUaXRlbC5wb3AoKTtcbiAgICBzcGxpdFRpdGVsWzBdID0gc3BsaXRUaXRlbFswXS50b0xvd2VyQ2FzZSgpO1xuICAgIHJldHVybiBzcGxpdFRpdGVsLmpvaW4oJycpO1xuICB9XG4gIHB1YmxpYyBlbWl0RG93bmxvYWRFeGNlbChleGNlbFRpdGVsOiBzdHJpbmcpIHtcbiAgICB0aGlzLmV4Y2VsLmVtaXQoeyBleGNlbFRpdGVsLCB0YWJsZURhdGE6IHRoaXMuc2VsZWN0ZWRUYWJsZVJlY29yZExpc3QgfSk7XG4gIH1cbiAgcHVibGljIGVtaXRTZWFyY2hLZXkoZXZlbnQ6IHsgW3g6IHN0cmluZ106IG51bWJlcjsgfSwga2V5OiBzdHJpbmcpIHtcbiAgICAvLyBjb25zb2xlLmxvZygnZXZlbnQnLCBldmVudCk7XG4gICAgLy8gY29uc29sZS5sb2coJ2tleScsIGtleSk7XG4gICAgaWYgKGtleSAmJiBldmVudFsna2V5Q29kZSddID09PSAxMykge1xuICAgICAgdGhpcy5zZWFyY2hLZXkuZW1pdChrZXkpO1xuICAgIH1cbiAgfVxuICBwdWJsaWMgY2hlY2tJc0RhdGUoa2V5OiBzdHJpbmcsIHRhYmxlUmVjb3JkPzogb2JqZWN0KSB7XG4gICAgdGhpcy5pc1JlY29yZERhdGUgPSBmYWxzZTtcbiAgICBzd2l0Y2ggKGtleSkge1xuICAgICAgY2FzZSAnY2hlY2tJbic6XG4gICAgICAgIHRoaXMuaXNSZWNvcmREYXRlID0gdHJ1ZTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlICdjaGVja091dCc6XG4gICAgICAgIHRoaXMuaXNSZWNvcmREYXRlID0gdHJ1ZTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlICdjcmVhdGVkRGF0ZSc6XG4gICAgICAgIHRoaXMuaXNSZWNvcmREYXRlID0gdHJ1ZTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlICdlbmREYXRlJzpcbiAgICAgICAgdGhpcy5pc1JlY29yZERhdGUgPSB0cnVlO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ3BpY2tVcERhdGUnOlxuICAgICAgICB0aGlzLmlzUmVjb3JkRGF0ZSA9IHRydWU7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnc3RhcnREYXRlJzpcbiAgICAgICAgdGhpcy5pc1JlY29yZERhdGUgPSB0cnVlO1xuICAgICAgICBicmVhaztcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHRoaXMuaXNSZWNvcmREYXRlID0gZmFsc2U7XG4gICAgICAgIGJyZWFrO1xuICAgIH1cbiAgICByZXR1cm4gdGhpcy5pc1JlY29yZERhdGU7XG4gIH1cbiAgYWxsVGFibGVSZWNvcmRTZWxlY3Rpb25DaGFuZ2VzKGV2ZW50OiB7IGNoZWNrZWQ6IGFueTsgfSkge1xuICAgIGlmIChldmVudC5jaGVja2VkKSB7XG4gICAgICB0aGlzLnNlbGVjdGVkVGFibGVSZWNvcmRMaXN0ID0gdGhpcy5kYXRhVGFibGUudGFibGVCb2R5LmNvbnRlbnQ7XG4gICAgICAvLyBjb25zb2xlLmxvZygndGhpcy5zZWxlY3RlZFRhYmxlUmVjb3JkTGlzdCcsIHRoaXMuc2VsZWN0ZWRUYWJsZVJlY29yZExpc3QpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB0aGlzLnNlbGVjdGVkVGFibGVSZWNvcmRMaXN0ID0gW10gYXMgQXJyYXk8b2JqZWN0PjtcbiAgICAvLyBjb25zb2xlLmxvZygndGhpcy5zZWxlY3RlZFRhYmxlUmVjb3JkTGlzdCcsIHRoaXMuc2VsZWN0ZWRUYWJsZVJlY29yZExpc3QpO1xuICB9XG4gIHRhYmxlUmVjb3JkU2VsZWN0aW9uQ2hhbmdlcyhldmVudDogeyBjaGVja2VkOiBhbnk7IH0sIHNlbGVjdGVkVGFibGVSZWNvcmQ6IG9iamVjdCwgcmVjb3JkSW5kZXg6IGFueSkge1xuICAgIC8vIGNvbnNvbGUubG9nKCdldmVudCwgc2VsZWN0ZWRUYWJsZVJlY29yZCcsIGV2ZW50LCBzZWxlY3RlZFRhYmxlUmVjb3JkKTtcbiAgICBpZiAoZXZlbnQuY2hlY2tlZCkge1xuICAgICAgdGhpcy5zZWxlY3RlZFRhYmxlUmVjb3JkTGlzdC5wdXNoKHNlbGVjdGVkVGFibGVSZWNvcmQpO1xuICAgICAgLy8gY29uc29sZS5sb2coJ3RoaXMuc2VsZWN0ZWRUYWJsZVJlY29yZExpc3QnLCB0aGlzLnNlbGVjdGVkVGFibGVSZWNvcmRMaXN0KTtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdGhpcy5zZWxlY3RlZFRhYmxlUmVjb3JkTGlzdCA9IHRoaXMucmVtb3ZlVW5zZWxlY3RlZFRhYmxlUmVjb3JkKHNlbGVjdGVkVGFibGVSZWNvcmQsIHRoaXMuc2VsZWN0ZWRUYWJsZVJlY29yZExpc3QpO1xuICAgIC8vIGNvbnNvbGUubG9nKCdkZWxldGVkIExpc3QnLCB0aGlzLnNlbGVjdGVkVGFibGVSZWNvcmRMaXN0KTtcbiAgfVxuICByZW1vdmVVbnNlbGVjdGVkVGFibGVSZWNvcmQocmVtb3ZhYmxlUmVjb3JkOiBvYmplY3QsIG9yaWdpbmFsUmVjb3JkTGlzdDogQXJyYXk8b2JqZWN0Pikge1xuICAgIHJldHVybiBvcmlnaW5hbFJlY29yZExpc3QuZmlsdGVyKChleGlzdGVkUmVjb3JkOiBvYmplY3QpID0+IHtcbiAgICAgIGlmIChyZW1vdmFibGVSZWNvcmQgJiYgZXhpc3RlZFJlY29yZCkge1xuICAgICAgICByZXR1cm4gIShleGlzdGVkUmVjb3JkWydpZCddID09PSByZW1vdmFibGVSZWNvcmRbJ2lkJ10pO1xuICAgICAgfVxuICAgICAgaWYgKCFyZW1vdmFibGVSZWNvcmQpIHtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICB9XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfSk7XG4gIH1cbiAgdXBsb2FkRmlsZShpbXBvcnRGaWxlUmVmOiBIVE1MSW5wdXRFbGVtZW50KSB7XG4gICAgLy8gY29uc29sZS5sb2coJ2ltcG9ydEZpbGVSZWYnLCBpbXBvcnRGaWxlUmVmKTtcbiAgICBpZiAoaW1wb3J0RmlsZVJlZikge1xuICAgICAgaW1wb3J0RmlsZVJlZi5jbGljaygpO1xuICAgIH1cbiAgfVxuICBoYW5kbGVVcGxvYWRlZEZpbGUoZmlsZXM6IEZpbGVMaXN0KSB7XG4gICAgLy8gY29uc29sZS5sb2coJ2ZpbGVzJywgZmlsZXMpO1xuICAgIGZvciAoY29uc3Qga2V5IGluIGZpbGVzKSB7XG4gICAgICBpZiAoZmlsZXMuaGFzT3duUHJvcGVydHkoa2V5KSAmJiBrZXkgIT09ICdsZW5ndGgnKSB7XG4gICAgICAgIHRoaXMuZmlsZXNUb1VwbG9hZFswXSA9IGZpbGVzLml0ZW0oMCk7XG4gICAgICB9XG4gICAgfVxuICAgIHRoaXMudXBsb2FkZWRGaWxlcy5lbWl0KHRoaXMuZmlsZXNUb1VwbG9hZCk7XG4gICAgLy8gY29uc29sZS5sb2coJ3RoaXMuZmlsZXNUb1VwbG9hZCcsIHRoaXMuZmlsZXNUb1VwbG9hZCk7XG4gIH1cbiAgZG93bmxvYWRFeHBvcnQoKSB7XG4gICAgLy8gY29uc29sZS5sb2coJ3RoaXMuZXhwb3J0TGluaycsIHRoaXMuZXhwb3J0TGluayk7XG4gICAgdGhpcy5odHRwQ2xpZW50LmdldCh0aGlzLmV4cG9ydExpbmssIHsgaGVhZGVyczogbmV3IEh0dHBIZWFkZXJzKCkuc2V0KCdBY2NlcHQnLCAnYXBwbGljYXRpb24vdm5kLm1zLWV4Y2VsJykgfSkuc3Vic2NyaWJlKHJlcyA9PiB7XG4gICAgICAvLyBjb25zb2xlLmxvZygncmVzJywgcmVzKTtcbiAgICB9KVxuICB9XG5cbiAgY2xpY2tFeHBvcnRUb0V4Y2VsKCkge1xuICAgIHRoaXMuZXhwb3J0VG9FeGNlbC5lbWl0KHRydWUpXG4gIH1cbiAgY29sdW1uTGVuZ3RoQ2hhbmdlZChldmVudCkge1xuICAgIC8vIGNvbnNvbGUubG9nKCdjb2x1bW5WYWx1ZUNoYW5nZXMnLCBldmVudCk7XG4gICAgdGhpcy5jb2x1bW5MZW5ndGhDaGFuZ2VzLmVtaXQoZXZlbnQudGFyZ2V0LnZhbHVlKTtcbiAgfVxufVxuIl19