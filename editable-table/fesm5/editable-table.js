import { EventEmitter, Injectable, ɵɵdefineInjectable, Component, forwardRef, ChangeDetectorRef, Input, Output, NgModule } from '@angular/core';
import { Subject, Observable, Subscription } from 'rxjs';
import { __assign } from 'tslib';
import { NG_VALUE_ACCESSOR, NG_VALIDATORS, FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { map } from 'rxjs/operators';
import { v4 } from 'uuid';
import { CommonModule } from '@angular/common';
import { MatCheckboxModule, MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatIconModule } from '@angular/material';
import { MatSelectModule } from '@angular/material/select';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/et-data-handler.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var EtDataHandlerService = /** @class */ (function () {
    function EtDataHandlerService() {
        var _this = this;
        this.sendColumnNameRecord$ = new Subject();
        this.sendColumnRecord$ = new Subject();
        this.validateTable$ = new EventEmitter();
        this.getTableRecord$ = new EventEmitter();
        this.getColumnNameToCollectColumnRecord$ = new Observable((/**
         * @param {?} subscriber
         * @return {?}
         */
        function (subscriber) {
            _this.sendColumnNameRecord = subscriber;
        }));
    }
    /**
     * @param {?} formControlName
     * @return {?}
     */
    EtDataHandlerService.prototype.getColumnRecord = /**
     * @param {?} formControlName
     * @return {?}
     */
    function (formControlName) {
        // console.log('formControlName', formControlName);
        this.sendColumnNameRecord$.next(formControlName);
        // return this.sendColumnRecord$;
    };
    /**
     * @return {?}
     */
    EtDataHandlerService.prototype.getColumnNameToCollectColumnRecord = /**
     * @return {?}
     */
    function () {
        var _this = this;
        return new Observable((/**
         * @param {?} subscriber
         * @return {?}
         */
        function (subscriber) {
            _this.sendColumnNameRecord = subscriber;
        }));
    };
    /**
     * @param {?} columnRecordList
     * @param {?} formControlName
     * @return {?}
     */
    EtDataHandlerService.prototype.emitColumnRecord = /**
     * @param {?} columnRecordList
     * @param {?} formControlName
     * @return {?}
     */
    function (columnRecordList, formControlName) {
        var _this = this;
        // console.log('columnRecordList', columnRecordList);
        setTimeout((/**
         * @return {?}
         */
        function () {
            _this.sendColumnRecord$.next({ recordList: columnRecordList, formControlName: formControlName });
        }), 1);
    };
    /**
     * @return {?}
     */
    EtDataHandlerService.prototype.validateTable = /**
     * @return {?}
     */
    function () {
        return this.validateTable$;
    };
    /**
     * @return {?}
     */
    EtDataHandlerService.prototype.getValidTableRecord = /**
     * @return {?}
     */
    function () {
        this.validateTable$.emit(true);
        return this.getTableRecord$;
    };
    /**
     * @param {?} record
     * @return {?}
     */
    EtDataHandlerService.prototype.sendTableRecord = /**
     * @param {?} record
     * @return {?}
     */
    function (record) {
        var _this = this;
        setTimeout((/**
         * @return {?}
         */
        function () {
            _this.getTableRecord$.emit(record);
        }), 100);
    };
    EtDataHandlerService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    EtDataHandlerService.ctorParameters = function () { return []; };
    /** @nocollapse */ EtDataHandlerService.ngInjectableDef = ɵɵdefineInjectable({ factory: function EtDataHandlerService_Factory() { return new EtDataHandlerService(); }, token: EtDataHandlerService, providedIn: "root" });
    return EtDataHandlerService;
}());
if (false) {
    /**
     * @type {?}
     * @private
     */
    EtDataHandlerService.prototype.sendColumnNameRecord;
    /** @type {?} */
    EtDataHandlerService.prototype.sendColumnNameRecord$;
    /** @type {?} */
    EtDataHandlerService.prototype.sendColumnRecord$;
    /**
     * @type {?}
     * @private
     */
    EtDataHandlerService.prototype.validateTable$;
    /**
     * @type {?}
     * @private
     */
    EtDataHandlerService.prototype.getTableRecord$;
    /**
     * @type {?}
     * @private
     */
    EtDataHandlerService.prototype.getColumnNameToCollectColumnRecord$;
    /**
     * @type {?}
     * @private
     */
    EtDataHandlerService.prototype.sendColumnRecord;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/et-form-handler.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var EtFormHandlerService = /** @class */ (function () {
    function EtFormHandlerService() {
    }
    /**
     * @param {?} validateFn
     * @return {?}
     */
    EtFormHandlerService.prototype.setValidators = /**
     * @param {?} validateFn
     * @return {?}
     */
    function (validateFn) {
    };
    EtFormHandlerService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    EtFormHandlerService.ctorParameters = function () { return []; };
    /** @nocollapse */ EtFormHandlerService.ngInjectableDef = ɵɵdefineInjectable({ factory: function EtFormHandlerService_Factory() { return new EtFormHandlerService(); }, token: EtFormHandlerService, providedIn: "root" });
    return EtFormHandlerService;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/editable-table-config.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function TableConfig() { }
if (false) {
    /** @type {?} */
    TableConfig.prototype.title;
    /** @type {?} */
    TableConfig.prototype.formControlName;
    /** @type {?|undefined} */
    TableConfig.prototype.key;
    /** @type {?} */
    TableConfig.prototype.inputField;
    /** @type {?|undefined} */
    TableConfig.prototype.isNeedValueChanges;
    /** @type {?|undefined} */
    TableConfig.prototype.displayKey;
    /** @type {?|undefined} */
    TableConfig.prototype.patchKey;
}
/**
 * @record
 */
function PatchValue() { }
if (false) {
    /** @type {?} */
    PatchValue.prototype.rowIndex;
    /** @type {?} */
    PatchValue.prototype.patchValue;
    /** @type {?|undefined} */
    PatchValue.prototype.tableRowId;
}
/**
 * @record
 */
function AssignListToSelect() { }
if (false) {
    /** @type {?} */
    AssignListToSelect.prototype.formControlName;
    /** @type {?} */
    AssignListToSelect.prototype.list;
}
/**
 * @record
 */
function AssignListToAutocomplete() { }
if (false) {
    /** @type {?} */
    AssignListToAutocomplete.prototype.config;
    /** @type {?} */
    AssignListToAutocomplete.prototype.list;
    /** @type {?|undefined} */
    AssignListToAutocomplete.prototype.searchKey;
}
/**
 * @record
 */
function ControlsConfig() { }
/**
 * @record
 */
function ColumnRecord() { }
if (false) {
    /** @type {?} */
    ColumnRecord.prototype.recordList;
    /** @type {?} */
    ColumnRecord.prototype.formControlName;
}
/**
 * @record
 * @template T
 */
function EtTableValueChangeEvent() { }
if (false) {
    /** @type {?} */
    EtTableValueChangeEvent.prototype.key;
    /** @type {?} */
    EtTableValueChangeEvent.prototype.config;
    /** @type {?} */
    EtTableValueChangeEvent.prototype.tableRow;
}
/**
 * @record
 * @template T
 */
function EtAutocompleteSelectedEvent() { }
if (false) {
    /** @type {?} */
    EtAutocompleteSelectedEvent.prototype.option;
    /** @type {?} */
    EtAutocompleteSelectedEvent.prototype.rowIndex;
    /** @type {?} */
    EtAutocompleteSelectedEvent.prototype.config;
    /** @type {?} */
    EtAutocompleteSelectedEvent.prototype.tableRow;
}
var ETValidationError = /** @class */ (function () {
    function ETValidationError(formControlName, tableRowId, msg, type) {
        this.formControlName = formControlName;
        this.tableRowId = tableRowId;
        this.msg = msg;
        this.type = type;
        if (!msg) {
            this.isRemove = true;
        }
        Object.seal(this);
    }
    return ETValidationError;
}());
if (false) {
    /** @type {?} */
    ETValidationError.prototype.isRemove;
    /** @type {?} */
    ETValidationError.prototype.formControlName;
    /** @type {?} */
    ETValidationError.prototype.tableRowId;
    /** @type {?} */
    ETValidationError.prototype.msg;
    /** @type {?} */
    ETValidationError.prototype.type;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/editable-table.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var EditableTableComponent = /** @class */ (function () {
    function EditableTableComponent(fb, etDataHandlerService, _changeDetectorRef) {
        var _this = this;
        this.fb = fb;
        this.etDataHandlerService = etDataHandlerService;
        this._changeDetectorRef = _changeDetectorRef;
        this.filteredOptions = {};
        this.selectOptions = {};
        this.subscription = new Subscription();
        this.viewOnly = false;
        this.hideActionBtn = false;
        this.hideDefaultRow = false;
        this.tableConfig = (/** @type {?} */ ([]));
        this.getTableRecordChange = new EventEmitter();
        this.optionSelected = new EventEmitter();
        this.searchValueChanges = new EventEmitter();
        this.tableValueChanges = new EventEmitter();
        this.deletedTableRecords = new EventEmitter();
        this.matAutocompleteDisplayFn = new Subject();
        /* ================  NG_VALUE_ACCESSOR && NG_VALIDATORS ===============*/
        this.onTouched = (/**
         * @return {?}
         */
        function () {
            _this.editableTableForm.markAllAsTouched();
        });
    }
    Object.defineProperty(EditableTableComponent.prototype, "assignListToSelect", {
        set: /**
         * @param {?} v
         * @return {?}
         */
        function (v) {
            var _this = this;
            if (!!v) {
                v.forEach((/**
                 * @param {?} ele
                 * @return {?}
                 */
                function (ele) {
                    _this.selectOptions[ele.formControlName] = ele.list;
                }));
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EditableTableComponent.prototype, "getFormData", {
        set: /**
         * @param {?} v
         * @return {?}
         */
        function (v) {
            var _this = this;
            v.subscribe((/**
             * @param {?} res
             * @return {?}
             */
            function (res) {
                if (!!res) {
                    _this.getTableRecordChange.emit(((/** @type {?} */ (_this.editableTableForm.controls.table))).getRawValue());
                }
            }));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EditableTableComponent.prototype, "assignListToAutocomplete", {
        set: /**
         * @param {?} v
         * @return {?}
         */
        function (v) {
            if (v && v.config.inputField === 'autocomplete') {
                this.filteredOptions[v.config.formControlName] = this._filter(v.searchKey, v.list, v.config.displayKey);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EditableTableComponent.prototype, "patchValue", {
        set: /**
         * @param {?} v
         * @return {?}
         */
        function (v) {
            this._patchValue = v;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EditableTableComponent.prototype, "patchValueToSelect", {
        set: /**
         * @param {?} v
         * @return {?}
         */
        function (v) {
            if (!!v && this.editableTableForm) {
                /** @type {?} */
                var form_1 = ((/** @type {?} */ (this.editableTableForm.controls.table)));
                v.forEach((/**
                 * @param {?} ele
                 * @return {?}
                 */
                function (ele) {
                    if (form_1.controls[ele.rowIndex]) {
                        form_1.controls[ele.rowIndex].patchValue(ele.patchValue);
                    }
                }));
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EditableTableComponent.prototype, "tableData", {
        set: /**
         * @param {?} v
         * @return {?}
         */
        function (v) {
            {
                if (!!v && v.length > 0) {
                    this._tableData = v;
                    this.createTableWithData(v);
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EditableTableComponent.prototype, "formGroupForTableRow", {
        set: /**
         * @param {?} fb
         * @return {?}
         */
        function (fb) {
            this._formGroupForTableRow = __assign({}, fb);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EditableTableComponent.prototype, "validationError", {
        set: /**
         * @param {?} ve
         * @return {?}
         */
        function (ve) {
            if (ve) {
                this._validationError = ve;
                this.setError(ve);
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    EditableTableComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.createTableForm();
        this.subscribeToSendColumnNameRecord();
        this.subscribeToValidateTable();
        this.assignMarkAsTouchedFnToControl(this.control);
        // this.setQuotationListToTable();
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    EditableTableComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (!!changes.patchValue && !!changes.patchValue.currentValue) {
            this._patchValue = changes.patchValue.currentValue;
            this.patchValueToTable(this._patchValue);
        }
        if (!!changes.control && !!changes.control.currentValue) {
            this.assignMarkAsTouchedFnToControl(this.control);
        }
    };
    /**
     * @param {?} control
     * @return {?}
     */
    EditableTableComponent.prototype.assignMarkAsTouchedFnToControl = /**
     * @param {?} control
     * @return {?}
     */
    function (control) {
        var _this = this;
        if (control) {
            control.markAsTouched = (/**
             * @return {?}
             */
            function () {
                _this.editableTableForm.markAllAsTouched();
            });
        }
    };
    /**
     * @private
     * @param {?} v
     * @return {?}
     */
    EditableTableComponent.prototype.patchValueToTable = /**
     * @private
     * @param {?} v
     * @return {?}
     */
    function (v) {
        if (!!v && v.length > 0 && this.editableTableForm) {
            if (!this.editableTableForm) {
                this.createTableForm();
            }
            /** @type {?} */
            var form_2 = ((/** @type {?} */ (this.editableTableForm.controls.table)));
            /** @type {?} */
            var rowIndex_1;
            v.forEach((/**
             * @param {?} ele
             * @return {?}
             */
            function (ele) {
                if (ele.tableRowId) {
                    for (var key in form_2.value) {
                        if (form_2.value.hasOwnProperty(key)) {
                            /** @type {?} */
                            var tableRow = form_2.value[key];
                            if (tableRow['tableRowId'] === ele.tableRowId) {
                                rowIndex_1 = key;
                            }
                        }
                    }
                }
                rowIndex_1 = (!rowIndex_1 && ele.rowIndex) ? ele.rowIndex : rowIndex_1;
                rowIndex_1 ? form_2.controls[rowIndex_1].patchValue(ele.patchValue) : console.error('patch row undefined');
            }));
        }
    };
    /**
     * @private
     * @param {?} tableData
     * @return {?}
     */
    EditableTableComponent.prototype.createTableWithData = /**
     * @private
     * @param {?} tableData
     * @return {?}
     */
    function (tableData) {
        var _this = this;
        tableData.forEach((/**
         * @param {?} element
         * @param {?} index
         * @return {?}
         */
        function (element, index) {
            if (index === 0) {
                _this.patchValueToTable([{ rowIndex: '0', patchValue: element }]);
                return;
            }
            _this.addRow(element);
        }));
    };
    // @Input() private tableData;
    // @Input() private tableData;
    /**
     * @private
     * @return {?}
     */
    EditableTableComponent.prototype.subscribeToSendColumnNameRecord = 
    // @Input() private tableData;
    /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        this.etDataHandlerService.sendColumnNameRecord$.subscribe((/**
         * @param {?} formControlName
         * @return {?}
         */
        function (formControlName) {
            if (_this.viewOnly) {
                return;
            }
            /** @type {?} */
            var form = ((/** @type {?} */ (_this.editableTableForm.controls.table)));
            /** @type {?} */
            var columnValues = [];
            /** @type {?} */
            var tableRecord = form.getRawValue();
            if (formControlName.toLowerCase() === 'all') {
                /** @type {?} */
                var sortedConfigList = _this.tableConfig.filter((/**
                 * @param {?} config
                 * @return {?}
                 */
                function (config) { return config.isNeedValueChanges; }));
                sortedConfigList.forEach((/**
                 * @param {?} config
                 * @return {?}
                 */
                function (config) {
                    columnValues = _this.collectColumnRecord(tableRecord, config.formControlName);
                    _this.etDataHandlerService.emitColumnRecord(columnValues, config.formControlName);
                }));
                return;
            }
            columnValues = _this.collectColumnRecord(tableRecord, formControlName);
            _this.etDataHandlerService.emitColumnRecord(columnValues, formControlName);
        }));
    };
    /**
     * @private
     * @param {?} tableRecord
     * @param {?} tableRowKey
     * @return {?}
     */
    EditableTableComponent.prototype.collectColumnRecord = /**
     * @private
     * @param {?} tableRecord
     * @param {?} tableRowKey
     * @return {?}
     */
    function (tableRecord, tableRowKey) {
        /** @type {?} */
        var columnValues = [];
        for (var key in tableRecord) {
            if (tableRecord.hasOwnProperty(key)) {
                /** @type {?} */
                var tableRow = tableRecord[key];
                columnValues.push(tableRow[tableRowKey]);
            }
        }
        return columnValues;
    };
    /**
     * @private
     * @return {?}
     */
    EditableTableComponent.prototype.subscribeToValidateTable = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var validateSbscrb = this.etDataHandlerService.validateTable().subscribe((/**
         * @param {?} status
         * @return {?}
         */
        function (status) {
            if (status) {
                _this.editableTableForm.markAllAsTouched();
                if (_this.editableTableForm.status === 'VALID' || _this.editableTableForm.status === 'DISABLED') {
                    _this.etDataHandlerService.sendTableRecord({
                        data: ((/** @type {?} */ (_this.editableTableForm.controls.table))).getRawValue(),
                        valid: true
                    });
                    return;
                }
                _this.etDataHandlerService.sendTableRecord({
                    data: [],
                    valid: false
                });
            }
        }));
        this.subscription.add(validateSbscrb);
    };
    /**
     * @return {?}
     */
    EditableTableComponent.prototype.createTableForm = /**
     * @return {?}
     */
    function () {
        if (this.editableTableForm) {
            return;
        }
        if (this.etFormArray) {
            this.editableTableForm = this.fb.group({
                table: this.etFormArray
            });
        }
        else {
            this.editableTableForm = this.fb.group({
                table: this.fb.array([])
            });
        }
        if (!this.hideDefaultRow
            && ((/** @type {?} */ (this.editableTableForm.controls.table))).length === 0
            && (this.tableConfig.length > 0 || this.etFormArray.length > 0)) {
            this.addRow();
        }
        this.tableValueChanges.emit(this.editableTableForm.valueChanges);
    };
    /**
     * @param {?=} data
     * @return {?}
     */
    EditableTableComponent.prototype.implementsDetailsRow = /**
     * @param {?=} data
     * @return {?}
     */
    function (data) {
        var _this = this;
        /** @type {?} */
        var fg = this.fb.group(this._formGroupForTableRow);
        if (!!data && !this.viewOnly) {
            fg.patchValue(data);
        }
        if (this.viewOnly) {
            fg.disable();
            !!data ? fg.patchValue(data, { onlySelf: true, emitEvent: false }) : null;
        }
        fg.addControl('tableRowId', this.fb.control(v4()));
        this.tableConfig.forEach((/**
         * @param {?} ele
         * @return {?}
         */
        function (ele) {
            // fg.valueCha nges
            if (ele.inputField === 'autocomplete' || ele.isNeedValueChanges) {
                fg.controls[ele.formControlName].valueChanges.subscribe((/**
                 * @param {?} searchValue
                 * @return {?}
                 */
                function (searchValue) {
                    _this.searchValueChanges.emit({ key: searchValue, config: ele, tableRow: fg.getRawValue() });
                    _this.dirtyAutocompleteColumn = __assign({}, ele);
                }));
            }
        }));
        return fg;
    };
    /**
     * @private
     * @param {?} value
     * @param {?} options
     * @param {?} key
     * @return {?}
     */
    EditableTableComponent.prototype._filter = /**
     * @private
     * @param {?} value
     * @param {?} options
     * @param {?} key
     * @return {?}
     */
    function (value, options, key) {
        /** @type {?} */
        var filterValue;
        if (!!value || value === "") {
            return options;
        }
        if (typeof value === 'string') {
            filterValue = value.toLowerCase();
        }
        if (typeof value === 'object') {
            filterValue = value[key].toLowerCase();
        }
        return options.filter((/**
         * @param {?} option
         * @return {?}
         */
        function (option) { return option[key].toLowerCase().indexOf(filterValue) === 0; }));
    };
    /**
     * @return {?}
     */
    EditableTableComponent.prototype.submitData = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @param {?=} row
     * @return {?}
     */
    EditableTableComponent.prototype.addRow = /**
     * @param {?=} row
     * @return {?}
     */
    function (row) {
        if (this.etFormArray) {
            this.editableTableForm.controls.table = this.etFormArray;
            return;
        }
        /** @type {?} */
        var implementsDetails = (/** @type {?} */ (this.editableTableForm.controls.table));
        implementsDetails.push(this.implementsDetailsRow(row));
    };
    /**
     * @param {?=} row
     * @return {?}
     */
    EditableTableComponent.prototype.resetTable = /**
     * @param {?=} row
     * @return {?}
     */
    function (row) {
        /** @type {?} */
        var implementsDetails = (/** @type {?} */ (this.editableTableForm.controls.table));
        implementsDetails.clear();
        implementsDetails.push(this.implementsDetailsRow(row));
    };
    /**
     * @return {?}
     */
    EditableTableComponent.prototype.deleteRow = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var implementsDetails = (/** @type {?} */ (this.editableTableForm.controls.table));
        /** @type {?} */
        var deleteRecordList = [];
        /** @type {?} */
        var nonSelected = implementsDetails.controls.filter((/**
         * @param {?} machinery
         * @return {?}
         */
        function (machinery) {
            if (machinery.value.isSelected) {
                deleteRecordList.push(machinery.getRawValue());
            }
            return !machinery.value.isSelected;
        }));
        this.deletedTableRecords.emit(deleteRecordList);
        implementsDetails.clear();
        nonSelected.forEach((/**
         * @param {?} el
         * @return {?}
         */
        function (el) { return implementsDetails.push(el); }));
    };
    /**
     * @private
     * @param {?} config
     * @return {?}
     */
    EditableTableComponent.prototype.generateAutocompleteDisplayFn = /**
     * @private
     * @param {?} config
     * @return {?}
     */
    function (config) {
        /** @type {?} */
        var key = config.patchKey || config.displayKey;
        /** @type {?} */
        var displayWith = (/**
         * @param {?} user
         * @return {?}
         */
        function (user) {
            return (user && typeof user === 'object') ? user[key] : user;
        })
        // return of(displayWith);
        ;
        // return of(displayWith);
        this.matAutocompleteDisplayFn.next(displayWith);
        this._changeDetectorRef.detectChanges();
    };
    /**
     * @param {?} config
     * @param {?} formGroup
     * @return {?}
     */
    EditableTableComponent.prototype.autocompleteClicked = /**
     * @param {?} config
     * @param {?} formGroup
     * @return {?}
     */
    function (config, formGroup) {
        this.filteredOptions[config.formControlName] = [];
        this.generateAutocompleteDisplayFn(config);
        if (formGroup.controls[config.formControlName].value) {
            this.searchValueChanges.emit({ key: formGroup.controls[config.formControlName].value, config: config, tableRow: formGroup.getRawValue() });
            this.dirtyAutocompleteColumn = __assign({}, config);
        }
    };
    /**
     * @param {?} event
     * @param {?} rowIndex
     * @param {?} rowFormGroup
     * @param {?} config
     * @return {?}
     */
    EditableTableComponent.prototype.valueSelectedFromAutocomplete = /**
     * @param {?} event
     * @param {?} rowIndex
     * @param {?} rowFormGroup
     * @param {?} config
     * @return {?}
     */
    function (event, rowIndex, rowFormGroup, config) {
        /** @type {?} */
        var option = (event && typeof event.option.value === 'object') ? __assign({}, event.option.value) : event.option.value || '';
        this.optionSelected.emit(__assign({ option: option }, { rowIndex: rowIndex, tableRow: rowFormGroup.getRawValue(), config: config }));
    };
    /**
     * @param {?} event
     * @return {?}
     */
    EditableTableComponent.prototype.selectionChanged = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
    };
    /**
     * @param {?} value
     * @param {?} rowFormGroup
     * @param {?} formControlName
     * @return {?}
     */
    EditableTableComponent.prototype.contenteditableValuechange = /**
     * @param {?} value
     * @param {?} rowFormGroup
     * @param {?} formControlName
     * @return {?}
     */
    function (value, rowFormGroup, formControlName) {
        rowFormGroup.controls[formControlName].patchValue(value);
    };
    /**
     * @private
     * @param {?} ve
     * @return {?}
     */
    EditableTableComponent.prototype.setError = /**
     * @private
     * @param {?} ve
     * @return {?}
     */
    function (ve) {
        /** @type {?} */
        var formArray = ((/** @type {?} */ (this.editableTableForm.get('table'))));
        /** @type {?} */
        var formIndex = formArray.getRawValue().findIndex((/**
         * @param {?} formValue
         * @return {?}
         */
        function (formValue) {
            if (formValue.tableRowId === ve.tableRowId) {
                return true;
            }
        }));
        /** @type {?} */
        var tableRowFG = formArray.at(formIndex);
        if (tableRowFG) {
            if (ve.isRemove) {
                tableRowFG.get(ve.formControlName).setErrors(null);
                return;
            }
            tableRowFG.get(ve.formControlName).setErrors(__assign({ msg: ve.msg }, ve.type));
        }
    };
    /**
     * @param {?} val
     * @return {?}
     */
    EditableTableComponent.prototype.writeValue = /**
     * @param {?} val
     * @return {?}
     */
    function (val) {
        if (val && val.resetForm) {
            this.editableTableForm.reset();
            this.resetTable();
            return;
        }
        val && val.length > 0 && this.createTableWithData(val);
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    EditableTableComponent.prototype.registerOnChange = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        var _this = this;
        fn && this.editableTableForm.controls.table.valueChanges.pipe(map((/**
         * @param {?} value
         * @return {?}
         */
        function (value) { return ((/** @type {?} */ (_this.editableTableForm.controls.table))).getRawValue(); }))).subscribe(fn);
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    EditableTableComponent.prototype.registerOnTouched = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        fn && (this.onTouched = fn);
    };
    /**
     * @param {?} isDisabled
     * @return {?}
     */
    EditableTableComponent.prototype.setDisabledState = /**
     * @param {?} isDisabled
     * @return {?}
     */
    function (isDisabled) {
        isDisabled ? this.editableTableForm.controls.table.disable() : this.editableTableForm.controls.table.enable();
        // throw new Error("Method not implemented.");
    };
    /**
     * @param {?} control
     * @return {?}
     */
    EditableTableComponent.prototype.validate = /**
     * @param {?} control
     * @return {?}
     */
    function (control) {
        return this.editableTableForm.controls.table.valid ? null : { invalidForm: { valid: false, message: "editableTableForm fields are invalid" } };
    };
    /* ==================================================================== */
    /* ==================================================================== */
    /**
     * @return {?}
     */
    EditableTableComponent.prototype.ngOnDestroy = /* ==================================================================== */
    /**
     * @return {?}
     */
    function () {
        this.subscription.unsubscribe();
    };
    EditableTableComponent.decorators = [
        { type: Component, args: [{
                    selector: 'editable-table',
                    template: "<div id=\"divshow\">\n  <form [formGroup]=\"editableTableForm\">\n    <div>\n      <div class=\"table-responsive\">\n        <table class=\"table table-bordered table-sm\" formArrayName=\"table\">\n          <thead class=\"thead-light\">\n            <tr>\n              <th scope=\"col\" style=\"width: 10px;\" *ngFor=\"let config of tableConfig\">{{config.title}}</th>\n            </tr>\n          </thead>\n          <tbody>\n            <ng-container *ngFor=\"let rowFormGroup of editableTableForm['controls'].table['controls']; let i = index\"\n              [formGroupName]=\"i\">\n              <tr>\n                <ng-container *ngFor=\"let config of tableConfig\">\n                  <td *ngIf=\"config.inputField !== 'contenteditable'\">\n                    <mat-checkbox *ngIf=\"config.inputField === 'checkbox'\" color=\"basic\"\n                      class=\"form-control form-control-sm\" [formControlName]=\"config.formControlName\" [checked]=\"false\">\n                    </mat-checkbox>\n                    <mat-form-field class=\"example-full-width\" *ngIf=\"config.inputField === 'input'\"\n                      appearance=\"outline\" [ngClass]=\"{pb_0:!rowFormGroup.get(config.formControlName).errors}\">\n                      <input matInput [placeholder]=\"config.title\" [formControlName]=\"config.formControlName\">\n                      <mat-error\n                        *ngIf=\"rowFormGroup.get(config.formControlName).errors && rowFormGroup.get(config.formControlName).errors.msg &&!rowFormGroup.get(config.formControlName).errors.required\">\n                        {{rowFormGroup.get(config.formControlName).errors.msg}}</mat-error>\n                      <mat-error\n                        *ngIf=\"rowFormGroup.get(config.formControlName).errors && !rowFormGroup.get(config.formControlName).errors.msg &&rowFormGroup.get(config.formControlName).errors.required\">\n                        This field is required</mat-error>\n                    </mat-form-field>\n                    <mat-form-field *ngIf=\"config.inputField === 'autocomplete'\" class=\"w-100\"\n                      [ngClass]=\"{pb_0:!rowFormGroup.get(config.formControlName).errors}\" appearance=\"outline\">\n                      <input matInput [formControlName]=\"config.formControlName\" [matAutocomplete]=\"autoAmcNumber\"\n                        (focus)=\"autocompleteClicked(config, rowFormGroup)\" (keyup.enter)=\"$event.preventDefault();\">\n                      <!-- (blur)=\"resetFormControlNotHavingObject(searchAMCForm.controls.amcNumber, 'amcNumber')\" -->\n                      <mat-autocomplete #autoAmcNumber=\"matAutocomplete\"\n                        [displayWith]=\"matAutocompleteDisplayFn | async\"\n                        (optionSelected)=\"valueSelectedFromAutocomplete($event, i, rowFormGroup, config)\">\n                        <mat-option *ngFor=\"let option of filteredOptions[config.formControlName]\" [value]=\"option\">\n                          <!-- (click)=\"valueSelectedFromAutocomplete(option)\" -->\n                          {{option[config.displayKey]? option[config.displayKey] : option}}\n                        </mat-option>\n                      </mat-autocomplete>\n                      <mat-icon matSuffix>search</mat-icon>\n                      <mat-error\n                        *ngIf=\"rowFormGroup.get(config.formControlName).errors && rowFormGroup.get(config.formControlName).errors.msg &&!rowFormGroup.get(config.formControlName).errors.required\">\n                        {{rowFormGroup.get(config.formControlName).errors.msg}}</mat-error>\n                      <mat-error\n                        *ngIf=\"rowFormGroup.get(config.formControlName).errors && !rowFormGroup.get(config.formControlName).errors.msg &&rowFormGroup.get(config.formControlName).errors.required\">\n                        This field is required</mat-error>\n                    </mat-form-field>\n                    <!-- <span *ngIf=\"config.inputField === 'autocomplete'\">\n                      {{matAutocompleteDisplayFn | async}}\n                    </span> -->\n                    <mat-form-field *ngIf=\"config.inputField === 'select'\" appearance=\"outline\" class=\"pb_0\">\n                      <mat-label>Select {{config.title}}</mat-label>\n                      <mat-select [formControlName]=\"config.formControlName\"\n                        (selectionChange)=\"selectionChanged($event)\">\n                        <mat-option *ngFor=\"let option of selectOptions[config.formControlName]\" [value]=\"option\">\n                          {{option[config.displayKey]? option[config.displayKey] : option}}\n                        </mat-option>\n                      </mat-select>\n                      <mat-error\n                      *ngIf=\"rowFormGroup.get(config.formControlName).errors && rowFormGroup.get(config.formControlName).errors.msg &&!rowFormGroup.get(config.formControlName).errors.required\">\n                      {{rowFormGroup.get(config.formControlName).errors.msg}}</mat-error>\n                      <mat-error\n                        *ngIf=\"rowFormGroup.get(config.formControlName).errors && !rowFormGroup.get(config.formControlName).errors.msg &&rowFormGroup.get(config.formControlName).errors.required\">\n                        This field is required</mat-error>\n                    </mat-form-field>\n                  </td>\n                  <td *ngIf=\"config.inputField === 'contenteditable'\"\n                    [attr.contenteditable]=\"config.inputField !== 'contenteditable' || config.isNeedValueChanges\"\n                    [textContent]=\"rowFormGroup.controls[config.formControlName].value\"\n                    (input)=\"contenteditableValuechange($event.target.textContent,rowFormGroup,config.formControlName)\"\n                    #contenteditableRef></td>\n                </ng-container>\n              </tr>\n              <!-- <ng-content select=\"tr\"></ng-content> -->\n            </ng-container>\n          </tbody>\n          <tfoot>\n            <ng-content select=\"ng-container.table_footer\"></ng-content>\n          </tfoot>\n        </table>\n      </div>\n    </div>\n  </form>\n  <div class=\" text-center mt-2\" *ngIf=\"!hideActionBtn && !viewOnly \">\n    <button type=\"button\" class=\"btn btn-sm btn_primary \" (click)=\"addRow()\"> Add\n      Row</button>\n    <button type=\"button\" class=\"btn btn-sm btn_secondary ml-2 \" (click)=\"deleteRow()\"> Delete Row </button>\n  </div>\n</div>",
                    providers: [
                        {
                            provide: NG_VALUE_ACCESSOR,
                            useExisting: forwardRef((/**
                             * @return {?}
                             */
                            function () { return EditableTableComponent; })),
                            multi: true
                        },
                        {
                            provide: NG_VALIDATORS,
                            useExisting: forwardRef((/**
                             * @return {?}
                             */
                            function () { return EditableTableComponent; })),
                            multi: true
                        }
                    ],
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    EditableTableComponent.ctorParameters = function () { return [
        { type: FormBuilder },
        { type: EtDataHandlerService },
        { type: ChangeDetectorRef }
    ]; };
    EditableTableComponent.propDecorators = {
        viewOnly: [{ type: Input }],
        hideActionBtn: [{ type: Input }],
        hideDefaultRow: [{ type: Input }],
        assignListToSelect: [{ type: Input }],
        tableConfig: [{ type: Input }],
        getFormData: [{ type: Input }],
        getTableRecordChange: [{ type: Output }],
        optionSelected: [{ type: Output }],
        assignListToAutocomplete: [{ type: Input }],
        patchValue: [{ type: Input }],
        patchValueToSelect: [{ type: Input }],
        tableData: [{ type: Input, args: ['tableData',] }],
        searchValueChanges: [{ type: Output }],
        tableValueChanges: [{ type: Output }],
        formGroupForTableRow: [{ type: Input }],
        deletedTableRecords: [{ type: Output }],
        matAutocompleteDisplayFn: [{ type: Input }],
        etFormArray: [{ type: Input }],
        control: [{ type: Input }],
        validationError: [{ type: Input }]
    };
    return EditableTableComponent;
}());
if (false) {
    /** @type {?} */
    EditableTableComponent.prototype.editableTableForm;
    /** @type {?} */
    EditableTableComponent.prototype.isEdit;
    /** @type {?} */
    EditableTableComponent.prototype.isView;
    /** @type {?} */
    EditableTableComponent.prototype.filteredOptions;
    /** @type {?} */
    EditableTableComponent.prototype.selectOptions;
    /** @type {?} */
    EditableTableComponent.prototype.subscription;
    /** @type {?} */
    EditableTableComponent.prototype.viewOnly;
    /** @type {?} */
    EditableTableComponent.prototype.hideActionBtn;
    /** @type {?} */
    EditableTableComponent.prototype.hideDefaultRow;
    /** @type {?} */
    EditableTableComponent.prototype.tableConfig;
    /** @type {?} */
    EditableTableComponent.prototype.getTableRecordChange;
    /** @type {?} */
    EditableTableComponent.prototype.optionSelected;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype._patchValue;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype.dirtyAutocompleteColumn;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype._tableData;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype._formGroupForTableRow;
    /** @type {?} */
    EditableTableComponent.prototype.searchValueChanges;
    /** @type {?} */
    EditableTableComponent.prototype.tableValueChanges;
    /** @type {?} */
    EditableTableComponent.prototype.deletedTableRecords;
    /** @type {?} */
    EditableTableComponent.prototype.matAutocompleteDisplayFn;
    /** @type {?} */
    EditableTableComponent.prototype.etFormArray;
    /** @type {?} */
    EditableTableComponent.prototype.control;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype._validationError;
    /** @type {?} */
    EditableTableComponent.prototype.onTouched;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype.fb;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype.etDataHandlerService;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype._changeDetectorRef;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/editable-table.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var EditableTableModule = /** @class */ (function () {
    function EditableTableModule() {
    }
    EditableTableModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [EditableTableComponent],
                    imports: [
                        CommonModule,
                        FormsModule,
                        ReactiveFormsModule,
                        MatCheckboxModule,
                        MatAutocompleteModule,
                        MatFormFieldModule,
                        MatInputModule,
                        MatIconModule,
                        MatSelectModule
                    ],
                    exports: [EditableTableComponent]
                },] }
    ];
    return EditableTableModule;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: editable-table.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { ETValidationError, EditableTableComponent, EditableTableModule, EtDataHandlerService, EtFormHandlerService };
//# sourceMappingURL=editable-table.js.map
