(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('rxjs'), require('@angular/forms'), require('rxjs/operators'), require('uuid'), require('@angular/common'), require('@angular/material'), require('@angular/material/select')) :
    typeof define === 'function' && define.amd ? define('editable-table', ['exports', '@angular/core', 'rxjs', '@angular/forms', 'rxjs/operators', 'uuid', '@angular/common', '@angular/material', '@angular/material/select'], factory) :
    (global = global || self, factory(global['editable-table'] = {}, global.ng.core, global.rxjs, global.ng.forms, global.rxjs.operators, global.uuid, global.ng.common, global.ng.material, global.ng.material.select));
}(this, (function (exports, core, rxjs, forms, operators, uuid, common, material, select) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __exportStar(m, exports) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
        if (m) return m.call(o);
        return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/et-data-handler.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var EtDataHandlerService = /** @class */ (function () {
        function EtDataHandlerService() {
            var _this = this;
            this.sendColumnNameRecord$ = new rxjs.Subject();
            this.sendColumnRecord$ = new rxjs.Subject();
            this.validateTable$ = new core.EventEmitter();
            this.getTableRecord$ = new core.EventEmitter();
            this.getColumnNameToCollectColumnRecord$ = new rxjs.Observable((/**
             * @param {?} subscriber
             * @return {?}
             */
            function (subscriber) {
                _this.sendColumnNameRecord = subscriber;
            }));
        }
        /**
         * @param {?} formControlName
         * @return {?}
         */
        EtDataHandlerService.prototype.getColumnRecord = /**
         * @param {?} formControlName
         * @return {?}
         */
        function (formControlName) {
            // console.log('formControlName', formControlName);
            this.sendColumnNameRecord$.next(formControlName);
            // return this.sendColumnRecord$;
        };
        /**
         * @return {?}
         */
        EtDataHandlerService.prototype.getColumnNameToCollectColumnRecord = /**
         * @return {?}
         */
        function () {
            var _this = this;
            return new rxjs.Observable((/**
             * @param {?} subscriber
             * @return {?}
             */
            function (subscriber) {
                _this.sendColumnNameRecord = subscriber;
            }));
        };
        /**
         * @param {?} columnRecordList
         * @param {?} formControlName
         * @return {?}
         */
        EtDataHandlerService.prototype.emitColumnRecord = /**
         * @param {?} columnRecordList
         * @param {?} formControlName
         * @return {?}
         */
        function (columnRecordList, formControlName) {
            var _this = this;
            // console.log('columnRecordList', columnRecordList);
            setTimeout((/**
             * @return {?}
             */
            function () {
                _this.sendColumnRecord$.next({ recordList: columnRecordList, formControlName: formControlName });
            }), 1);
        };
        /**
         * @return {?}
         */
        EtDataHandlerService.prototype.validateTable = /**
         * @return {?}
         */
        function () {
            return this.validateTable$;
        };
        /**
         * @return {?}
         */
        EtDataHandlerService.prototype.getValidTableRecord = /**
         * @return {?}
         */
        function () {
            this.validateTable$.emit(true);
            return this.getTableRecord$;
        };
        /**
         * @param {?} record
         * @return {?}
         */
        EtDataHandlerService.prototype.sendTableRecord = /**
         * @param {?} record
         * @return {?}
         */
        function (record) {
            var _this = this;
            setTimeout((/**
             * @return {?}
             */
            function () {
                _this.getTableRecord$.emit(record);
            }), 100);
        };
        EtDataHandlerService.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        EtDataHandlerService.ctorParameters = function () { return []; };
        /** @nocollapse */ EtDataHandlerService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function EtDataHandlerService_Factory() { return new EtDataHandlerService(); }, token: EtDataHandlerService, providedIn: "root" });
        return EtDataHandlerService;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        EtDataHandlerService.prototype.sendColumnNameRecord;
        /** @type {?} */
        EtDataHandlerService.prototype.sendColumnNameRecord$;
        /** @type {?} */
        EtDataHandlerService.prototype.sendColumnRecord$;
        /**
         * @type {?}
         * @private
         */
        EtDataHandlerService.prototype.validateTable$;
        /**
         * @type {?}
         * @private
         */
        EtDataHandlerService.prototype.getTableRecord$;
        /**
         * @type {?}
         * @private
         */
        EtDataHandlerService.prototype.getColumnNameToCollectColumnRecord$;
        /**
         * @type {?}
         * @private
         */
        EtDataHandlerService.prototype.sendColumnRecord;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/et-form-handler.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var EtFormHandlerService = /** @class */ (function () {
        function EtFormHandlerService() {
        }
        /**
         * @param {?} validateFn
         * @return {?}
         */
        EtFormHandlerService.prototype.setValidators = /**
         * @param {?} validateFn
         * @return {?}
         */
        function (validateFn) {
        };
        EtFormHandlerService.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        EtFormHandlerService.ctorParameters = function () { return []; };
        /** @nocollapse */ EtFormHandlerService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function EtFormHandlerService_Factory() { return new EtFormHandlerService(); }, token: EtFormHandlerService, providedIn: "root" });
        return EtFormHandlerService;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/editable-table-config.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function TableConfig() { }
    if (false) {
        /** @type {?} */
        TableConfig.prototype.title;
        /** @type {?} */
        TableConfig.prototype.formControlName;
        /** @type {?|undefined} */
        TableConfig.prototype.key;
        /** @type {?} */
        TableConfig.prototype.inputField;
        /** @type {?|undefined} */
        TableConfig.prototype.isNeedValueChanges;
        /** @type {?|undefined} */
        TableConfig.prototype.displayKey;
        /** @type {?|undefined} */
        TableConfig.prototype.patchKey;
    }
    /**
     * @record
     */
    function PatchValue() { }
    if (false) {
        /** @type {?} */
        PatchValue.prototype.rowIndex;
        /** @type {?} */
        PatchValue.prototype.patchValue;
        /** @type {?|undefined} */
        PatchValue.prototype.tableRowId;
    }
    /**
     * @record
     */
    function AssignListToSelect() { }
    if (false) {
        /** @type {?} */
        AssignListToSelect.prototype.formControlName;
        /** @type {?} */
        AssignListToSelect.prototype.list;
    }
    /**
     * @record
     */
    function AssignListToAutocomplete() { }
    if (false) {
        /** @type {?} */
        AssignListToAutocomplete.prototype.config;
        /** @type {?} */
        AssignListToAutocomplete.prototype.list;
        /** @type {?|undefined} */
        AssignListToAutocomplete.prototype.searchKey;
    }
    /**
     * @record
     */
    function ControlsConfig() { }
    /**
     * @record
     */
    function ColumnRecord() { }
    if (false) {
        /** @type {?} */
        ColumnRecord.prototype.recordList;
        /** @type {?} */
        ColumnRecord.prototype.formControlName;
    }
    /**
     * @record
     * @template T
     */
    function EtTableValueChangeEvent() { }
    if (false) {
        /** @type {?} */
        EtTableValueChangeEvent.prototype.key;
        /** @type {?} */
        EtTableValueChangeEvent.prototype.config;
        /** @type {?} */
        EtTableValueChangeEvent.prototype.tableRow;
    }
    /**
     * @record
     * @template T
     */
    function EtAutocompleteSelectedEvent() { }
    if (false) {
        /** @type {?} */
        EtAutocompleteSelectedEvent.prototype.option;
        /** @type {?} */
        EtAutocompleteSelectedEvent.prototype.rowIndex;
        /** @type {?} */
        EtAutocompleteSelectedEvent.prototype.config;
        /** @type {?} */
        EtAutocompleteSelectedEvent.prototype.tableRow;
    }
    var ETValidationError = /** @class */ (function () {
        function ETValidationError(formControlName, tableRowId, msg, type) {
            this.formControlName = formControlName;
            this.tableRowId = tableRowId;
            this.msg = msg;
            this.type = type;
            if (!msg) {
                this.isRemove = true;
            }
            Object.seal(this);
        }
        return ETValidationError;
    }());
    if (false) {
        /** @type {?} */
        ETValidationError.prototype.isRemove;
        /** @type {?} */
        ETValidationError.prototype.formControlName;
        /** @type {?} */
        ETValidationError.prototype.tableRowId;
        /** @type {?} */
        ETValidationError.prototype.msg;
        /** @type {?} */
        ETValidationError.prototype.type;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/editable-table.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var EditableTableComponent = /** @class */ (function () {
        function EditableTableComponent(fb, etDataHandlerService, _changeDetectorRef) {
            var _this = this;
            this.fb = fb;
            this.etDataHandlerService = etDataHandlerService;
            this._changeDetectorRef = _changeDetectorRef;
            this.filteredOptions = {};
            this.selectOptions = {};
            this.subscription = new rxjs.Subscription();
            this.viewOnly = false;
            this.hideActionBtn = false;
            this.hideDefaultRow = false;
            this.tableConfig = (/** @type {?} */ ([]));
            this.getTableRecordChange = new core.EventEmitter();
            this.optionSelected = new core.EventEmitter();
            this.searchValueChanges = new core.EventEmitter();
            this.tableValueChanges = new core.EventEmitter();
            this.deletedTableRecords = new core.EventEmitter();
            this.matAutocompleteDisplayFn = new rxjs.Subject();
            /* ================  NG_VALUE_ACCESSOR && NG_VALIDATORS ===============*/
            this.onTouched = (/**
             * @return {?}
             */
            function () {
                _this.editableTableForm.markAllAsTouched();
            });
        }
        Object.defineProperty(EditableTableComponent.prototype, "assignListToSelect", {
            set: /**
             * @param {?} v
             * @return {?}
             */
            function (v) {
                var _this = this;
                if (!!v) {
                    v.forEach((/**
                     * @param {?} ele
                     * @return {?}
                     */
                    function (ele) {
                        _this.selectOptions[ele.formControlName] = ele.list;
                    }));
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EditableTableComponent.prototype, "getFormData", {
            set: /**
             * @param {?} v
             * @return {?}
             */
            function (v) {
                var _this = this;
                v.subscribe((/**
                 * @param {?} res
                 * @return {?}
                 */
                function (res) {
                    if (!!res) {
                        _this.getTableRecordChange.emit(((/** @type {?} */ (_this.editableTableForm.controls.table))).getRawValue());
                    }
                }));
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EditableTableComponent.prototype, "assignListToAutocomplete", {
            set: /**
             * @param {?} v
             * @return {?}
             */
            function (v) {
                if (v && v.config.inputField === 'autocomplete') {
                    this.filteredOptions[v.config.formControlName] = this._filter(v.searchKey, v.list, v.config.displayKey);
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EditableTableComponent.prototype, "patchValue", {
            set: /**
             * @param {?} v
             * @return {?}
             */
            function (v) {
                this._patchValue = v;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EditableTableComponent.prototype, "patchValueToSelect", {
            set: /**
             * @param {?} v
             * @return {?}
             */
            function (v) {
                if (!!v && this.editableTableForm) {
                    /** @type {?} */
                    var form_1 = ((/** @type {?} */ (this.editableTableForm.controls.table)));
                    v.forEach((/**
                     * @param {?} ele
                     * @return {?}
                     */
                    function (ele) {
                        if (form_1.controls[ele.rowIndex]) {
                            form_1.controls[ele.rowIndex].patchValue(ele.patchValue);
                        }
                    }));
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EditableTableComponent.prototype, "tableData", {
            set: /**
             * @param {?} v
             * @return {?}
             */
            function (v) {
                {
                    if (!!v && v.length > 0) {
                        this._tableData = v;
                        this.createTableWithData(v);
                    }
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EditableTableComponent.prototype, "formGroupForTableRow", {
            set: /**
             * @param {?} fb
             * @return {?}
             */
            function (fb) {
                this._formGroupForTableRow = __assign({}, fb);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EditableTableComponent.prototype, "validationError", {
            set: /**
             * @param {?} ve
             * @return {?}
             */
            function (ve) {
                if (ve) {
                    this._validationError = ve;
                    this.setError(ve);
                }
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @return {?}
         */
        EditableTableComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            this.createTableForm();
            this.subscribeToSendColumnNameRecord();
            this.subscribeToValidateTable();
            this.assignMarkAsTouchedFnToControl(this.control);
            // this.setQuotationListToTable();
        };
        /**
         * @param {?} changes
         * @return {?}
         */
        EditableTableComponent.prototype.ngOnChanges = /**
         * @param {?} changes
         * @return {?}
         */
        function (changes) {
            if (!!changes.patchValue && !!changes.patchValue.currentValue) {
                this._patchValue = changes.patchValue.currentValue;
                this.patchValueToTable(this._patchValue);
            }
            if (!!changes.control && !!changes.control.currentValue) {
                this.assignMarkAsTouchedFnToControl(this.control);
            }
        };
        /**
         * @param {?} control
         * @return {?}
         */
        EditableTableComponent.prototype.assignMarkAsTouchedFnToControl = /**
         * @param {?} control
         * @return {?}
         */
        function (control) {
            var _this = this;
            if (control) {
                control.markAsTouched = (/**
                 * @return {?}
                 */
                function () {
                    _this.editableTableForm.markAllAsTouched();
                });
            }
        };
        /**
         * @private
         * @param {?} v
         * @return {?}
         */
        EditableTableComponent.prototype.patchValueToTable = /**
         * @private
         * @param {?} v
         * @return {?}
         */
        function (v) {
            if (!!v && v.length > 0 && this.editableTableForm) {
                if (!this.editableTableForm) {
                    this.createTableForm();
                }
                /** @type {?} */
                var form_2 = ((/** @type {?} */ (this.editableTableForm.controls.table)));
                /** @type {?} */
                var rowIndex_1;
                v.forEach((/**
                 * @param {?} ele
                 * @return {?}
                 */
                function (ele) {
                    if (ele.tableRowId) {
                        for (var key in form_2.value) {
                            if (form_2.value.hasOwnProperty(key)) {
                                /** @type {?} */
                                var tableRow = form_2.value[key];
                                if (tableRow['tableRowId'] === ele.tableRowId) {
                                    rowIndex_1 = key;
                                }
                            }
                        }
                    }
                    rowIndex_1 = (!rowIndex_1 && ele.rowIndex) ? ele.rowIndex : rowIndex_1;
                    rowIndex_1 ? form_2.controls[rowIndex_1].patchValue(ele.patchValue) : console.error('patch row undefined');
                }));
            }
        };
        /**
         * @private
         * @param {?} tableData
         * @return {?}
         */
        EditableTableComponent.prototype.createTableWithData = /**
         * @private
         * @param {?} tableData
         * @return {?}
         */
        function (tableData) {
            var _this = this;
            tableData.forEach((/**
             * @param {?} element
             * @param {?} index
             * @return {?}
             */
            function (element, index) {
                if (index === 0) {
                    _this.patchValueToTable([{ rowIndex: '0', patchValue: element }]);
                    return;
                }
                _this.addRow(element);
            }));
        };
        // @Input() private tableData;
        // @Input() private tableData;
        /**
         * @private
         * @return {?}
         */
        EditableTableComponent.prototype.subscribeToSendColumnNameRecord = 
        // @Input() private tableData;
        /**
         * @private
         * @return {?}
         */
        function () {
            var _this = this;
            this.etDataHandlerService.sendColumnNameRecord$.subscribe((/**
             * @param {?} formControlName
             * @return {?}
             */
            function (formControlName) {
                if (_this.viewOnly) {
                    return;
                }
                /** @type {?} */
                var form = ((/** @type {?} */ (_this.editableTableForm.controls.table)));
                /** @type {?} */
                var columnValues = [];
                /** @type {?} */
                var tableRecord = form.getRawValue();
                if (formControlName.toLowerCase() === 'all') {
                    /** @type {?} */
                    var sortedConfigList = _this.tableConfig.filter((/**
                     * @param {?} config
                     * @return {?}
                     */
                    function (config) { return config.isNeedValueChanges; }));
                    sortedConfigList.forEach((/**
                     * @param {?} config
                     * @return {?}
                     */
                    function (config) {
                        columnValues = _this.collectColumnRecord(tableRecord, config.formControlName);
                        _this.etDataHandlerService.emitColumnRecord(columnValues, config.formControlName);
                    }));
                    return;
                }
                columnValues = _this.collectColumnRecord(tableRecord, formControlName);
                _this.etDataHandlerService.emitColumnRecord(columnValues, formControlName);
            }));
        };
        /**
         * @private
         * @param {?} tableRecord
         * @param {?} tableRowKey
         * @return {?}
         */
        EditableTableComponent.prototype.collectColumnRecord = /**
         * @private
         * @param {?} tableRecord
         * @param {?} tableRowKey
         * @return {?}
         */
        function (tableRecord, tableRowKey) {
            /** @type {?} */
            var columnValues = [];
            for (var key in tableRecord) {
                if (tableRecord.hasOwnProperty(key)) {
                    /** @type {?} */
                    var tableRow = tableRecord[key];
                    columnValues.push(tableRow[tableRowKey]);
                }
            }
            return columnValues;
        };
        /**
         * @private
         * @return {?}
         */
        EditableTableComponent.prototype.subscribeToValidateTable = /**
         * @private
         * @return {?}
         */
        function () {
            var _this = this;
            /** @type {?} */
            var validateSbscrb = this.etDataHandlerService.validateTable().subscribe((/**
             * @param {?} status
             * @return {?}
             */
            function (status) {
                if (status) {
                    _this.editableTableForm.markAllAsTouched();
                    if (_this.editableTableForm.status === 'VALID' || _this.editableTableForm.status === 'DISABLED') {
                        _this.etDataHandlerService.sendTableRecord({
                            data: ((/** @type {?} */ (_this.editableTableForm.controls.table))).getRawValue(),
                            valid: true
                        });
                        return;
                    }
                    _this.etDataHandlerService.sendTableRecord({
                        data: [],
                        valid: false
                    });
                }
            }));
            this.subscription.add(validateSbscrb);
        };
        /**
         * @return {?}
         */
        EditableTableComponent.prototype.createTableForm = /**
         * @return {?}
         */
        function () {
            if (this.editableTableForm) {
                return;
            }
            if (this.etFormArray) {
                this.editableTableForm = this.fb.group({
                    table: this.etFormArray
                });
            }
            else {
                this.editableTableForm = this.fb.group({
                    table: this.fb.array([])
                });
            }
            if (!this.hideDefaultRow
                && ((/** @type {?} */ (this.editableTableForm.controls.table))).length === 0
                && (this.tableConfig.length > 0 || this.etFormArray.length > 0)) {
                this.addRow();
            }
            this.tableValueChanges.emit(this.editableTableForm.valueChanges);
        };
        /**
         * @param {?=} data
         * @return {?}
         */
        EditableTableComponent.prototype.implementsDetailsRow = /**
         * @param {?=} data
         * @return {?}
         */
        function (data) {
            var _this = this;
            /** @type {?} */
            var fg = this.fb.group(this._formGroupForTableRow);
            if (!!data && !this.viewOnly) {
                fg.patchValue(data);
            }
            if (this.viewOnly) {
                fg.disable();
                !!data ? fg.patchValue(data, { onlySelf: true, emitEvent: false }) : null;
            }
            fg.addControl('tableRowId', this.fb.control(uuid.v4()));
            this.tableConfig.forEach((/**
             * @param {?} ele
             * @return {?}
             */
            function (ele) {
                // fg.valueCha nges
                if (ele.inputField === 'autocomplete' || ele.isNeedValueChanges) {
                    fg.controls[ele.formControlName].valueChanges.subscribe((/**
                     * @param {?} searchValue
                     * @return {?}
                     */
                    function (searchValue) {
                        _this.searchValueChanges.emit({ key: searchValue, config: ele, tableRow: fg.getRawValue() });
                        _this.dirtyAutocompleteColumn = __assign({}, ele);
                    }));
                }
            }));
            return fg;
        };
        /**
         * @private
         * @param {?} value
         * @param {?} options
         * @param {?} key
         * @return {?}
         */
        EditableTableComponent.prototype._filter = /**
         * @private
         * @param {?} value
         * @param {?} options
         * @param {?} key
         * @return {?}
         */
        function (value, options, key) {
            /** @type {?} */
            var filterValue;
            if (!!value || value === "") {
                return options;
            }
            if (typeof value === 'string') {
                filterValue = value.toLowerCase();
            }
            if (typeof value === 'object') {
                filterValue = value[key].toLowerCase();
            }
            return options.filter((/**
             * @param {?} option
             * @return {?}
             */
            function (option) { return option[key].toLowerCase().indexOf(filterValue) === 0; }));
        };
        /**
         * @return {?}
         */
        EditableTableComponent.prototype.submitData = /**
         * @return {?}
         */
        function () {
        };
        /**
         * @param {?=} row
         * @return {?}
         */
        EditableTableComponent.prototype.addRow = /**
         * @param {?=} row
         * @return {?}
         */
        function (row) {
            if (this.etFormArray) {
                this.editableTableForm.controls.table = this.etFormArray;
                return;
            }
            /** @type {?} */
            var implementsDetails = (/** @type {?} */ (this.editableTableForm.controls.table));
            implementsDetails.push(this.implementsDetailsRow(row));
        };
        /**
         * @param {?=} row
         * @return {?}
         */
        EditableTableComponent.prototype.resetTable = /**
         * @param {?=} row
         * @return {?}
         */
        function (row) {
            /** @type {?} */
            var implementsDetails = (/** @type {?} */ (this.editableTableForm.controls.table));
            implementsDetails.clear();
            implementsDetails.push(this.implementsDetailsRow(row));
        };
        /**
         * @return {?}
         */
        EditableTableComponent.prototype.deleteRow = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var implementsDetails = (/** @type {?} */ (this.editableTableForm.controls.table));
            /** @type {?} */
            var deleteRecordList = [];
            /** @type {?} */
            var nonSelected = implementsDetails.controls.filter((/**
             * @param {?} machinery
             * @return {?}
             */
            function (machinery) {
                if (machinery.value.isSelected) {
                    deleteRecordList.push(machinery.getRawValue());
                }
                return !machinery.value.isSelected;
            }));
            this.deletedTableRecords.emit(deleteRecordList);
            implementsDetails.clear();
            nonSelected.forEach((/**
             * @param {?} el
             * @return {?}
             */
            function (el) { return implementsDetails.push(el); }));
        };
        /**
         * @private
         * @param {?} config
         * @return {?}
         */
        EditableTableComponent.prototype.generateAutocompleteDisplayFn = /**
         * @private
         * @param {?} config
         * @return {?}
         */
        function (config) {
            /** @type {?} */
            var key = config.patchKey || config.displayKey;
            /** @type {?} */
            var displayWith = (/**
             * @param {?} user
             * @return {?}
             */
            function (user) {
                return (user && typeof user === 'object') ? user[key] : user;
            })
            // return of(displayWith);
            ;
            // return of(displayWith);
            this.matAutocompleteDisplayFn.next(displayWith);
            this._changeDetectorRef.detectChanges();
        };
        /**
         * @param {?} config
         * @param {?} formGroup
         * @return {?}
         */
        EditableTableComponent.prototype.autocompleteClicked = /**
         * @param {?} config
         * @param {?} formGroup
         * @return {?}
         */
        function (config, formGroup) {
            this.filteredOptions[config.formControlName] = [];
            this.generateAutocompleteDisplayFn(config);
            if (formGroup.controls[config.formControlName].value) {
                this.searchValueChanges.emit({ key: formGroup.controls[config.formControlName].value, config: config, tableRow: formGroup.getRawValue() });
                this.dirtyAutocompleteColumn = __assign({}, config);
            }
        };
        /**
         * @param {?} event
         * @param {?} rowIndex
         * @param {?} rowFormGroup
         * @param {?} config
         * @return {?}
         */
        EditableTableComponent.prototype.valueSelectedFromAutocomplete = /**
         * @param {?} event
         * @param {?} rowIndex
         * @param {?} rowFormGroup
         * @param {?} config
         * @return {?}
         */
        function (event, rowIndex, rowFormGroup, config) {
            /** @type {?} */
            var option = (event && typeof event.option.value === 'object') ? __assign({}, event.option.value) : event.option.value || '';
            this.optionSelected.emit(__assign({ option: option }, { rowIndex: rowIndex, tableRow: rowFormGroup.getRawValue(), config: config }));
        };
        /**
         * @param {?} event
         * @return {?}
         */
        EditableTableComponent.prototype.selectionChanged = /**
         * @param {?} event
         * @return {?}
         */
        function (event) {
        };
        /**
         * @param {?} value
         * @param {?} rowFormGroup
         * @param {?} formControlName
         * @return {?}
         */
        EditableTableComponent.prototype.contenteditableValuechange = /**
         * @param {?} value
         * @param {?} rowFormGroup
         * @param {?} formControlName
         * @return {?}
         */
        function (value, rowFormGroup, formControlName) {
            rowFormGroup.controls[formControlName].patchValue(value);
        };
        /**
         * @private
         * @param {?} ve
         * @return {?}
         */
        EditableTableComponent.prototype.setError = /**
         * @private
         * @param {?} ve
         * @return {?}
         */
        function (ve) {
            /** @type {?} */
            var formArray = ((/** @type {?} */ (this.editableTableForm.get('table'))));
            /** @type {?} */
            var formIndex = formArray.getRawValue().findIndex((/**
             * @param {?} formValue
             * @return {?}
             */
            function (formValue) {
                if (formValue.tableRowId === ve.tableRowId) {
                    return true;
                }
            }));
            /** @type {?} */
            var tableRowFG = formArray.at(formIndex);
            if (tableRowFG) {
                if (ve.isRemove) {
                    tableRowFG.get(ve.formControlName).setErrors(null);
                    return;
                }
                tableRowFG.get(ve.formControlName).setErrors(__assign({ msg: ve.msg }, ve.type));
            }
        };
        /**
         * @param {?} val
         * @return {?}
         */
        EditableTableComponent.prototype.writeValue = /**
         * @param {?} val
         * @return {?}
         */
        function (val) {
            if (val && val.resetForm) {
                this.editableTableForm.reset();
                this.resetTable();
                return;
            }
            val && val.length > 0 && this.createTableWithData(val);
        };
        /**
         * @param {?} fn
         * @return {?}
         */
        EditableTableComponent.prototype.registerOnChange = /**
         * @param {?} fn
         * @return {?}
         */
        function (fn) {
            var _this = this;
            fn && this.editableTableForm.controls.table.valueChanges.pipe(operators.map((/**
             * @param {?} value
             * @return {?}
             */
            function (value) { return ((/** @type {?} */ (_this.editableTableForm.controls.table))).getRawValue(); }))).subscribe(fn);
        };
        /**
         * @param {?} fn
         * @return {?}
         */
        EditableTableComponent.prototype.registerOnTouched = /**
         * @param {?} fn
         * @return {?}
         */
        function (fn) {
            fn && (this.onTouched = fn);
        };
        /**
         * @param {?} isDisabled
         * @return {?}
         */
        EditableTableComponent.prototype.setDisabledState = /**
         * @param {?} isDisabled
         * @return {?}
         */
        function (isDisabled) {
            isDisabled ? this.editableTableForm.controls.table.disable() : this.editableTableForm.controls.table.enable();
            // throw new Error("Method not implemented.");
        };
        /**
         * @param {?} control
         * @return {?}
         */
        EditableTableComponent.prototype.validate = /**
         * @param {?} control
         * @return {?}
         */
        function (control) {
            return this.editableTableForm.controls.table.valid ? null : { invalidForm: { valid: false, message: "editableTableForm fields are invalid" } };
        };
        /* ==================================================================== */
        /* ==================================================================== */
        /**
         * @return {?}
         */
        EditableTableComponent.prototype.ngOnDestroy = /* ==================================================================== */
        /**
         * @return {?}
         */
        function () {
            this.subscription.unsubscribe();
        };
        EditableTableComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'editable-table',
                        template: "<div id=\"divshow\">\n  <form [formGroup]=\"editableTableForm\">\n    <div>\n      <div class=\"table-responsive\">\n        <table class=\"table table-bordered table-sm\" formArrayName=\"table\">\n          <thead class=\"thead-light\">\n            <tr>\n              <th scope=\"col\" style=\"width: 10px;\" *ngFor=\"let config of tableConfig\">{{config.title}}</th>\n            </tr>\n          </thead>\n          <tbody>\n            <ng-container *ngFor=\"let rowFormGroup of editableTableForm['controls'].table['controls']; let i = index\"\n              [formGroupName]=\"i\">\n              <tr>\n                <ng-container *ngFor=\"let config of tableConfig\">\n                  <td *ngIf=\"config.inputField !== 'contenteditable'\">\n                    <mat-checkbox *ngIf=\"config.inputField === 'checkbox'\" color=\"basic\"\n                      class=\"form-control form-control-sm\" [formControlName]=\"config.formControlName\" [checked]=\"false\">\n                    </mat-checkbox>\n                    <mat-form-field class=\"example-full-width\" *ngIf=\"config.inputField === 'input'\"\n                      appearance=\"outline\" [ngClass]=\"{pb_0:!rowFormGroup.get(config.formControlName).errors}\">\n                      <input matInput [placeholder]=\"config.title\" [formControlName]=\"config.formControlName\">\n                      <mat-error\n                        *ngIf=\"rowFormGroup.get(config.formControlName).errors && rowFormGroup.get(config.formControlName).errors.msg &&!rowFormGroup.get(config.formControlName).errors.required\">\n                        {{rowFormGroup.get(config.formControlName).errors.msg}}</mat-error>\n                      <mat-error\n                        *ngIf=\"rowFormGroup.get(config.formControlName).errors && !rowFormGroup.get(config.formControlName).errors.msg &&rowFormGroup.get(config.formControlName).errors.required\">\n                        This field is required</mat-error>\n                    </mat-form-field>\n                    <mat-form-field *ngIf=\"config.inputField === 'autocomplete'\" class=\"w-100\"\n                      [ngClass]=\"{pb_0:!rowFormGroup.get(config.formControlName).errors}\" appearance=\"outline\">\n                      <input matInput [formControlName]=\"config.formControlName\" [matAutocomplete]=\"autoAmcNumber\"\n                        (focus)=\"autocompleteClicked(config, rowFormGroup)\" (keyup.enter)=\"$event.preventDefault();\">\n                      <!-- (blur)=\"resetFormControlNotHavingObject(searchAMCForm.controls.amcNumber, 'amcNumber')\" -->\n                      <mat-autocomplete #autoAmcNumber=\"matAutocomplete\"\n                        [displayWith]=\"matAutocompleteDisplayFn | async\"\n                        (optionSelected)=\"valueSelectedFromAutocomplete($event, i, rowFormGroup, config)\">\n                        <mat-option *ngFor=\"let option of filteredOptions[config.formControlName]\" [value]=\"option\">\n                          <!-- (click)=\"valueSelectedFromAutocomplete(option)\" -->\n                          {{option[config.displayKey]? option[config.displayKey] : option}}\n                        </mat-option>\n                      </mat-autocomplete>\n                      <mat-icon matSuffix>search</mat-icon>\n                      <mat-error\n                        *ngIf=\"rowFormGroup.get(config.formControlName).errors && rowFormGroup.get(config.formControlName).errors.msg &&!rowFormGroup.get(config.formControlName).errors.required\">\n                        {{rowFormGroup.get(config.formControlName).errors.msg}}</mat-error>\n                      <mat-error\n                        *ngIf=\"rowFormGroup.get(config.formControlName).errors && !rowFormGroup.get(config.formControlName).errors.msg &&rowFormGroup.get(config.formControlName).errors.required\">\n                        This field is required</mat-error>\n                    </mat-form-field>\n                    <!-- <span *ngIf=\"config.inputField === 'autocomplete'\">\n                      {{matAutocompleteDisplayFn | async}}\n                    </span> -->\n                    <mat-form-field *ngIf=\"config.inputField === 'select'\" appearance=\"outline\" class=\"pb_0\">\n                      <mat-label>Select {{config.title}}</mat-label>\n                      <mat-select [formControlName]=\"config.formControlName\"\n                        (selectionChange)=\"selectionChanged($event)\">\n                        <mat-option *ngFor=\"let option of selectOptions[config.formControlName]\" [value]=\"option\">\n                          {{option[config.displayKey]? option[config.displayKey] : option}}\n                        </mat-option>\n                      </mat-select>\n                      <mat-error\n                      *ngIf=\"rowFormGroup.get(config.formControlName).errors && rowFormGroup.get(config.formControlName).errors.msg &&!rowFormGroup.get(config.formControlName).errors.required\">\n                      {{rowFormGroup.get(config.formControlName).errors.msg}}</mat-error>\n                      <mat-error\n                        *ngIf=\"rowFormGroup.get(config.formControlName).errors && !rowFormGroup.get(config.formControlName).errors.msg &&rowFormGroup.get(config.formControlName).errors.required\">\n                        This field is required</mat-error>\n                    </mat-form-field>\n                  </td>\n                  <td *ngIf=\"config.inputField === 'contenteditable'\"\n                    [attr.contenteditable]=\"config.inputField !== 'contenteditable' || config.isNeedValueChanges\"\n                    [textContent]=\"rowFormGroup.controls[config.formControlName].value\"\n                    (input)=\"contenteditableValuechange($event.target.textContent,rowFormGroup,config.formControlName)\"\n                    #contenteditableRef></td>\n                </ng-container>\n              </tr>\n              <!-- <ng-content select=\"tr\"></ng-content> -->\n            </ng-container>\n          </tbody>\n          <tfoot>\n            <ng-content select=\"ng-container.table_footer\"></ng-content>\n          </tfoot>\n        </table>\n      </div>\n    </div>\n  </form>\n  <div class=\" text-center mt-2\" *ngIf=\"!hideActionBtn && !viewOnly \">\n    <button type=\"button\" class=\"btn btn-sm btn_primary \" (click)=\"addRow()\"> Add\n      Row</button>\n    <button type=\"button\" class=\"btn btn-sm btn_secondary ml-2 \" (click)=\"deleteRow()\"> Delete Row </button>\n  </div>\n</div>",
                        providers: [
                            {
                                provide: forms.NG_VALUE_ACCESSOR,
                                useExisting: core.forwardRef((/**
                                 * @return {?}
                                 */
                                function () { return EditableTableComponent; })),
                                multi: true
                            },
                            {
                                provide: forms.NG_VALIDATORS,
                                useExisting: core.forwardRef((/**
                                 * @return {?}
                                 */
                                function () { return EditableTableComponent; })),
                                multi: true
                            }
                        ],
                        styles: [""]
                    }] }
        ];
        /** @nocollapse */
        EditableTableComponent.ctorParameters = function () { return [
            { type: forms.FormBuilder },
            { type: EtDataHandlerService },
            { type: core.ChangeDetectorRef }
        ]; };
        EditableTableComponent.propDecorators = {
            viewOnly: [{ type: core.Input }],
            hideActionBtn: [{ type: core.Input }],
            hideDefaultRow: [{ type: core.Input }],
            assignListToSelect: [{ type: core.Input }],
            tableConfig: [{ type: core.Input }],
            getFormData: [{ type: core.Input }],
            getTableRecordChange: [{ type: core.Output }],
            optionSelected: [{ type: core.Output }],
            assignListToAutocomplete: [{ type: core.Input }],
            patchValue: [{ type: core.Input }],
            patchValueToSelect: [{ type: core.Input }],
            tableData: [{ type: core.Input, args: ['tableData',] }],
            searchValueChanges: [{ type: core.Output }],
            tableValueChanges: [{ type: core.Output }],
            formGroupForTableRow: [{ type: core.Input }],
            deletedTableRecords: [{ type: core.Output }],
            matAutocompleteDisplayFn: [{ type: core.Input }],
            etFormArray: [{ type: core.Input }],
            control: [{ type: core.Input }],
            validationError: [{ type: core.Input }]
        };
        return EditableTableComponent;
    }());
    if (false) {
        /** @type {?} */
        EditableTableComponent.prototype.editableTableForm;
        /** @type {?} */
        EditableTableComponent.prototype.isEdit;
        /** @type {?} */
        EditableTableComponent.prototype.isView;
        /** @type {?} */
        EditableTableComponent.prototype.filteredOptions;
        /** @type {?} */
        EditableTableComponent.prototype.selectOptions;
        /** @type {?} */
        EditableTableComponent.prototype.subscription;
        /** @type {?} */
        EditableTableComponent.prototype.viewOnly;
        /** @type {?} */
        EditableTableComponent.prototype.hideActionBtn;
        /** @type {?} */
        EditableTableComponent.prototype.hideDefaultRow;
        /** @type {?} */
        EditableTableComponent.prototype.tableConfig;
        /** @type {?} */
        EditableTableComponent.prototype.getTableRecordChange;
        /** @type {?} */
        EditableTableComponent.prototype.optionSelected;
        /**
         * @type {?}
         * @private
         */
        EditableTableComponent.prototype._patchValue;
        /**
         * @type {?}
         * @private
         */
        EditableTableComponent.prototype.dirtyAutocompleteColumn;
        /**
         * @type {?}
         * @private
         */
        EditableTableComponent.prototype._tableData;
        /**
         * @type {?}
         * @private
         */
        EditableTableComponent.prototype._formGroupForTableRow;
        /** @type {?} */
        EditableTableComponent.prototype.searchValueChanges;
        /** @type {?} */
        EditableTableComponent.prototype.tableValueChanges;
        /** @type {?} */
        EditableTableComponent.prototype.deletedTableRecords;
        /** @type {?} */
        EditableTableComponent.prototype.matAutocompleteDisplayFn;
        /** @type {?} */
        EditableTableComponent.prototype.etFormArray;
        /** @type {?} */
        EditableTableComponent.prototype.control;
        /**
         * @type {?}
         * @private
         */
        EditableTableComponent.prototype._validationError;
        /** @type {?} */
        EditableTableComponent.prototype.onTouched;
        /**
         * @type {?}
         * @private
         */
        EditableTableComponent.prototype.fb;
        /**
         * @type {?}
         * @private
         */
        EditableTableComponent.prototype.etDataHandlerService;
        /**
         * @type {?}
         * @private
         */
        EditableTableComponent.prototype._changeDetectorRef;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/editable-table.module.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var EditableTableModule = /** @class */ (function () {
        function EditableTableModule() {
        }
        EditableTableModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [EditableTableComponent],
                        imports: [
                            common.CommonModule,
                            forms.FormsModule,
                            forms.ReactiveFormsModule,
                            material.MatCheckboxModule,
                            material.MatAutocompleteModule,
                            material.MatFormFieldModule,
                            material.MatInputModule,
                            material.MatIconModule,
                            select.MatSelectModule
                        ],
                        exports: [EditableTableComponent]
                    },] }
        ];
        return EditableTableModule;
    }());

    exports.ETValidationError = ETValidationError;
    exports.EditableTableComponent = EditableTableComponent;
    exports.EditableTableModule = EditableTableModule;
    exports.EtDataHandlerService = EtDataHandlerService;
    exports.EtFormHandlerService = EtFormHandlerService;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=editable-table.umd.js.map
