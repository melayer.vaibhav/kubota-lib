/**
 * @fileoverview added by tsickle
 * Generated from: lib/editable-table.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, Input, Output, EventEmitter, forwardRef, ChangeDetectorRef } from '@angular/core';
import { FormArray, FormBuilder, NG_VALUE_ACCESSOR, NG_VALIDATORS, FormControl } from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { ETValidationError } from './editable-table-config';
import * as uuid from 'uuid';
import { EtDataHandlerService } from './et-data-handler.service';
var EditableTableComponent = /** @class */ (function () {
    function EditableTableComponent(fb, etDataHandlerService, _changeDetectorRef) {
        var _this = this;
        this.fb = fb;
        this.etDataHandlerService = etDataHandlerService;
        this._changeDetectorRef = _changeDetectorRef;
        this.filteredOptions = {};
        this.selectOptions = {};
        this.subscription = new Subscription();
        this.viewOnly = false;
        this.hideActionBtn = false;
        this.hideDefaultRow = false;
        this.tableConfig = (/** @type {?} */ ([]));
        this.getTableRecordChange = new EventEmitter();
        this.optionSelected = new EventEmitter();
        this.searchValueChanges = new EventEmitter();
        this.tableValueChanges = new EventEmitter();
        this.deletedTableRecords = new EventEmitter();
        this.matAutocompleteDisplayFn = new Subject();
        /* ================  NG_VALUE_ACCESSOR && NG_VALIDATORS ===============*/
        this.onTouched = (/**
         * @return {?}
         */
        function () {
            _this.editableTableForm.markAllAsTouched();
        });
    }
    Object.defineProperty(EditableTableComponent.prototype, "assignListToSelect", {
        set: /**
         * @param {?} v
         * @return {?}
         */
        function (v) {
            var _this = this;
            if (!!v) {
                v.forEach((/**
                 * @param {?} ele
                 * @return {?}
                 */
                function (ele) {
                    _this.selectOptions[ele.formControlName] = ele.list;
                }));
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EditableTableComponent.prototype, "getFormData", {
        set: /**
         * @param {?} v
         * @return {?}
         */
        function (v) {
            var _this = this;
            v.subscribe((/**
             * @param {?} res
             * @return {?}
             */
            function (res) {
                if (!!res) {
                    _this.getTableRecordChange.emit(((/** @type {?} */ (_this.editableTableForm.controls.table))).getRawValue());
                }
            }));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EditableTableComponent.prototype, "assignListToAutocomplete", {
        set: /**
         * @param {?} v
         * @return {?}
         */
        function (v) {
            if (v && v.config.inputField === 'autocomplete') {
                this.filteredOptions[v.config.formControlName] = this._filter(v.searchKey, v.list, v.config.displayKey);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EditableTableComponent.prototype, "patchValue", {
        set: /**
         * @param {?} v
         * @return {?}
         */
        function (v) {
            this._patchValue = v;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EditableTableComponent.prototype, "patchValueToSelect", {
        set: /**
         * @param {?} v
         * @return {?}
         */
        function (v) {
            if (!!v && this.editableTableForm) {
                /** @type {?} */
                var form_1 = ((/** @type {?} */ (this.editableTableForm.controls.table)));
                v.forEach((/**
                 * @param {?} ele
                 * @return {?}
                 */
                function (ele) {
                    if (form_1.controls[ele.rowIndex]) {
                        form_1.controls[ele.rowIndex].patchValue(ele.patchValue);
                    }
                }));
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EditableTableComponent.prototype, "tableData", {
        set: /**
         * @param {?} v
         * @return {?}
         */
        function (v) {
            {
                if (!!v && v.length > 0) {
                    this._tableData = v;
                    this.createTableWithData(v);
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EditableTableComponent.prototype, "formGroupForTableRow", {
        set: /**
         * @param {?} fb
         * @return {?}
         */
        function (fb) {
            this._formGroupForTableRow = tslib_1.__assign({}, fb);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EditableTableComponent.prototype, "validationError", {
        set: /**
         * @param {?} ve
         * @return {?}
         */
        function (ve) {
            if (ve) {
                this._validationError = ve;
                this.setError(ve);
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    EditableTableComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.createTableForm();
        this.subscribeToSendColumnNameRecord();
        this.subscribeToValidateTable();
        this.assignMarkAsTouchedFnToControl(this.control);
        // this.setQuotationListToTable();
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    EditableTableComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (!!changes.patchValue && !!changes.patchValue.currentValue) {
            this._patchValue = changes.patchValue.currentValue;
            this.patchValueToTable(this._patchValue);
        }
        if (!!changes.control && !!changes.control.currentValue) {
            this.assignMarkAsTouchedFnToControl(this.control);
        }
    };
    /**
     * @param {?} control
     * @return {?}
     */
    EditableTableComponent.prototype.assignMarkAsTouchedFnToControl = /**
     * @param {?} control
     * @return {?}
     */
    function (control) {
        var _this = this;
        if (control) {
            control.markAsTouched = (/**
             * @return {?}
             */
            function () {
                _this.editableTableForm.markAllAsTouched();
            });
        }
    };
    /**
     * @private
     * @param {?} v
     * @return {?}
     */
    EditableTableComponent.prototype.patchValueToTable = /**
     * @private
     * @param {?} v
     * @return {?}
     */
    function (v) {
        if (!!v && v.length > 0 && this.editableTableForm) {
            if (!this.editableTableForm) {
                this.createTableForm();
            }
            /** @type {?} */
            var form_2 = ((/** @type {?} */ (this.editableTableForm.controls.table)));
            /** @type {?} */
            var rowIndex_1;
            v.forEach((/**
             * @param {?} ele
             * @return {?}
             */
            function (ele) {
                if (ele.tableRowId) {
                    for (var key in form_2.value) {
                        if (form_2.value.hasOwnProperty(key)) {
                            /** @type {?} */
                            var tableRow = form_2.value[key];
                            if (tableRow['tableRowId'] === ele.tableRowId) {
                                rowIndex_1 = key;
                            }
                        }
                    }
                }
                rowIndex_1 = (!rowIndex_1 && ele.rowIndex) ? ele.rowIndex : rowIndex_1;
                rowIndex_1 ? form_2.controls[rowIndex_1].patchValue(ele.patchValue) : console.error('patch row undefined');
            }));
        }
    };
    /**
     * @private
     * @param {?} tableData
     * @return {?}
     */
    EditableTableComponent.prototype.createTableWithData = /**
     * @private
     * @param {?} tableData
     * @return {?}
     */
    function (tableData) {
        var _this = this;
        tableData.forEach((/**
         * @param {?} element
         * @param {?} index
         * @return {?}
         */
        function (element, index) {
            if (index === 0) {
                _this.patchValueToTable([{ rowIndex: '0', patchValue: element }]);
                return;
            }
            _this.addRow(element);
        }));
    };
    // @Input() private tableData;
    // @Input() private tableData;
    /**
     * @private
     * @return {?}
     */
    EditableTableComponent.prototype.subscribeToSendColumnNameRecord = 
    // @Input() private tableData;
    /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        this.etDataHandlerService.sendColumnNameRecord$.subscribe((/**
         * @param {?} formControlName
         * @return {?}
         */
        function (formControlName) {
            if (_this.viewOnly) {
                return;
            }
            /** @type {?} */
            var form = ((/** @type {?} */ (_this.editableTableForm.controls.table)));
            /** @type {?} */
            var columnValues = [];
            /** @type {?} */
            var tableRecord = form.getRawValue();
            if (formControlName.toLowerCase() === 'all') {
                /** @type {?} */
                var sortedConfigList = _this.tableConfig.filter((/**
                 * @param {?} config
                 * @return {?}
                 */
                function (config) { return config.isNeedValueChanges; }));
                sortedConfigList.forEach((/**
                 * @param {?} config
                 * @return {?}
                 */
                function (config) {
                    columnValues = _this.collectColumnRecord(tableRecord, config.formControlName);
                    _this.etDataHandlerService.emitColumnRecord(columnValues, config.formControlName);
                }));
                return;
            }
            columnValues = _this.collectColumnRecord(tableRecord, formControlName);
            _this.etDataHandlerService.emitColumnRecord(columnValues, formControlName);
        }));
    };
    /**
     * @private
     * @param {?} tableRecord
     * @param {?} tableRowKey
     * @return {?}
     */
    EditableTableComponent.prototype.collectColumnRecord = /**
     * @private
     * @param {?} tableRecord
     * @param {?} tableRowKey
     * @return {?}
     */
    function (tableRecord, tableRowKey) {
        /** @type {?} */
        var columnValues = [];
        for (var key in tableRecord) {
            if (tableRecord.hasOwnProperty(key)) {
                /** @type {?} */
                var tableRow = tableRecord[key];
                columnValues.push(tableRow[tableRowKey]);
            }
        }
        return columnValues;
    };
    /**
     * @private
     * @return {?}
     */
    EditableTableComponent.prototype.subscribeToValidateTable = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var validateSbscrb = this.etDataHandlerService.validateTable().subscribe((/**
         * @param {?} status
         * @return {?}
         */
        function (status) {
            if (status) {
                _this.editableTableForm.markAllAsTouched();
                if (_this.editableTableForm.status === 'VALID' || _this.editableTableForm.status === 'DISABLED') {
                    _this.etDataHandlerService.sendTableRecord({
                        data: ((/** @type {?} */ (_this.editableTableForm.controls.table))).getRawValue(),
                        valid: true
                    });
                    return;
                }
                _this.etDataHandlerService.sendTableRecord({
                    data: [],
                    valid: false
                });
            }
        }));
        this.subscription.add(validateSbscrb);
    };
    /**
     * @return {?}
     */
    EditableTableComponent.prototype.createTableForm = /**
     * @return {?}
     */
    function () {
        if (this.editableTableForm) {
            return;
        }
        if (this.etFormArray) {
            this.editableTableForm = this.fb.group({
                table: this.etFormArray
            });
        }
        else {
            this.editableTableForm = this.fb.group({
                table: this.fb.array([])
            });
        }
        if (!this.hideDefaultRow
            && ((/** @type {?} */ (this.editableTableForm.controls.table))).length === 0
            && (this.tableConfig.length > 0 || this.etFormArray.length > 0)) {
            this.addRow();
        }
        this.tableValueChanges.emit(this.editableTableForm.valueChanges);
    };
    /**
     * @param {?=} data
     * @return {?}
     */
    EditableTableComponent.prototype.implementsDetailsRow = /**
     * @param {?=} data
     * @return {?}
     */
    function (data) {
        var _this = this;
        /** @type {?} */
        var fg = this.fb.group(this._formGroupForTableRow);
        if (!!data && !this.viewOnly) {
            fg.patchValue(data);
        }
        if (this.viewOnly) {
            fg.disable();
            !!data ? fg.patchValue(data, { onlySelf: true, emitEvent: false }) : null;
        }
        fg.addControl('tableRowId', this.fb.control(uuid.v4()));
        this.tableConfig.forEach((/**
         * @param {?} ele
         * @return {?}
         */
        function (ele) {
            // fg.valueCha nges
            if (ele.inputField === 'autocomplete' || ele.isNeedValueChanges) {
                fg.controls[ele.formControlName].valueChanges.subscribe((/**
                 * @param {?} searchValue
                 * @return {?}
                 */
                function (searchValue) {
                    _this.searchValueChanges.emit({ key: searchValue, config: ele, tableRow: fg.getRawValue() });
                    _this.dirtyAutocompleteColumn = tslib_1.__assign({}, ele);
                }));
            }
        }));
        return fg;
    };
    /**
     * @private
     * @param {?} value
     * @param {?} options
     * @param {?} key
     * @return {?}
     */
    EditableTableComponent.prototype._filter = /**
     * @private
     * @param {?} value
     * @param {?} options
     * @param {?} key
     * @return {?}
     */
    function (value, options, key) {
        /** @type {?} */
        var filterValue;
        if (!!value || value === "") {
            return options;
        }
        if (typeof value === 'string') {
            filterValue = value.toLowerCase();
        }
        if (typeof value === 'object') {
            filterValue = value[key].toLowerCase();
        }
        return options.filter((/**
         * @param {?} option
         * @return {?}
         */
        function (option) { return option[key].toLowerCase().indexOf(filterValue) === 0; }));
    };
    /**
     * @return {?}
     */
    EditableTableComponent.prototype.submitData = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @param {?=} row
     * @return {?}
     */
    EditableTableComponent.prototype.addRow = /**
     * @param {?=} row
     * @return {?}
     */
    function (row) {
        if (this.etFormArray) {
            this.editableTableForm.controls.table = this.etFormArray;
            return;
        }
        /** @type {?} */
        var implementsDetails = (/** @type {?} */ (this.editableTableForm.controls.table));
        implementsDetails.push(this.implementsDetailsRow(row));
    };
    /**
     * @param {?=} row
     * @return {?}
     */
    EditableTableComponent.prototype.resetTable = /**
     * @param {?=} row
     * @return {?}
     */
    function (row) {
        /** @type {?} */
        var implementsDetails = (/** @type {?} */ (this.editableTableForm.controls.table));
        implementsDetails.clear();
        implementsDetails.push(this.implementsDetailsRow(row));
    };
    /**
     * @return {?}
     */
    EditableTableComponent.prototype.deleteRow = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var implementsDetails = (/** @type {?} */ (this.editableTableForm.controls.table));
        /** @type {?} */
        var deleteRecordList = [];
        /** @type {?} */
        var nonSelected = implementsDetails.controls.filter((/**
         * @param {?} machinery
         * @return {?}
         */
        function (machinery) {
            if (machinery.value.isSelected) {
                deleteRecordList.push(machinery.getRawValue());
            }
            return !machinery.value.isSelected;
        }));
        this.deletedTableRecords.emit(deleteRecordList);
        implementsDetails.clear();
        nonSelected.forEach((/**
         * @param {?} el
         * @return {?}
         */
        function (el) { return implementsDetails.push(el); }));
    };
    /**
     * @private
     * @param {?} config
     * @return {?}
     */
    EditableTableComponent.prototype.generateAutocompleteDisplayFn = /**
     * @private
     * @param {?} config
     * @return {?}
     */
    function (config) {
        /** @type {?} */
        var key = config.patchKey || config.displayKey;
        /** @type {?} */
        var displayWith = (/**
         * @param {?} user
         * @return {?}
         */
        function (user) {
            return (user && typeof user === 'object') ? user[key] : user;
        })
        // return of(displayWith);
        ;
        // return of(displayWith);
        this.matAutocompleteDisplayFn.next(displayWith);
        this._changeDetectorRef.detectChanges();
    };
    /**
     * @param {?} config
     * @param {?} formGroup
     * @return {?}
     */
    EditableTableComponent.prototype.autocompleteClicked = /**
     * @param {?} config
     * @param {?} formGroup
     * @return {?}
     */
    function (config, formGroup) {
        this.filteredOptions[config.formControlName] = [];
        this.generateAutocompleteDisplayFn(config);
        if (formGroup.controls[config.formControlName].value) {
            this.searchValueChanges.emit({ key: formGroup.controls[config.formControlName].value, config: config, tableRow: formGroup.getRawValue() });
            this.dirtyAutocompleteColumn = tslib_1.__assign({}, config);
        }
    };
    /**
     * @param {?} event
     * @param {?} rowIndex
     * @param {?} rowFormGroup
     * @param {?} config
     * @return {?}
     */
    EditableTableComponent.prototype.valueSelectedFromAutocomplete = /**
     * @param {?} event
     * @param {?} rowIndex
     * @param {?} rowFormGroup
     * @param {?} config
     * @return {?}
     */
    function (event, rowIndex, rowFormGroup, config) {
        /** @type {?} */
        var option = (event && typeof event.option.value === 'object') ? tslib_1.__assign({}, event.option.value) : event.option.value || '';
        this.optionSelected.emit(tslib_1.__assign({ option: option }, { rowIndex: rowIndex, tableRow: rowFormGroup.getRawValue(), config: config }));
    };
    /**
     * @param {?} event
     * @return {?}
     */
    EditableTableComponent.prototype.selectionChanged = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
    };
    /**
     * @param {?} value
     * @param {?} rowFormGroup
     * @param {?} formControlName
     * @return {?}
     */
    EditableTableComponent.prototype.contenteditableValuechange = /**
     * @param {?} value
     * @param {?} rowFormGroup
     * @param {?} formControlName
     * @return {?}
     */
    function (value, rowFormGroup, formControlName) {
        rowFormGroup.controls[formControlName].patchValue(value);
    };
    /**
     * @private
     * @param {?} ve
     * @return {?}
     */
    EditableTableComponent.prototype.setError = /**
     * @private
     * @param {?} ve
     * @return {?}
     */
    function (ve) {
        /** @type {?} */
        var formArray = ((/** @type {?} */ (this.editableTableForm.get('table'))));
        /** @type {?} */
        var formIndex = formArray.getRawValue().findIndex((/**
         * @param {?} formValue
         * @return {?}
         */
        function (formValue) {
            if (formValue.tableRowId === ve.tableRowId) {
                return true;
            }
        }));
        /** @type {?} */
        var tableRowFG = formArray.at(formIndex);
        if (tableRowFG) {
            if (ve.isRemove) {
                tableRowFG.get(ve.formControlName).setErrors(null);
                return;
            }
            tableRowFG.get(ve.formControlName).setErrors(tslib_1.__assign({ msg: ve.msg }, ve.type));
        }
    };
    /**
     * @param {?} val
     * @return {?}
     */
    EditableTableComponent.prototype.writeValue = /**
     * @param {?} val
     * @return {?}
     */
    function (val) {
        if (val && val.resetForm) {
            this.editableTableForm.reset();
            this.resetTable();
            return;
        }
        val && val.length > 0 && this.createTableWithData(val);
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    EditableTableComponent.prototype.registerOnChange = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        var _this = this;
        fn && this.editableTableForm.controls.table.valueChanges.pipe(map((/**
         * @param {?} value
         * @return {?}
         */
        function (value) { return ((/** @type {?} */ (_this.editableTableForm.controls.table))).getRawValue(); }))).subscribe(fn);
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    EditableTableComponent.prototype.registerOnTouched = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        fn && (this.onTouched = fn);
    };
    /**
     * @param {?} isDisabled
     * @return {?}
     */
    EditableTableComponent.prototype.setDisabledState = /**
     * @param {?} isDisabled
     * @return {?}
     */
    function (isDisabled) {
        isDisabled ? this.editableTableForm.controls.table.disable() : this.editableTableForm.controls.table.enable();
        // throw new Error("Method not implemented.");
    };
    /**
     * @param {?} control
     * @return {?}
     */
    EditableTableComponent.prototype.validate = /**
     * @param {?} control
     * @return {?}
     */
    function (control) {
        return this.editableTableForm.controls.table.valid ? null : { invalidForm: { valid: false, message: "editableTableForm fields are invalid" } };
    };
    /* ==================================================================== */
    /* ==================================================================== */
    /**
     * @return {?}
     */
    EditableTableComponent.prototype.ngOnDestroy = /* ==================================================================== */
    /**
     * @return {?}
     */
    function () {
        this.subscription.unsubscribe();
    };
    EditableTableComponent.decorators = [
        { type: Component, args: [{
                    selector: 'editable-table',
                    template: "<div id=\"divshow\">\n  <form [formGroup]=\"editableTableForm\">\n    <div>\n      <div class=\"table-responsive\">\n        <table class=\"table table-bordered table-sm\" formArrayName=\"table\">\n          <thead class=\"thead-light\">\n            <tr>\n              <th scope=\"col\" style=\"width: 10px;\" *ngFor=\"let config of tableConfig\">{{config.title}}</th>\n            </tr>\n          </thead>\n          <tbody>\n            <ng-container *ngFor=\"let rowFormGroup of editableTableForm['controls'].table['controls']; let i = index\"\n              [formGroupName]=\"i\">\n              <tr>\n                <ng-container *ngFor=\"let config of tableConfig\">\n                  <td *ngIf=\"config.inputField !== 'contenteditable'\">\n                    <mat-checkbox *ngIf=\"config.inputField === 'checkbox'\" color=\"basic\"\n                      class=\"form-control form-control-sm\" [formControlName]=\"config.formControlName\" [checked]=\"false\">\n                    </mat-checkbox>\n                    <mat-form-field class=\"example-full-width\" *ngIf=\"config.inputField === 'input'\"\n                      appearance=\"outline\" [ngClass]=\"{pb_0:!rowFormGroup.get(config.formControlName).errors}\">\n                      <input matInput [placeholder]=\"config.title\" [formControlName]=\"config.formControlName\">\n                      <mat-error\n                        *ngIf=\"rowFormGroup.get(config.formControlName).errors && rowFormGroup.get(config.formControlName).errors.msg &&!rowFormGroup.get(config.formControlName).errors.required\">\n                        {{rowFormGroup.get(config.formControlName).errors.msg}}</mat-error>\n                      <mat-error\n                        *ngIf=\"rowFormGroup.get(config.formControlName).errors && !rowFormGroup.get(config.formControlName).errors.msg &&rowFormGroup.get(config.formControlName).errors.required\">\n                        This field is required</mat-error>\n                    </mat-form-field>\n                    <mat-form-field *ngIf=\"config.inputField === 'autocomplete'\" class=\"w-100\"\n                      [ngClass]=\"{pb_0:!rowFormGroup.get(config.formControlName).errors}\" appearance=\"outline\">\n                      <input matInput [formControlName]=\"config.formControlName\" [matAutocomplete]=\"autoAmcNumber\"\n                        (focus)=\"autocompleteClicked(config, rowFormGroup)\" (keyup.enter)=\"$event.preventDefault();\">\n                      <!-- (blur)=\"resetFormControlNotHavingObject(searchAMCForm.controls.amcNumber, 'amcNumber')\" -->\n                      <mat-autocomplete #autoAmcNumber=\"matAutocomplete\"\n                        [displayWith]=\"matAutocompleteDisplayFn | async\"\n                        (optionSelected)=\"valueSelectedFromAutocomplete($event, i, rowFormGroup, config)\">\n                        <mat-option *ngFor=\"let option of filteredOptions[config.formControlName]\" [value]=\"option\">\n                          <!-- (click)=\"valueSelectedFromAutocomplete(option)\" -->\n                          {{option[config.displayKey]? option[config.displayKey] : option}}\n                        </mat-option>\n                      </mat-autocomplete>\n                      <mat-icon matSuffix>search</mat-icon>\n                      <mat-error\n                        *ngIf=\"rowFormGroup.get(config.formControlName).errors && rowFormGroup.get(config.formControlName).errors.msg &&!rowFormGroup.get(config.formControlName).errors.required\">\n                        {{rowFormGroup.get(config.formControlName).errors.msg}}</mat-error>\n                      <mat-error\n                        *ngIf=\"rowFormGroup.get(config.formControlName).errors && !rowFormGroup.get(config.formControlName).errors.msg &&rowFormGroup.get(config.formControlName).errors.required\">\n                        This field is required</mat-error>\n                    </mat-form-field>\n                    <!-- <span *ngIf=\"config.inputField === 'autocomplete'\">\n                      {{matAutocompleteDisplayFn | async}}\n                    </span> -->\n                    <mat-form-field *ngIf=\"config.inputField === 'select'\" appearance=\"outline\" class=\"pb_0\">\n                      <mat-label>Select {{config.title}}</mat-label>\n                      <mat-select [formControlName]=\"config.formControlName\"\n                        (selectionChange)=\"selectionChanged($event)\">\n                        <mat-option *ngFor=\"let option of selectOptions[config.formControlName]\" [value]=\"option\">\n                          {{option[config.displayKey]? option[config.displayKey] : option}}\n                        </mat-option>\n                      </mat-select>\n                      <mat-error\n                      *ngIf=\"rowFormGroup.get(config.formControlName).errors && rowFormGroup.get(config.formControlName).errors.msg &&!rowFormGroup.get(config.formControlName).errors.required\">\n                      {{rowFormGroup.get(config.formControlName).errors.msg}}</mat-error>\n                      <mat-error\n                        *ngIf=\"rowFormGroup.get(config.formControlName).errors && !rowFormGroup.get(config.formControlName).errors.msg &&rowFormGroup.get(config.formControlName).errors.required\">\n                        This field is required</mat-error>\n                    </mat-form-field>\n                  </td>\n                  <td *ngIf=\"config.inputField === 'contenteditable'\"\n                    [attr.contenteditable]=\"config.inputField !== 'contenteditable' || config.isNeedValueChanges\"\n                    [textContent]=\"rowFormGroup.controls[config.formControlName].value\"\n                    (input)=\"contenteditableValuechange($event.target.textContent,rowFormGroup,config.formControlName)\"\n                    #contenteditableRef></td>\n                </ng-container>\n              </tr>\n              <!-- <ng-content select=\"tr\"></ng-content> -->\n            </ng-container>\n          </tbody>\n          <tfoot>\n            <ng-content select=\"ng-container.table_footer\"></ng-content>\n          </tfoot>\n        </table>\n      </div>\n    </div>\n  </form>\n  <div class=\" text-center mt-2\" *ngIf=\"!hideActionBtn && !viewOnly \">\n    <button type=\"button\" class=\"btn btn-sm btn_primary \" (click)=\"addRow()\"> Add\n      Row</button>\n    <button type=\"button\" class=\"btn btn-sm btn_secondary ml-2 \" (click)=\"deleteRow()\"> Delete Row </button>\n  </div>\n</div>",
                    providers: [
                        {
                            provide: NG_VALUE_ACCESSOR,
                            useExisting: forwardRef((/**
                             * @return {?}
                             */
                            function () { return EditableTableComponent; })),
                            multi: true
                        },
                        {
                            provide: NG_VALIDATORS,
                            useExisting: forwardRef((/**
                             * @return {?}
                             */
                            function () { return EditableTableComponent; })),
                            multi: true
                        }
                    ],
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    EditableTableComponent.ctorParameters = function () { return [
        { type: FormBuilder },
        { type: EtDataHandlerService },
        { type: ChangeDetectorRef }
    ]; };
    EditableTableComponent.propDecorators = {
        viewOnly: [{ type: Input }],
        hideActionBtn: [{ type: Input }],
        hideDefaultRow: [{ type: Input }],
        assignListToSelect: [{ type: Input }],
        tableConfig: [{ type: Input }],
        getFormData: [{ type: Input }],
        getTableRecordChange: [{ type: Output }],
        optionSelected: [{ type: Output }],
        assignListToAutocomplete: [{ type: Input }],
        patchValue: [{ type: Input }],
        patchValueToSelect: [{ type: Input }],
        tableData: [{ type: Input, args: ['tableData',] }],
        searchValueChanges: [{ type: Output }],
        tableValueChanges: [{ type: Output }],
        formGroupForTableRow: [{ type: Input }],
        deletedTableRecords: [{ type: Output }],
        matAutocompleteDisplayFn: [{ type: Input }],
        etFormArray: [{ type: Input }],
        control: [{ type: Input }],
        validationError: [{ type: Input }]
    };
    return EditableTableComponent;
}());
export { EditableTableComponent };
if (false) {
    /** @type {?} */
    EditableTableComponent.prototype.editableTableForm;
    /** @type {?} */
    EditableTableComponent.prototype.isEdit;
    /** @type {?} */
    EditableTableComponent.prototype.isView;
    /** @type {?} */
    EditableTableComponent.prototype.filteredOptions;
    /** @type {?} */
    EditableTableComponent.prototype.selectOptions;
    /** @type {?} */
    EditableTableComponent.prototype.subscription;
    /** @type {?} */
    EditableTableComponent.prototype.viewOnly;
    /** @type {?} */
    EditableTableComponent.prototype.hideActionBtn;
    /** @type {?} */
    EditableTableComponent.prototype.hideDefaultRow;
    /** @type {?} */
    EditableTableComponent.prototype.tableConfig;
    /** @type {?} */
    EditableTableComponent.prototype.getTableRecordChange;
    /** @type {?} */
    EditableTableComponent.prototype.optionSelected;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype._patchValue;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype.dirtyAutocompleteColumn;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype._tableData;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype._formGroupForTableRow;
    /** @type {?} */
    EditableTableComponent.prototype.searchValueChanges;
    /** @type {?} */
    EditableTableComponent.prototype.tableValueChanges;
    /** @type {?} */
    EditableTableComponent.prototype.deletedTableRecords;
    /** @type {?} */
    EditableTableComponent.prototype.matAutocompleteDisplayFn;
    /** @type {?} */
    EditableTableComponent.prototype.etFormArray;
    /** @type {?} */
    EditableTableComponent.prototype.control;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype._validationError;
    /** @type {?} */
    EditableTableComponent.prototype.onTouched;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype.fb;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype.etDataHandlerService;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype._changeDetectorRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWRpdGFibGUtdGFibGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWRpdGFibGUtdGFibGUvIiwic291cmNlcyI6WyJsaWIvZWRpdGFibGUtdGFibGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFpQixNQUFNLEVBQUUsWUFBWSxFQUE0QixVQUFVLEVBQWEsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbEssT0FBTyxFQUFFLFNBQVMsRUFBYyxXQUFXLEVBQThDLGlCQUFpQixFQUFFLGFBQWEsRUFBcUMsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDbE0sT0FBTyxFQUFrQixPQUFPLEVBQUUsWUFBWSxFQUFtQixNQUFNLE1BQU0sQ0FBQztBQUM5RSxPQUFPLEVBQUUsR0FBRyxFQUFhLE1BQU0sZ0JBQWdCLENBQUM7QUFDaEQsT0FBTyxFQUFpRixpQkFBaUIsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBRTNJLE9BQU8sS0FBSyxJQUFJLE1BQU0sTUFBTSxDQUFDO0FBQzdCLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBR2pFO0lBOEZFLGdDQUNVLEVBQWUsRUFDZixvQkFBMEMsRUFDMUMsa0JBQXFDO1FBSC9DLGlCQUlLO1FBSEssT0FBRSxHQUFGLEVBQUUsQ0FBYTtRQUNmLHlCQUFvQixHQUFwQixvQkFBb0IsQ0FBc0I7UUFDMUMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFtQjtRQTVFL0Msb0JBQWUsR0FBRyxFQUFFLENBQUM7UUFDckIsa0JBQWEsR0FBRyxFQUFFLENBQUM7UUFDbkIsaUJBQVksR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3pCLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFDakIsa0JBQWEsR0FBRyxLQUFLLENBQUM7UUFDdEIsbUJBQWMsR0FBWSxLQUFLLENBQUM7UUFVekMsZ0JBQVcsR0FBRyxtQkFBQSxFQUFFLEVBQXNCLENBQUM7UUFRN0IseUJBQW9CLEdBQUcsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUNsRCxtQkFBYyxHQUFHLElBQUksWUFBWSxFQUF1QyxDQUFDO1FBaUN6RSx1QkFBa0IsR0FBRyxJQUFJLFlBQVksRUFBbUMsQ0FBQztRQUN6RSxzQkFBaUIsR0FBRyxJQUFJLFlBQVksRUFBVSxDQUFDO1FBSS9DLHdCQUFtQixHQUFHLElBQUksWUFBWSxFQUFpQixDQUFDO1FBQ3pELDZCQUF3QixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7O1FBaVEzQyxjQUFTOzs7UUFBZTtZQUM3QixLQUFJLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUM1QyxDQUFDLEVBQUM7SUFyUEUsQ0FBQztJQXZFTCxzQkFDSSxzREFBa0I7Ozs7O1FBRHRCLFVBQ3VCLENBQUM7WUFEeEIsaUJBT0M7WUFMQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQ1AsQ0FBQyxDQUFDLE9BQU87Ozs7Z0JBQUMsVUFBQSxHQUFHO29CQUNYLEtBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUM7Z0JBQ3JELENBQUMsRUFBQyxDQUFDO2FBQ0o7UUFDSCxDQUFDOzs7T0FBQTtJQUdELHNCQUFhLCtDQUFXOzs7OztRQUF4QixVQUF5QixDQUFDO1lBQTFCLGlCQU1DO1lBTEMsQ0FBQyxDQUFDLFNBQVM7Ozs7WUFBQyxVQUFBLEdBQUc7Z0JBQ2IsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFO29CQUNULEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxtQkFBQSxLQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBYSxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztpQkFDcEc7WUFDSCxDQUFDLEVBQUMsQ0FBQztRQUNMLENBQUM7OztPQUFBO0lBR0Qsc0JBQ0ksNERBQXdCOzs7OztRQUQ1QixVQUM2QixDQUFDO1lBQzVCLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUMsVUFBVSxLQUFLLGNBQWMsRUFBRTtnQkFDL0MsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLGVBQWUsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUE7YUFDeEc7UUFDSCxDQUFDOzs7T0FBQTtJQUVELHNCQUFhLDhDQUFVOzs7OztRQUF2QixVQUF3QixDQUFvQjtZQUMxQyxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztRQUN2QixDQUFDOzs7T0FBQTtJQUNELHNCQUFhLHNEQUFrQjs7Ozs7UUFBL0IsVUFBZ0MsQ0FBb0I7WUFDbEQsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTs7b0JBQzdCLE1BQUksR0FBRyxDQUFDLG1CQUFBLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFhLENBQUM7Z0JBQy9ELENBQUMsQ0FBQyxPQUFPOzs7O2dCQUFDLFVBQUEsR0FBRztvQkFDWCxJQUFJLE1BQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxFQUFFO3dCQUMvQixNQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO3FCQUN4RDtnQkFDSCxDQUFDLEVBQUMsQ0FBQzthQUNKO1FBQ0gsQ0FBQzs7O09BQUE7SUFHRCxzQkFDVyw2Q0FBUzs7Ozs7UUFEcEIsVUFDcUIsQ0FBQztZQUNwQjtnQkFDRSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQ3ZCLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO29CQUNwQixJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUE7aUJBQzVCO2FBQ0Y7UUFDSCxDQUFDOzs7T0FBQTtJQUlELHNCQUFvQix3REFBb0I7Ozs7O1FBQXhDLFVBQXlDLEVBQUU7WUFDekMsSUFBSSxDQUFDLHFCQUFxQix3QkFBUSxFQUFFLENBQUUsQ0FBQztRQUN6QyxDQUFDOzs7T0FBQTtJQU1ELHNCQUFvQixtREFBZTs7Ozs7UUFBbkMsVUFBb0MsRUFBb0I7WUFDdEQsSUFBSSxFQUFFLEVBQUU7Z0JBQ04sSUFBSSxDQUFDLGdCQUFnQixHQUFHLEVBQUUsQ0FBQztnQkFDM0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQTthQUNsQjtRQUNILENBQUM7OztPQUFBOzs7O0lBT0QseUNBQVE7OztJQUFSO1FBQ0UsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQywrQkFBK0IsRUFBRSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyx3QkFBd0IsRUFBRSxDQUFDO1FBQ2hDLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDbEQsa0NBQWtDO0lBQ3BDLENBQUM7Ozs7O0lBQ0QsNENBQVc7Ozs7SUFBWCxVQUFZLE9BQXNCO1FBQ2hDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFO1lBQzdELElBQUksQ0FBQyxXQUFXLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUM7WUFDbkQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztTQUMxQztRQUNELElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFO1lBQ3ZELElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDbkQ7SUFDSCxDQUFDOzs7OztJQUNELCtEQUE4Qjs7OztJQUE5QixVQUErQixPQUF1QjtRQUF0RCxpQkFNQztRQUxDLElBQUksT0FBTyxFQUFFO1lBQ1gsT0FBTyxDQUFDLGFBQWE7OztZQUFHO2dCQUN0QixLQUFJLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztZQUM1QyxDQUFDLENBQUEsQ0FBQztTQUNIO0lBQ0gsQ0FBQzs7Ozs7O0lBRU8sa0RBQWlCOzs7OztJQUF6QixVQUEwQixDQUFDO1FBQ3pCLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDakQsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtnQkFDM0IsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2FBQ3hCOztnQkFDRyxNQUFJLEdBQUcsQ0FBQyxtQkFBQSxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBYSxDQUFDOztnQkFDM0QsVUFBUTtZQUNaLENBQUMsQ0FBQyxPQUFPOzs7O1lBQUMsVUFBQSxHQUFHO2dCQUNYLElBQUksR0FBRyxDQUFDLFVBQVUsRUFBRTtvQkFDbEIsS0FBSyxJQUFNLEdBQUcsSUFBSSxNQUFJLENBQUMsS0FBSyxFQUFFO3dCQUM1QixJQUFJLE1BQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFOztnQ0FDNUIsUUFBUSxHQUFHLE1BQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDOzRCQUNoQyxJQUFJLFFBQVEsQ0FBQyxZQUFZLENBQUMsS0FBSyxHQUFHLENBQUMsVUFBVSxFQUFFO2dDQUM3QyxVQUFRLEdBQUcsR0FBRyxDQUFDOzZCQUNoQjt5QkFDRjtxQkFDRjtpQkFDRjtnQkFDRCxVQUFRLEdBQUcsQ0FBQyxDQUFDLFVBQVEsSUFBSSxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFVBQVEsQ0FBQztnQkFDakUsVUFBUSxDQUFDLENBQUMsQ0FBQyxNQUFJLENBQUMsUUFBUSxDQUFDLFVBQVEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUN2RyxDQUFDLEVBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7Ozs7O0lBQ08sb0RBQW1COzs7OztJQUEzQixVQUE0QixTQUFTO1FBQXJDLGlCQVFDO1FBUEMsU0FBUyxDQUFDLE9BQU87Ozs7O1FBQUMsVUFBQyxPQUFPLEVBQUUsS0FBSztZQUMvQixJQUFJLEtBQUssS0FBSyxDQUFDLEVBQUU7Z0JBQ2YsS0FBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxRQUFRLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pFLE9BQU87YUFDUjtZQUNELEtBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDdkIsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDO0lBQ0QsOEJBQThCOzs7Ozs7SUFDdEIsZ0VBQStCOzs7Ozs7SUFBdkM7UUFBQSxpQkFtQkM7UUFsQkMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLHFCQUFxQixDQUFDLFNBQVM7Ozs7UUFBQyxVQUFDLGVBQWU7WUFDeEUsSUFBSSxLQUFJLENBQUMsUUFBUSxFQUFFO2dCQUNqQixPQUFPO2FBQ1I7O2dCQUNHLElBQUksR0FBRyxDQUFDLG1CQUFBLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFhLENBQUM7O2dCQUMzRCxZQUFZLEdBQUcsRUFBRTs7Z0JBQ2YsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDdEMsSUFBSSxlQUFlLENBQUMsV0FBVyxFQUFFLEtBQUssS0FBSyxFQUFFOztvQkFDdkMsZ0JBQWdCLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNOzs7O2dCQUFDLFVBQUEsTUFBTSxJQUFJLE9BQUEsTUFBTSxDQUFDLGtCQUFrQixFQUF6QixDQUF5QixFQUFDO2dCQUNuRixnQkFBZ0IsQ0FBQyxPQUFPOzs7O2dCQUFDLFVBQUMsTUFBTTtvQkFDOUIsWUFBWSxHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFBO29CQUM1RSxLQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQztnQkFDbkYsQ0FBQyxFQUFDLENBQUM7Z0JBQ0gsT0FBTzthQUNSO1lBQ0QsWUFBWSxHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsZUFBZSxDQUFDLENBQUE7WUFDckUsS0FBSSxDQUFDLG9CQUFvQixDQUFDLGdCQUFnQixDQUFDLFlBQVksRUFBRSxlQUFlLENBQUMsQ0FBQztRQUM1RSxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7Ozs7SUFDTyxvREFBbUI7Ozs7OztJQUEzQixVQUE0QixXQUEwQixFQUFFLFdBQW1COztZQUNyRSxZQUFZLEdBQUcsRUFBRTtRQUNyQixLQUFLLElBQU0sR0FBRyxJQUFJLFdBQVcsRUFBRTtZQUM3QixJQUFJLFdBQVcsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7O29CQUM3QixRQUFRLEdBQUcsV0FBVyxDQUFDLEdBQUcsQ0FBQztnQkFDakMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQTthQUN6QztTQUNGO1FBQ0QsT0FBTyxZQUFZLENBQUM7SUFDdEIsQ0FBQzs7Ozs7SUFDTyx5REFBd0I7Ozs7SUFBaEM7UUFBQSxpQkFrQkM7O1lBakJPLGNBQWMsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsYUFBYSxFQUFFLENBQUMsU0FBUzs7OztRQUFDLFVBQUEsTUFBTTtZQUMvRSxJQUFJLE1BQU0sRUFBRTtnQkFDVixLQUFJLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztnQkFDMUMsSUFBSSxLQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxLQUFLLE9BQU8sSUFBSSxLQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxLQUFLLFVBQVUsRUFBRTtvQkFDN0YsS0FBSSxDQUFDLG9CQUFvQixDQUFDLGVBQWUsQ0FBQzt3QkFDeEMsSUFBSSxFQUFFLENBQUMsbUJBQUEsS0FBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQWEsQ0FBQyxDQUFDLFdBQVcsRUFBRTt3QkFDeEUsS0FBSyxFQUFFLElBQUk7cUJBQ1osQ0FBQyxDQUFDO29CQUNILE9BQU87aUJBQ1I7Z0JBQ0QsS0FBSSxDQUFDLG9CQUFvQixDQUFDLGVBQWUsQ0FBQztvQkFDeEMsSUFBSSxFQUFFLEVBQUU7b0JBQ1IsS0FBSyxFQUFFLEtBQUs7aUJBQ2IsQ0FBQyxDQUFDO2FBQ0o7UUFDSCxDQUFDLEVBQUM7UUFDRixJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUN4QyxDQUFDOzs7O0lBQ0QsZ0RBQWU7OztJQUFmO1FBQ0UsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDMUIsT0FBTztTQUNSO1FBQ0QsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ3BCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztnQkFDckMsS0FBSyxFQUFFLElBQUksQ0FBQyxXQUFXO2FBQ3hCLENBQUMsQ0FBQztTQUNKO2FBQU07WUFDTCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7Z0JBQ3JDLEtBQUssRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUM7YUFDekIsQ0FBQyxDQUFDO1NBQ0o7UUFDRCxJQUNFLENBQUMsSUFBSSxDQUFDLGNBQWM7ZUFDakIsQ0FBQyxtQkFBQSxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBYSxDQUFDLENBQUMsTUFBTSxLQUFLLENBQUM7ZUFDakUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLEVBQy9EO1lBQ0EsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1NBQ2Y7UUFDRCxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUNuRSxDQUFDOzs7OztJQUNELHFEQUFvQjs7OztJQUFwQixVQUFxQixJQUFhO1FBQWxDLGlCQW9CQzs7WUFuQk8sRUFBRSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQztRQUNwRCxJQUFJLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQzVCLEVBQUUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDckI7UUFDRCxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakIsRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ2IsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7U0FDM0U7UUFDRCxFQUFFLENBQUMsVUFBVSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFBO1FBQ3ZELElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTzs7OztRQUFDLFVBQUMsR0FBZ0I7WUFDeEMsbUJBQW1CO1lBQ25CLElBQUksR0FBRyxDQUFDLFVBQVUsS0FBSyxjQUFjLElBQUksR0FBRyxDQUFDLGtCQUFrQixFQUFFO2dCQUMvRCxFQUFFLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxZQUFZLENBQUMsU0FBUzs7OztnQkFBQyxVQUFBLFdBQVc7b0JBQ2pFLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLEVBQUUsV0FBVyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsUUFBUSxFQUFFLEVBQUUsQ0FBQyxXQUFXLEVBQUUsRUFBRSxDQUFDLENBQUM7b0JBQzVGLEtBQUksQ0FBQyx1QkFBdUIsd0JBQVEsR0FBRyxDQUFFLENBQUM7Z0JBQzVDLENBQUMsRUFBQyxDQUFDO2FBQ0o7UUFDSCxDQUFDLEVBQUMsQ0FBQztRQUNILE9BQU8sRUFBRSxDQUFDO0lBQ1osQ0FBQzs7Ozs7Ozs7SUFDTyx3Q0FBTzs7Ozs7OztJQUFmLFVBQWdCLEtBQXNCLEVBQUUsT0FBK0IsRUFBRSxHQUFHOztZQUN0RSxXQUFXO1FBQ2YsSUFBSSxDQUFDLENBQUMsS0FBSyxJQUFJLEtBQUssS0FBSyxFQUFFLEVBQUU7WUFDM0IsT0FBTyxPQUFPLENBQUM7U0FDaEI7UUFDRCxJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsRUFBRTtZQUM3QixXQUFXLEdBQUcsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ25DO1FBQ0QsSUFBSSxPQUFPLEtBQUssS0FBSyxRQUFRLEVBQUU7WUFDN0IsV0FBVyxHQUFHLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUN4QztRQUNELE9BQU8sT0FBTyxDQUFDLE1BQU07Ozs7UUFBQyxVQUFBLE1BQU0sSUFBSSxPQUFBLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUFwRCxDQUFvRCxFQUFDLENBQUM7SUFDeEYsQ0FBQzs7OztJQUVELDJDQUFVOzs7SUFBVjtJQUNBLENBQUM7Ozs7O0lBQ0QsdUNBQU07Ozs7SUFBTixVQUFPLEdBQVk7UUFDakIsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ3BCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7WUFDekQsT0FBTztTQUNSOztZQUNHLGlCQUFpQixHQUFHLG1CQUFBLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFhO1FBQzFFLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztJQUN6RCxDQUFDOzs7OztJQUNELDJDQUFVOzs7O0lBQVYsVUFBVyxHQUFJOztZQUNULGlCQUFpQixHQUFHLG1CQUFBLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFhO1FBQzFFLGlCQUFpQixDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzFCLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztJQUN6RCxDQUFDOzs7O0lBQ0QsMENBQVM7OztJQUFUOztZQUNNLGlCQUFpQixHQUFHLG1CQUFBLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFhOztZQUN0RSxnQkFBZ0IsR0FBRyxFQUFFOztZQUNyQixXQUFXLEdBQUcsaUJBQWlCLENBQUMsUUFBUSxDQUFDLE1BQU07Ozs7UUFBQyxVQUFDLFNBQW9CO1lBQ3ZFLElBQUksU0FBUyxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQUU7Z0JBQzlCLGdCQUFnQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQzthQUNoRDtZQUNELE9BQU8sQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQTtRQUNwQyxDQUFDLEVBQUM7UUFDRixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDaEQsaUJBQWlCLENBQUMsS0FBSyxFQUFFLENBQUE7UUFDekIsV0FBVyxDQUFDLE9BQU87Ozs7UUFBQyxVQUFBLEVBQUUsSUFBSSxPQUFBLGlCQUFpQixDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBMUIsQ0FBMEIsRUFBQyxDQUFBO0lBQ3ZELENBQUM7Ozs7OztJQUVPLDhEQUE2Qjs7Ozs7SUFBckMsVUFBc0MsTUFBbUI7O1lBQ2pELEdBQUcsR0FBRyxNQUFNLENBQUMsUUFBUSxJQUFJLE1BQU0sQ0FBQyxVQUFVOztZQUMxQyxXQUFXOzs7O1FBQUcsVUFBQyxJQUFJO1lBQ3ZCLE9BQU8sQ0FBQyxJQUFJLElBQUksT0FBTyxJQUFJLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQy9ELENBQUMsQ0FBQTtRQUNELDBCQUEwQjs7UUFBMUIsMEJBQTBCO1FBQzFCLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQzFDLENBQUM7Ozs7OztJQUNELG9EQUFtQjs7Ozs7SUFBbkIsVUFBb0IsTUFBbUIsRUFBRSxTQUFvQjtRQUMzRCxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsR0FBRyxFQUFFLENBQUM7UUFDbEQsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzNDLElBQUksU0FBUyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUMsS0FBSyxFQUFFO1lBQ3BELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLEVBQUUsU0FBUyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLFNBQVMsQ0FBQyxXQUFXLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDM0ksSUFBSSxDQUFDLHVCQUF1Qix3QkFBUSxNQUFNLENBQUUsQ0FBQztTQUM5QztJQUNILENBQUM7Ozs7Ozs7O0lBQ0QsOERBQTZCOzs7Ozs7O0lBQTdCLFVBQThCLEtBQW1DLEVBQUUsUUFBUSxFQUFFLFlBQXVCLEVBQUUsTUFBbUI7O1lBQ25ILE1BQU0sR0FBRyxDQUFDLEtBQUssSUFBSSxPQUFPLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsc0JBQU0sS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxJQUFJLEVBQUU7UUFDckgsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLG9CQUFHLE1BQU0sUUFBQSxJQUFLLEVBQUUsUUFBUSxVQUFBLEVBQUUsUUFBUSxFQUFFLFlBQVksQ0FBQyxXQUFXLEVBQUUsRUFBRSxNQUFNLFFBQUEsRUFBRSxFQUFHLENBQUM7SUFDdEcsQ0FBQzs7Ozs7SUFDRCxpREFBZ0I7Ozs7SUFBaEIsVUFBaUIsS0FBSztJQUV0QixDQUFDOzs7Ozs7O0lBQ0QsMkRBQTBCOzs7Ozs7SUFBMUIsVUFBMkIsS0FBSyxFQUFFLFlBQXVCLEVBQUUsZUFBZTtRQUN4RSxZQUFZLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUUzRCxDQUFDOzs7Ozs7SUFDTyx5Q0FBUTs7Ozs7SUFBaEIsVUFBaUIsRUFBcUI7O1lBQzlCLFNBQVMsR0FBRyxDQUFFLG1CQUFBLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEVBQWEsQ0FBQzs7WUFDL0QsU0FBUyxHQUFHLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQyxTQUFTO1lBQzVELElBQUksU0FBUyxDQUFDLFVBQVUsS0FBSyxFQUFFLENBQUMsVUFBVSxFQUFFO2dCQUMxQyxPQUFPLElBQUksQ0FBQzthQUNiO1FBQ0gsQ0FBQyxFQUFDOztZQUNJLFVBQVUsR0FBRyxTQUFTLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBQztRQUMxQyxJQUFJLFVBQVUsRUFBRTtZQUNkLElBQUksRUFBRSxDQUFDLFFBQVEsRUFBRTtnQkFDZixVQUFVLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxlQUFlLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ25ELE9BQU87YUFDUjtZQUNELFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLGVBQWUsQ0FBQyxDQUFDLFNBQVMsb0JBQzFDLEdBQUcsRUFBQyxFQUFFLENBQUMsR0FBRyxJQUNQLEVBQUUsQ0FBQyxJQUFJLEVBQ1YsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7Ozs7SUFNRCwyQ0FBVTs7OztJQUFWLFVBQVcsR0FBUTtRQUNqQixJQUFJLEdBQUcsSUFBSSxHQUFHLENBQUMsU0FBUyxFQUFFO1lBQ3hCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUMvQixJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDbEIsT0FBTztTQUNSO1FBQ0QsR0FBRyxJQUFJLEdBQUcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN6RCxDQUFDOzs7OztJQUNELGlEQUFnQjs7OztJQUFoQixVQUFpQixFQUFPO1FBQXhCLGlCQUlDO1FBSEMsRUFBRSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQzNELEdBQUc7Ozs7UUFBQyxVQUFDLEtBQUssSUFBSyxPQUFBLENBQUMsbUJBQUEsS0FBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQWEsQ0FBQyxDQUFDLFdBQVcsRUFBRSxFQUFsRSxDQUFrRSxFQUFDLENBQ25GLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ2xCLENBQUM7Ozs7O0lBQ0Qsa0RBQWlCOzs7O0lBQWpCLFVBQWtCLEVBQU87UUFDdkIsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUM5QixDQUFDOzs7OztJQUNELGlEQUFnQjs7OztJQUFoQixVQUFrQixVQUFtQjtRQUNuQyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUM5Ryw4Q0FBOEM7SUFDaEQsQ0FBQzs7Ozs7SUFDRCx5Q0FBUTs7OztJQUFSLFVBQVMsT0FBd0I7UUFDL0IsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxXQUFXLEVBQUUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxzQ0FBc0MsRUFBRSxFQUFFLENBQUM7SUFDakosQ0FBQztJQUNELDBFQUEwRTs7Ozs7SUFFMUUsNENBQVc7Ozs7SUFBWDtRQUNFLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDbEMsQ0FBQzs7Z0JBblhGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsZ0JBQWdCO29CQUMxQix3NU1BQThDO29CQUU5QyxTQUFTLEVBQUU7d0JBQ1Q7NEJBQ0UsT0FBTyxFQUFFLGlCQUFpQjs0QkFDMUIsV0FBVyxFQUFFLFVBQVU7Ozs0QkFBQyxjQUFNLE9BQUEsc0JBQXNCLEVBQXRCLENBQXNCLEVBQUM7NEJBQ3JELEtBQUssRUFBRSxJQUFJO3lCQUNaO3dCQUNEOzRCQUNFLE9BQU8sRUFBRSxhQUFhOzRCQUN0QixXQUFXLEVBQUUsVUFBVTs7OzRCQUFDLGNBQU0sT0FBQSxzQkFBc0IsRUFBdEIsQ0FBc0IsRUFBQzs0QkFDckQsS0FBSyxFQUFFLElBQUk7eUJBQ1o7cUJBQ0Y7O2lCQUNGOzs7O2dCQXpCK0IsV0FBVztnQkFNbEMsb0JBQW9CO2dCQVA0RixpQkFBaUI7OzsyQkFrQ3ZJLEtBQUs7Z0NBQ0wsS0FBSztpQ0FDTCxLQUFLO3FDQUNMLEtBQUs7OEJBUUwsS0FBSzs4QkFFTCxLQUFLO3VDQU9MLE1BQU07aUNBQ04sTUFBTTsyQ0FDTixLQUFLOzZCQU9MLEtBQUs7cUNBR0wsS0FBSzs0QkFZTCxLQUFLLFNBQUMsV0FBVztxQ0FVakIsTUFBTTtvQ0FDTixNQUFNO3VDQUNOLEtBQUs7c0NBR0wsTUFBTTsyQ0FDTixLQUFLOzhCQUNMLEtBQUs7MEJBQ0wsS0FBSztrQ0FFTCxLQUFLOztJQTRSUiw2QkFBQztDQUFBLEFBcFhELElBb1hDO1NBbldZLHNCQUFzQjs7O0lBQ2pDLG1EQUE0Qjs7SUFDNUIsd0NBQWdCOztJQUNoQix3Q0FBZ0I7O0lBQ2hCLGlEQUFxQjs7SUFDckIsK0NBQW1COztJQUNuQiw4Q0FBa0M7O0lBQ2xDLDBDQUEwQjs7SUFDMUIsK0NBQStCOztJQUMvQixnREFBeUM7O0lBU3pDLDZDQUN1Qzs7SUFRdkMsc0RBQTREOztJQUM1RCxnREFBbUY7Ozs7O0lBT25GLDZDQUF1Qzs7Ozs7SUFjdkMseURBQTZDOzs7OztJQUM3Qyw0Q0FBbUI7Ozs7O0lBVW5CLHVEQUE4Qjs7SUFDOUIsb0RBQW1GOztJQUNuRixtREFBeUQ7O0lBSXpELHFEQUFrRTs7SUFDbEUsMERBQWtEOztJQUNsRCw2Q0FBZ0M7O0lBQ2hDLHlDQUE4Qjs7Ozs7SUFDOUIsa0RBQTJDOztJQThQM0MsMkNBRUU7Ozs7O0lBeFBBLG9DQUF1Qjs7Ozs7SUFDdkIsc0RBQWtEOzs7OztJQUNsRCxvREFBNkMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIEFmdGVyVmlld0luaXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBPbkNoYW5nZXMsIFNpbXBsZUNoYW5nZXMsIGZvcndhcmRSZWYsIE9uRGVzdHJveSwgQ2hhbmdlRGV0ZWN0b3JSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZvcm1BcnJheSwgVmFsaWRhdG9ycywgRm9ybUJ1aWxkZXIsIEZvcm1Hcm91cCwgQ29udHJvbFZhbHVlQWNjZXNzb3IsIFZhbGlkYXRvciwgTkdfVkFMVUVfQUNDRVNTT1IsIE5HX1ZBTElEQVRPUlMsIEFic3RyYWN0Q29udHJvbCwgVmFsaWRhdGlvbkVycm9ycywgRm9ybUNvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBvZiwgT2JzZXJ2YWJsZSwgU3ViamVjdCwgU3Vic2NyaXB0aW9uLCBCZWhhdmlvclN1YmplY3QgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IG1hcCwgc3RhcnRXaXRoIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgVGFibGVDb25maWcsIFBhdGNoVmFsdWUsIEV0VGFibGVWYWx1ZUNoYW5nZUV2ZW50LCBFdEF1dG9jb21wbGV0ZVNlbGVjdGVkRXZlbnQsIEVUVmFsaWRhdGlvbkVycm9yIH0gZnJvbSAnLi9lZGl0YWJsZS10YWJsZS1jb25maWcnO1xuaW1wb3J0IHsgTWF0QXV0b2NvbXBsZXRlU2VsZWN0ZWRFdmVudCB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2F1dG9jb21wbGV0ZSc7XG5pbXBvcnQgKiBhcyB1dWlkIGZyb20gJ3V1aWQnO1xuaW1wb3J0IHsgRXREYXRhSGFuZGxlclNlcnZpY2UgfSBmcm9tICcuL2V0LWRhdGEtaGFuZGxlci5zZXJ2aWNlJztcblxuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdlZGl0YWJsZS10YWJsZScsXG4gIHRlbXBsYXRlVXJsOiAnLi9lZGl0YWJsZS10YWJsZS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2VkaXRhYmxlLXRhYmxlLmNvbXBvbmVudC5zY3NzJ10sXG4gIHByb3ZpZGVyczogW1xuICAgIHtcbiAgICAgIHByb3ZpZGU6IE5HX1ZBTFVFX0FDQ0VTU09SLFxuICAgICAgdXNlRXhpc3Rpbmc6IGZvcndhcmRSZWYoKCkgPT4gRWRpdGFibGVUYWJsZUNvbXBvbmVudCksXG4gICAgICBtdWx0aTogdHJ1ZVxuICAgIH0sXG4gICAge1xuICAgICAgcHJvdmlkZTogTkdfVkFMSURBVE9SUyxcbiAgICAgIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IEVkaXRhYmxlVGFibGVDb21wb25lbnQpLFxuICAgICAgbXVsdGk6IHRydWVcbiAgICB9XG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgRWRpdGFibGVUYWJsZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95LCBPbkNoYW5nZXMsIENvbnRyb2xWYWx1ZUFjY2Vzc29yLCBWYWxpZGF0b3Ige1xuICBlZGl0YWJsZVRhYmxlRm9ybTogRm9ybUdyb3VwXG4gIGlzRWRpdDogYm9vbGVhbjtcbiAgaXNWaWV3OiBib29sZWFuO1xuICBmaWx0ZXJlZE9wdGlvbnMgPSB7fTtcbiAgc2VsZWN0T3B0aW9ucyA9IHt9O1xuICBzdWJzY3JpcHRpb24gPSBuZXcgU3Vic2NyaXB0aW9uKCk7XG4gIEBJbnB1dCgpIHZpZXdPbmx5ID0gZmFsc2U7XG4gIEBJbnB1dCgpIGhpZGVBY3Rpb25CdG4gPSBmYWxzZTtcbiAgQElucHV0KCkgaGlkZURlZmF1bHRSb3c6IGJvb2xlYW4gPSBmYWxzZTtcbiAgQElucHV0KClcbiAgc2V0IGFzc2lnbkxpc3RUb1NlbGVjdCh2KSB7XG4gICAgaWYgKCEhdikge1xuICAgICAgdi5mb3JFYWNoKGVsZSA9PiB7XG4gICAgICAgIHRoaXMuc2VsZWN0T3B0aW9uc1tlbGUuZm9ybUNvbnRyb2xOYW1lXSA9IGVsZS5saXN0O1xuICAgICAgfSk7XG4gICAgfVxuICB9XG4gIEBJbnB1dCgpXG4gIHRhYmxlQ29uZmlnID0gW10gYXMgQXJyYXk8VGFibGVDb25maWc+O1xuICBASW5wdXQoKSBzZXQgZ2V0Rm9ybURhdGEodikge1xuICAgIHYuc3Vic2NyaWJlKHJlcyA9PiB7XG4gICAgICBpZiAoISFyZXMpIHtcbiAgICAgICAgdGhpcy5nZXRUYWJsZVJlY29yZENoYW5nZS5lbWl0KCh0aGlzLmVkaXRhYmxlVGFibGVGb3JtLmNvbnRyb2xzLnRhYmxlIGFzIEZvcm1BcnJheSkuZ2V0UmF3VmFsdWUoKSk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cbiAgQE91dHB1dCgpIGdldFRhYmxlUmVjb3JkQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxvYmplY3Q+KCk7XG4gIEBPdXRwdXQoKSBvcHRpb25TZWxlY3RlZCA9IG5ldyBFdmVudEVtaXR0ZXI8RXRBdXRvY29tcGxldGVTZWxlY3RlZEV2ZW50PG9iamVjdD4+KCk7XG4gIEBJbnB1dCgpXG4gIHNldCBhc3NpZ25MaXN0VG9BdXRvY29tcGxldGUodikge1xuICAgIGlmICh2ICYmIHYuY29uZmlnLmlucHV0RmllbGQgPT09ICdhdXRvY29tcGxldGUnKSB7XG4gICAgICB0aGlzLmZpbHRlcmVkT3B0aW9uc1t2LmNvbmZpZy5mb3JtQ29udHJvbE5hbWVdID0gdGhpcy5fZmlsdGVyKHYuc2VhcmNoS2V5LCB2Lmxpc3QsIHYuY29uZmlnLmRpc3BsYXlLZXkpXG4gICAgfVxuICB9XG4gIHByaXZhdGUgX3BhdGNoVmFsdWU6IEFycmF5PFBhdGNoVmFsdWU+O1xuICBASW5wdXQoKSBzZXQgcGF0Y2hWYWx1ZSh2OiBBcnJheTxQYXRjaFZhbHVlPikge1xuICAgIHRoaXMuX3BhdGNoVmFsdWUgPSB2O1xuICB9XG4gIEBJbnB1dCgpIHNldCBwYXRjaFZhbHVlVG9TZWxlY3QodjogQXJyYXk8UGF0Y2hWYWx1ZT4pIHtcbiAgICBpZiAoISF2ICYmIHRoaXMuZWRpdGFibGVUYWJsZUZvcm0pIHtcbiAgICAgIGxldCBmb3JtID0gKHRoaXMuZWRpdGFibGVUYWJsZUZvcm0uY29udHJvbHMudGFibGUgYXMgRm9ybUFycmF5KVxuICAgICAgdi5mb3JFYWNoKGVsZSA9PiB7XG4gICAgICAgIGlmIChmb3JtLmNvbnRyb2xzW2VsZS5yb3dJbmRleF0pIHtcbiAgICAgICAgICBmb3JtLmNvbnRyb2xzW2VsZS5yb3dJbmRleF0ucGF0Y2hWYWx1ZShlbGUucGF0Y2hWYWx1ZSk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuICBwcml2YXRlIGRpcnR5QXV0b2NvbXBsZXRlQ29sdW1uOiBUYWJsZUNvbmZpZztcbiAgcHJpdmF0ZSBfdGFibGVEYXRhO1xuICBASW5wdXQoJ3RhYmxlRGF0YScpXG4gIHB1YmxpYyBzZXQgdGFibGVEYXRhKHYpIHtcbiAgICB7XG4gICAgICBpZiAoISF2ICYmIHYubGVuZ3RoID4gMCkge1xuICAgICAgICB0aGlzLl90YWJsZURhdGEgPSB2O1xuICAgICAgICB0aGlzLmNyZWF0ZVRhYmxlV2l0aERhdGEodilcbiAgICAgIH1cbiAgICB9XG4gIH1cbiAgcHJpdmF0ZSBfZm9ybUdyb3VwRm9yVGFibGVSb3c7XG4gIEBPdXRwdXQoKSBzZWFyY2hWYWx1ZUNoYW5nZXMgPSBuZXcgRXZlbnRFbWl0dGVyPEV0VGFibGVWYWx1ZUNoYW5nZUV2ZW50PG9iamVjdD4+KCk7XG4gIEBPdXRwdXQoKSB0YWJsZVZhbHVlQ2hhbmdlcyA9IG5ldyBFdmVudEVtaXR0ZXI8b2JqZWN0PigpO1xuICBASW5wdXQoKSBwdWJsaWMgc2V0IGZvcm1Hcm91cEZvclRhYmxlUm93KGZiKSB7XG4gICAgdGhpcy5fZm9ybUdyb3VwRm9yVGFibGVSb3cgPSB7IC4uLmZiIH07XG4gIH1cbiAgQE91dHB1dCgpIGRlbGV0ZWRUYWJsZVJlY29yZHMgPSBuZXcgRXZlbnRFbWl0dGVyPEFycmF5PG9iamVjdD4+KCk7XG4gIEBJbnB1dCgpIG1hdEF1dG9jb21wbGV0ZURpc3BsYXlGbiA9IG5ldyBTdWJqZWN0KCk7XG4gIEBJbnB1dCgpIGV0Rm9ybUFycmF5OiBGb3JtQXJyYXk7XG4gIEBJbnB1dCgpIGNvbnRyb2w6IEZvcm1Db250cm9sO1xuICBwcml2YXRlIF92YWxpZGF0aW9uRXJyb3I6RVRWYWxpZGF0aW9uRXJyb3I7XG4gIEBJbnB1dCgpIHB1YmxpYyBzZXQgdmFsaWRhdGlvbkVycm9yKHZlOkVUVmFsaWRhdGlvbkVycm9yKXtcbiAgICBpZiAodmUpIHtcbiAgICAgIHRoaXMuX3ZhbGlkYXRpb25FcnJvciA9IHZlO1xuICAgICAgdGhpcy5zZXRFcnJvcih2ZSlcbiAgICB9XG4gIH1cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBmYjogRm9ybUJ1aWxkZXIsXG4gICAgcHJpdmF0ZSBldERhdGFIYW5kbGVyU2VydmljZTogRXREYXRhSGFuZGxlclNlcnZpY2UsXG4gICAgcHJpdmF0ZSBfY2hhbmdlRGV0ZWN0b3JSZWY6IENoYW5nZURldGVjdG9yUmVmLFxuICApIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMuY3JlYXRlVGFibGVGb3JtKCk7XG4gICAgdGhpcy5zdWJzY3JpYmVUb1NlbmRDb2x1bW5OYW1lUmVjb3JkKCk7XG4gICAgdGhpcy5zdWJzY3JpYmVUb1ZhbGlkYXRlVGFibGUoKTtcbiAgICB0aGlzLmFzc2lnbk1hcmtBc1RvdWNoZWRGblRvQ29udHJvbCh0aGlzLmNvbnRyb2wpO1xuICAgIC8vIHRoaXMuc2V0UXVvdGF0aW9uTGlzdFRvVGFibGUoKTtcbiAgfVxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKTogdm9pZCB7XG4gICAgaWYgKCEhY2hhbmdlcy5wYXRjaFZhbHVlICYmICEhY2hhbmdlcy5wYXRjaFZhbHVlLmN1cnJlbnRWYWx1ZSkge1xuICAgICAgdGhpcy5fcGF0Y2hWYWx1ZSA9IGNoYW5nZXMucGF0Y2hWYWx1ZS5jdXJyZW50VmFsdWU7XG4gICAgICB0aGlzLnBhdGNoVmFsdWVUb1RhYmxlKHRoaXMuX3BhdGNoVmFsdWUpO1xuICAgIH1cbiAgICBpZiAoISFjaGFuZ2VzLmNvbnRyb2wgJiYgISFjaGFuZ2VzLmNvbnRyb2wuY3VycmVudFZhbHVlKSB7XG4gICAgICB0aGlzLmFzc2lnbk1hcmtBc1RvdWNoZWRGblRvQ29udHJvbCh0aGlzLmNvbnRyb2wpO1xuICAgIH1cbiAgfVxuICBhc3NpZ25NYXJrQXNUb3VjaGVkRm5Ub0NvbnRyb2woY29udHJvbDpBYnN0cmFjdENvbnRyb2wpe1xuICAgIGlmIChjb250cm9sKSB7XG4gICAgICBjb250cm9sLm1hcmtBc1RvdWNoZWQgPSAoKSA9PiB7XG4gICAgICAgIHRoaXMuZWRpdGFibGVUYWJsZUZvcm0ubWFya0FsbEFzVG91Y2hlZCgpO1xuICAgICAgfTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIHBhdGNoVmFsdWVUb1RhYmxlKHYpIHtcbiAgICBpZiAoISF2ICYmIHYubGVuZ3RoID4gMCAmJiB0aGlzLmVkaXRhYmxlVGFibGVGb3JtKSB7XG4gICAgICBpZiAoIXRoaXMuZWRpdGFibGVUYWJsZUZvcm0pIHtcbiAgICAgICAgdGhpcy5jcmVhdGVUYWJsZUZvcm0oKTtcbiAgICAgIH1cbiAgICAgIGxldCBmb3JtID0gKHRoaXMuZWRpdGFibGVUYWJsZUZvcm0uY29udHJvbHMudGFibGUgYXMgRm9ybUFycmF5KVxuICAgICAgbGV0IHJvd0luZGV4O1xuICAgICAgdi5mb3JFYWNoKGVsZSA9PiB7XG4gICAgICAgIGlmIChlbGUudGFibGVSb3dJZCkge1xuICAgICAgICAgIGZvciAoY29uc3Qga2V5IGluIGZvcm0udmFsdWUpIHtcbiAgICAgICAgICAgIGlmIChmb3JtLnZhbHVlLmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgICAgICAgICAgY29uc3QgdGFibGVSb3cgPSBmb3JtLnZhbHVlW2tleV07XG4gICAgICAgICAgICAgIGlmICh0YWJsZVJvd1sndGFibGVSb3dJZCddID09PSBlbGUudGFibGVSb3dJZCkge1xuICAgICAgICAgICAgICAgIHJvd0luZGV4ID0ga2V5O1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJvd0luZGV4ID0gKCFyb3dJbmRleCAmJiBlbGUucm93SW5kZXgpID8gZWxlLnJvd0luZGV4IDogcm93SW5kZXg7XG4gICAgICAgIHJvd0luZGV4ID8gZm9ybS5jb250cm9sc1tyb3dJbmRleF0ucGF0Y2hWYWx1ZShlbGUucGF0Y2hWYWx1ZSkgOiBjb25zb2xlLmVycm9yKCdwYXRjaCByb3cgdW5kZWZpbmVkJyk7XG4gICAgICB9KTtcbiAgICB9XG4gIH1cbiAgcHJpdmF0ZSBjcmVhdGVUYWJsZVdpdGhEYXRhKHRhYmxlRGF0YSkge1xuICAgIHRhYmxlRGF0YS5mb3JFYWNoKChlbGVtZW50LCBpbmRleCkgPT4ge1xuICAgICAgaWYgKGluZGV4ID09PSAwKSB7XG4gICAgICAgIHRoaXMucGF0Y2hWYWx1ZVRvVGFibGUoW3sgcm93SW5kZXg6ICcwJywgcGF0Y2hWYWx1ZTogZWxlbWVudCB9XSk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIHRoaXMuYWRkUm93KGVsZW1lbnQpO1xuICAgIH0pO1xuICB9XG4gIC8vIEBJbnB1dCgpIHByaXZhdGUgdGFibGVEYXRhO1xuICBwcml2YXRlIHN1YnNjcmliZVRvU2VuZENvbHVtbk5hbWVSZWNvcmQoKSB7XG4gICAgdGhpcy5ldERhdGFIYW5kbGVyU2VydmljZS5zZW5kQ29sdW1uTmFtZVJlY29yZCQuc3Vic2NyaWJlKChmb3JtQ29udHJvbE5hbWUpID0+IHtcbiAgICAgIGlmICh0aGlzLnZpZXdPbmx5KSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIGxldCBmb3JtID0gKHRoaXMuZWRpdGFibGVUYWJsZUZvcm0uY29udHJvbHMudGFibGUgYXMgRm9ybUFycmF5KTtcbiAgICAgIGxldCBjb2x1bW5WYWx1ZXMgPSBbXTtcbiAgICAgIGNvbnN0IHRhYmxlUmVjb3JkID0gZm9ybS5nZXRSYXdWYWx1ZSgpO1xuICAgICAgaWYgKGZvcm1Db250cm9sTmFtZS50b0xvd2VyQ2FzZSgpID09PSAnYWxsJykge1xuICAgICAgICBsZXQgc29ydGVkQ29uZmlnTGlzdCA9IHRoaXMudGFibGVDb25maWcuZmlsdGVyKGNvbmZpZyA9PiBjb25maWcuaXNOZWVkVmFsdWVDaGFuZ2VzKTtcbiAgICAgICAgc29ydGVkQ29uZmlnTGlzdC5mb3JFYWNoKChjb25maWcpID0+IHtcbiAgICAgICAgICBjb2x1bW5WYWx1ZXMgPSB0aGlzLmNvbGxlY3RDb2x1bW5SZWNvcmQodGFibGVSZWNvcmQsIGNvbmZpZy5mb3JtQ29udHJvbE5hbWUpXG4gICAgICAgICAgdGhpcy5ldERhdGFIYW5kbGVyU2VydmljZS5lbWl0Q29sdW1uUmVjb3JkKGNvbHVtblZhbHVlcywgY29uZmlnLmZvcm1Db250cm9sTmFtZSk7XG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICBjb2x1bW5WYWx1ZXMgPSB0aGlzLmNvbGxlY3RDb2x1bW5SZWNvcmQodGFibGVSZWNvcmQsIGZvcm1Db250cm9sTmFtZSlcbiAgICAgIHRoaXMuZXREYXRhSGFuZGxlclNlcnZpY2UuZW1pdENvbHVtblJlY29yZChjb2x1bW5WYWx1ZXMsIGZvcm1Db250cm9sTmFtZSk7XG4gICAgfSk7XG4gIH1cbiAgcHJpdmF0ZSBjb2xsZWN0Q29sdW1uUmVjb3JkKHRhYmxlUmVjb3JkOiBBcnJheTxvYmplY3Q+LCB0YWJsZVJvd0tleTogc3RyaW5nKSB7XG4gICAgbGV0IGNvbHVtblZhbHVlcyA9IFtdXG4gICAgZm9yIChjb25zdCBrZXkgaW4gdGFibGVSZWNvcmQpIHtcbiAgICAgIGlmICh0YWJsZVJlY29yZC5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG4gICAgICAgIGNvbnN0IHRhYmxlUm93ID0gdGFibGVSZWNvcmRba2V5XTtcbiAgICAgICAgY29sdW1uVmFsdWVzLnB1c2godGFibGVSb3dbdGFibGVSb3dLZXldKVxuICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gY29sdW1uVmFsdWVzO1xuICB9XG4gIHByaXZhdGUgc3Vic2NyaWJlVG9WYWxpZGF0ZVRhYmxlKCkge1xuICAgIGNvbnN0IHZhbGlkYXRlU2JzY3JiID0gdGhpcy5ldERhdGFIYW5kbGVyU2VydmljZS52YWxpZGF0ZVRhYmxlKCkuc3Vic2NyaWJlKHN0YXR1cyA9PiB7XG4gICAgICBpZiAoc3RhdHVzKSB7XG4gICAgICAgIHRoaXMuZWRpdGFibGVUYWJsZUZvcm0ubWFya0FsbEFzVG91Y2hlZCgpO1xuICAgICAgICBpZiAodGhpcy5lZGl0YWJsZVRhYmxlRm9ybS5zdGF0dXMgPT09ICdWQUxJRCcgfHwgdGhpcy5lZGl0YWJsZVRhYmxlRm9ybS5zdGF0dXMgPT09ICdESVNBQkxFRCcpIHtcbiAgICAgICAgICB0aGlzLmV0RGF0YUhhbmRsZXJTZXJ2aWNlLnNlbmRUYWJsZVJlY29yZCh7XG4gICAgICAgICAgICBkYXRhOiAodGhpcy5lZGl0YWJsZVRhYmxlRm9ybS5jb250cm9scy50YWJsZSBhcyBGb3JtQXJyYXkpLmdldFJhd1ZhbHVlKCksXG4gICAgICAgICAgICB2YWxpZDogdHJ1ZVxuICAgICAgICAgIH0pO1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmV0RGF0YUhhbmRsZXJTZXJ2aWNlLnNlbmRUYWJsZVJlY29yZCh7XG4gICAgICAgICAgZGF0YTogW10sXG4gICAgICAgICAgdmFsaWQ6IGZhbHNlXG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH0pO1xuICAgIHRoaXMuc3Vic2NyaXB0aW9uLmFkZCh2YWxpZGF0ZVNic2NyYik7XG4gIH1cbiAgY3JlYXRlVGFibGVGb3JtKCkge1xuICAgIGlmICh0aGlzLmVkaXRhYmxlVGFibGVGb3JtKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIGlmICh0aGlzLmV0Rm9ybUFycmF5KSB7XG4gICAgICB0aGlzLmVkaXRhYmxlVGFibGVGb3JtID0gdGhpcy5mYi5ncm91cCh7XG4gICAgICAgIHRhYmxlOiB0aGlzLmV0Rm9ybUFycmF5XG4gICAgICB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5lZGl0YWJsZVRhYmxlRm9ybSA9IHRoaXMuZmIuZ3JvdXAoe1xuICAgICAgICB0YWJsZTogdGhpcy5mYi5hcnJheShbXSlcbiAgICAgIH0pO1xuICAgIH1cbiAgICBpZiAoXG4gICAgICAhdGhpcy5oaWRlRGVmYXVsdFJvd1xuICAgICAgJiYgKHRoaXMuZWRpdGFibGVUYWJsZUZvcm0uY29udHJvbHMudGFibGUgYXMgRm9ybUFycmF5KS5sZW5ndGggPT09IDBcbiAgICAgICYmICh0aGlzLnRhYmxlQ29uZmlnLmxlbmd0aCA+IDAgfHwgdGhpcy5ldEZvcm1BcnJheS5sZW5ndGggPiAwKVxuICAgICkge1xuICAgICAgdGhpcy5hZGRSb3coKTtcbiAgICB9XG4gICAgdGhpcy50YWJsZVZhbHVlQ2hhbmdlcy5lbWl0KHRoaXMuZWRpdGFibGVUYWJsZUZvcm0udmFsdWVDaGFuZ2VzKTtcbiAgfVxuICBpbXBsZW1lbnRzRGV0YWlsc1JvdyhkYXRhPzogb2JqZWN0KSB7XG4gICAgY29uc3QgZmcgPSB0aGlzLmZiLmdyb3VwKHRoaXMuX2Zvcm1Hcm91cEZvclRhYmxlUm93KTtcbiAgICBpZiAoISFkYXRhICYmICF0aGlzLnZpZXdPbmx5KSB7XG4gICAgICBmZy5wYXRjaFZhbHVlKGRhdGEpO1xuICAgIH1cbiAgICBpZiAodGhpcy52aWV3T25seSkge1xuICAgICAgZmcuZGlzYWJsZSgpO1xuICAgICAgISFkYXRhID8gZmcucGF0Y2hWYWx1ZShkYXRhLCB7IG9ubHlTZWxmOiB0cnVlLCBlbWl0RXZlbnQ6IGZhbHNlIH0pIDogbnVsbDtcbiAgICB9XG4gICAgZmcuYWRkQ29udHJvbCgndGFibGVSb3dJZCcsIHRoaXMuZmIuY29udHJvbCh1dWlkLnY0KCkpKVxuICAgIHRoaXMudGFibGVDb25maWcuZm9yRWFjaCgoZWxlOiBUYWJsZUNvbmZpZykgPT4ge1xuICAgICAgLy8gZmcudmFsdWVDaGEgbmdlc1xuICAgICAgaWYgKGVsZS5pbnB1dEZpZWxkID09PSAnYXV0b2NvbXBsZXRlJyB8fCBlbGUuaXNOZWVkVmFsdWVDaGFuZ2VzKSB7XG4gICAgICAgIGZnLmNvbnRyb2xzW2VsZS5mb3JtQ29udHJvbE5hbWVdLnZhbHVlQ2hhbmdlcy5zdWJzY3JpYmUoc2VhcmNoVmFsdWUgPT4ge1xuICAgICAgICAgIHRoaXMuc2VhcmNoVmFsdWVDaGFuZ2VzLmVtaXQoeyBrZXk6IHNlYXJjaFZhbHVlLCBjb25maWc6IGVsZSwgdGFibGVSb3c6IGZnLmdldFJhd1ZhbHVlKCkgfSk7XG4gICAgICAgICAgdGhpcy5kaXJ0eUF1dG9jb21wbGV0ZUNvbHVtbiA9IHsgLi4uZWxlIH07XG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH0pO1xuICAgIHJldHVybiBmZztcbiAgfVxuICBwcml2YXRlIF9maWx0ZXIodmFsdWU6IHN0cmluZyB8IG9iamVjdCwgb3B0aW9uczogQXJyYXk8T2JqZWN0IHwgc3RyaW5nPiwga2V5KTogQXJyYXk8T2JqZWN0IHwgc3RyaW5nPiB7XG4gICAgbGV0IGZpbHRlclZhbHVlO1xuICAgIGlmICghIXZhbHVlIHx8IHZhbHVlID09PSBcIlwiKSB7XG4gICAgICByZXR1cm4gb3B0aW9ucztcbiAgICB9XG4gICAgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGZpbHRlclZhbHVlID0gdmFsdWUudG9Mb3dlckNhc2UoKTtcbiAgICB9XG4gICAgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcpIHtcbiAgICAgIGZpbHRlclZhbHVlID0gdmFsdWVba2V5XS50b0xvd2VyQ2FzZSgpO1xuICAgIH1cbiAgICByZXR1cm4gb3B0aW9ucy5maWx0ZXIob3B0aW9uID0+IG9wdGlvbltrZXldLnRvTG93ZXJDYXNlKCkuaW5kZXhPZihmaWx0ZXJWYWx1ZSkgPT09IDApO1xuICB9XG5cbiAgc3VibWl0RGF0YSgpIHtcbiAgfVxuICBhZGRSb3cocm93Pzogb2JqZWN0KSB7XG4gICAgaWYgKHRoaXMuZXRGb3JtQXJyYXkpIHtcbiAgICAgIHRoaXMuZWRpdGFibGVUYWJsZUZvcm0uY29udHJvbHMudGFibGUgPSB0aGlzLmV0Rm9ybUFycmF5O1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBsZXQgaW1wbGVtZW50c0RldGFpbHMgPSB0aGlzLmVkaXRhYmxlVGFibGVGb3JtLmNvbnRyb2xzLnRhYmxlIGFzIEZvcm1BcnJheVxuICAgIGltcGxlbWVudHNEZXRhaWxzLnB1c2godGhpcy5pbXBsZW1lbnRzRGV0YWlsc1Jvdyhyb3cpKTtcbiAgfVxuICByZXNldFRhYmxlKHJvdz8pIHtcbiAgICBsZXQgaW1wbGVtZW50c0RldGFpbHMgPSB0aGlzLmVkaXRhYmxlVGFibGVGb3JtLmNvbnRyb2xzLnRhYmxlIGFzIEZvcm1BcnJheVxuICAgIGltcGxlbWVudHNEZXRhaWxzLmNsZWFyKCk7XG4gICAgaW1wbGVtZW50c0RldGFpbHMucHVzaCh0aGlzLmltcGxlbWVudHNEZXRhaWxzUm93KHJvdykpO1xuICB9XG4gIGRlbGV0ZVJvdygpIHtcbiAgICBsZXQgaW1wbGVtZW50c0RldGFpbHMgPSB0aGlzLmVkaXRhYmxlVGFibGVGb3JtLmNvbnRyb2xzLnRhYmxlIGFzIEZvcm1BcnJheTtcbiAgICBsZXQgZGVsZXRlUmVjb3JkTGlzdCA9IFtdO1xuICAgIGxldCBub25TZWxlY3RlZCA9IGltcGxlbWVudHNEZXRhaWxzLmNvbnRyb2xzLmZpbHRlcigobWFjaGluZXJ5OiBGb3JtR3JvdXApID0+IHtcbiAgICAgIGlmIChtYWNoaW5lcnkudmFsdWUuaXNTZWxlY3RlZCkge1xuICAgICAgICBkZWxldGVSZWNvcmRMaXN0LnB1c2gobWFjaGluZXJ5LmdldFJhd1ZhbHVlKCkpO1xuICAgICAgfVxuICAgICAgcmV0dXJuICFtYWNoaW5lcnkudmFsdWUuaXNTZWxlY3RlZFxuICAgIH0pO1xuICAgIHRoaXMuZGVsZXRlZFRhYmxlUmVjb3Jkcy5lbWl0KGRlbGV0ZVJlY29yZExpc3QpO1xuICAgIGltcGxlbWVudHNEZXRhaWxzLmNsZWFyKClcbiAgICBub25TZWxlY3RlZC5mb3JFYWNoKGVsID0+IGltcGxlbWVudHNEZXRhaWxzLnB1c2goZWwpKVxuICB9XG5cbiAgcHJpdmF0ZSBnZW5lcmF0ZUF1dG9jb21wbGV0ZURpc3BsYXlGbihjb25maWc6IFRhYmxlQ29uZmlnKSB7XG4gICAgY29uc3Qga2V5ID0gY29uZmlnLnBhdGNoS2V5IHx8IGNvbmZpZy5kaXNwbGF5S2V5O1xuICAgIGNvbnN0IGRpc3BsYXlXaXRoID0gKHVzZXIpOiBzdHJpbmcgfCB1bmRlZmluZWQgPT4ge1xuICAgICAgcmV0dXJuICh1c2VyICYmIHR5cGVvZiB1c2VyID09PSAnb2JqZWN0JykgPyB1c2VyW2tleV0gOiB1c2VyO1xuICAgIH1cbiAgICAvLyByZXR1cm4gb2YoZGlzcGxheVdpdGgpO1xuICAgIHRoaXMubWF0QXV0b2NvbXBsZXRlRGlzcGxheUZuLm5leHQoZGlzcGxheVdpdGgpO1xuICAgIHRoaXMuX2NoYW5nZURldGVjdG9yUmVmLmRldGVjdENoYW5nZXMoKTtcbiAgfVxuICBhdXRvY29tcGxldGVDbGlja2VkKGNvbmZpZzogVGFibGVDb25maWcsIGZvcm1Hcm91cDogRm9ybUFycmF5KSB7XG4gICAgdGhpcy5maWx0ZXJlZE9wdGlvbnNbY29uZmlnLmZvcm1Db250cm9sTmFtZV0gPSBbXTtcbiAgICB0aGlzLmdlbmVyYXRlQXV0b2NvbXBsZXRlRGlzcGxheUZuKGNvbmZpZyk7XG4gICAgaWYgKGZvcm1Hcm91cC5jb250cm9sc1tjb25maWcuZm9ybUNvbnRyb2xOYW1lXS52YWx1ZSkge1xuICAgICAgdGhpcy5zZWFyY2hWYWx1ZUNoYW5nZXMuZW1pdCh7IGtleTogZm9ybUdyb3VwLmNvbnRyb2xzW2NvbmZpZy5mb3JtQ29udHJvbE5hbWVdLnZhbHVlLCBjb25maWc6IGNvbmZpZywgdGFibGVSb3c6IGZvcm1Hcm91cC5nZXRSYXdWYWx1ZSgpIH0pO1xuICAgICAgdGhpcy5kaXJ0eUF1dG9jb21wbGV0ZUNvbHVtbiA9IHsgLi4uY29uZmlnIH07XG4gICAgfVxuICB9XG4gIHZhbHVlU2VsZWN0ZWRGcm9tQXV0b2NvbXBsZXRlKGV2ZW50OiBNYXRBdXRvY29tcGxldGVTZWxlY3RlZEV2ZW50LCByb3dJbmRleCwgcm93Rm9ybUdyb3VwOiBGb3JtR3JvdXAsIGNvbmZpZzogVGFibGVDb25maWcpIHtcbiAgICBsZXQgb3B0aW9uID0gKGV2ZW50ICYmIHR5cGVvZiBldmVudC5vcHRpb24udmFsdWUgPT09ICdvYmplY3QnKSA/IHsgLi4uZXZlbnQub3B0aW9uLnZhbHVlIH0gOiBldmVudC5vcHRpb24udmFsdWUgfHwgJyc7XG4gICAgdGhpcy5vcHRpb25TZWxlY3RlZC5lbWl0KHsgb3B0aW9uLCAuLi57IHJvd0luZGV4LCB0YWJsZVJvdzogcm93Rm9ybUdyb3VwLmdldFJhd1ZhbHVlKCksIGNvbmZpZyB9IH0pO1xuICB9XG4gIHNlbGVjdGlvbkNoYW5nZWQoZXZlbnQpIHtcblxuICB9XG4gIGNvbnRlbnRlZGl0YWJsZVZhbHVlY2hhbmdlKHZhbHVlLCByb3dGb3JtR3JvdXA6IEZvcm1Hcm91cCwgZm9ybUNvbnRyb2xOYW1lKSB7XG4gICAgcm93Rm9ybUdyb3VwLmNvbnRyb2xzW2Zvcm1Db250cm9sTmFtZV0ucGF0Y2hWYWx1ZSh2YWx1ZSk7XG5cbiAgfVxuICBwcml2YXRlIHNldEVycm9yKHZlOiBFVFZhbGlkYXRpb25FcnJvcil7XG4gICAgY29uc3QgZm9ybUFycmF5ID0gKCB0aGlzLmVkaXRhYmxlVGFibGVGb3JtLmdldCgndGFibGUnKSBhcyBGb3JtQXJyYXkpO1xuICAgIGNvbnN0IGZvcm1JbmRleCA9IGZvcm1BcnJheS5nZXRSYXdWYWx1ZSgpLmZpbmRJbmRleCgoZm9ybVZhbHVlKT0+e1xuICAgICAgaWYgKGZvcm1WYWx1ZS50YWJsZVJvd0lkID09PSB2ZS50YWJsZVJvd0lkKSB7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgfVxuICAgIH0pO1xuICAgIGNvbnN0IHRhYmxlUm93RkcgPSBmb3JtQXJyYXkuYXQoZm9ybUluZGV4KTtcbiAgICBpZiAodGFibGVSb3dGRykge1xuICAgICAgaWYgKHZlLmlzUmVtb3ZlKSB7XG4gICAgICAgIHRhYmxlUm93RkcuZ2V0KHZlLmZvcm1Db250cm9sTmFtZSkuc2V0RXJyb3JzKG51bGwpO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICB0YWJsZVJvd0ZHLmdldCh2ZS5mb3JtQ29udHJvbE5hbWUpLnNldEVycm9ycyh7XG4gICAgICAgIG1zZzp2ZS5tc2csXG4gICAgICAgIC4uLnZlLnR5cGVcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIC8qID09PT09PT09PT09PT09PT0gIE5HX1ZBTFVFX0FDQ0VTU09SICYmIE5HX1ZBTElEQVRPUlMgPT09PT09PT09PT09PT09Ki9cbiAgcHVibGljIG9uVG91Y2hlZDogKCkgPT4gdm9pZCA9ICgpID0+IHtcbiAgICB0aGlzLmVkaXRhYmxlVGFibGVGb3JtLm1hcmtBbGxBc1RvdWNoZWQoKTtcbiAgfTtcbiAgd3JpdGVWYWx1ZSh2YWw6IGFueSk6IHZvaWQge1xuICAgIGlmICh2YWwgJiYgdmFsLnJlc2V0Rm9ybSkge1xuICAgICAgdGhpcy5lZGl0YWJsZVRhYmxlRm9ybS5yZXNldCgpO1xuICAgICAgdGhpcy5yZXNldFRhYmxlKCk7XG4gICAgICByZXR1cm47XG4gICAgfVxuICAgIHZhbCAmJiB2YWwubGVuZ3RoID4gMCAmJiB0aGlzLmNyZWF0ZVRhYmxlV2l0aERhdGEodmFsKTtcbiAgfVxuICByZWdpc3Rlck9uQ2hhbmdlKGZuOiBhbnkpOiB2b2lkIHtcbiAgICBmbiAmJiB0aGlzLmVkaXRhYmxlVGFibGVGb3JtLmNvbnRyb2xzLnRhYmxlLnZhbHVlQ2hhbmdlcy5waXBlKFxuICAgICAgbWFwKCh2YWx1ZSkgPT4gKHRoaXMuZWRpdGFibGVUYWJsZUZvcm0uY29udHJvbHMudGFibGUgYXMgRm9ybUFycmF5KS5nZXRSYXdWYWx1ZSgpKVxuICAgICkuc3Vic2NyaWJlKGZuKTtcbiAgfVxuICByZWdpc3Rlck9uVG91Y2hlZChmbjogYW55KTogdm9pZCB7XG4gICAgZm4gJiYgKHRoaXMub25Ub3VjaGVkID0gZm4pO1xuICB9XG4gIHNldERpc2FibGVkU3RhdGU/KGlzRGlzYWJsZWQ6IGJvb2xlYW4pOiB2b2lkIHtcbiAgICBpc0Rpc2FibGVkID8gdGhpcy5lZGl0YWJsZVRhYmxlRm9ybS5jb250cm9scy50YWJsZS5kaXNhYmxlKCkgOiB0aGlzLmVkaXRhYmxlVGFibGVGb3JtLmNvbnRyb2xzLnRhYmxlLmVuYWJsZSgpO1xuICAgIC8vIHRocm93IG5ldyBFcnJvcihcIk1ldGhvZCBub3QgaW1wbGVtZW50ZWQuXCIpO1xuICB9XG4gIHZhbGlkYXRlKGNvbnRyb2w6IEFic3RyYWN0Q29udHJvbCk6IFZhbGlkYXRpb25FcnJvcnMge1xuICAgIHJldHVybiB0aGlzLmVkaXRhYmxlVGFibGVGb3JtLmNvbnRyb2xzLnRhYmxlLnZhbGlkID8gbnVsbCA6IHsgaW52YWxpZEZvcm06IHsgdmFsaWQ6IGZhbHNlLCBtZXNzYWdlOiBcImVkaXRhYmxlVGFibGVGb3JtIGZpZWxkcyBhcmUgaW52YWxpZFwiIH0gfTtcbiAgfVxuICAvKiA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSAqL1xuXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xuICAgIHRoaXMuc3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XG4gIH1cbn1cbiJdfQ==