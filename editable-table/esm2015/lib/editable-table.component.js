/**
 * @fileoverview added by tsickle
 * Generated from: lib/editable-table.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, Output, EventEmitter, forwardRef, ChangeDetectorRef } from '@angular/core';
import { FormArray, FormBuilder, NG_VALUE_ACCESSOR, NG_VALIDATORS, FormControl } from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { ETValidationError } from './editable-table-config';
import * as uuid from 'uuid';
import { EtDataHandlerService } from './et-data-handler.service';
export class EditableTableComponent {
    /**
     * @param {?} fb
     * @param {?} etDataHandlerService
     * @param {?} _changeDetectorRef
     */
    constructor(fb, etDataHandlerService, _changeDetectorRef) {
        this.fb = fb;
        this.etDataHandlerService = etDataHandlerService;
        this._changeDetectorRef = _changeDetectorRef;
        this.filteredOptions = {};
        this.selectOptions = {};
        this.subscription = new Subscription();
        this.viewOnly = false;
        this.hideActionBtn = false;
        this.hideDefaultRow = false;
        this.tableConfig = (/** @type {?} */ ([]));
        this.getTableRecordChange = new EventEmitter();
        this.optionSelected = new EventEmitter();
        this.searchValueChanges = new EventEmitter();
        this.tableValueChanges = new EventEmitter();
        this.deletedTableRecords = new EventEmitter();
        this.matAutocompleteDisplayFn = new Subject();
        /* ================  NG_VALUE_ACCESSOR && NG_VALIDATORS ===============*/
        this.onTouched = (/**
         * @return {?}
         */
        () => {
            this.editableTableForm.markAllAsTouched();
        });
    }
    /**
     * @param {?} v
     * @return {?}
     */
    set assignListToSelect(v) {
        if (!!v) {
            v.forEach((/**
             * @param {?} ele
             * @return {?}
             */
            ele => {
                this.selectOptions[ele.formControlName] = ele.list;
            }));
        }
    }
    /**
     * @param {?} v
     * @return {?}
     */
    set getFormData(v) {
        v.subscribe((/**
         * @param {?} res
         * @return {?}
         */
        res => {
            if (!!res) {
                this.getTableRecordChange.emit(((/** @type {?} */ (this.editableTableForm.controls.table))).getRawValue());
            }
        }));
    }
    /**
     * @param {?} v
     * @return {?}
     */
    set assignListToAutocomplete(v) {
        if (v && v.config.inputField === 'autocomplete') {
            this.filteredOptions[v.config.formControlName] = this._filter(v.searchKey, v.list, v.config.displayKey);
        }
    }
    /**
     * @param {?} v
     * @return {?}
     */
    set patchValue(v) {
        this._patchValue = v;
    }
    /**
     * @param {?} v
     * @return {?}
     */
    set patchValueToSelect(v) {
        if (!!v && this.editableTableForm) {
            /** @type {?} */
            let form = ((/** @type {?} */ (this.editableTableForm.controls.table)));
            v.forEach((/**
             * @param {?} ele
             * @return {?}
             */
            ele => {
                if (form.controls[ele.rowIndex]) {
                    form.controls[ele.rowIndex].patchValue(ele.patchValue);
                }
            }));
        }
    }
    /**
     * @param {?} v
     * @return {?}
     */
    set tableData(v) {
        {
            if (!!v && v.length > 0) {
                this._tableData = v;
                this.createTableWithData(v);
            }
        }
    }
    /**
     * @param {?} fb
     * @return {?}
     */
    set formGroupForTableRow(fb) {
        this._formGroupForTableRow = Object.assign({}, fb);
    }
    /**
     * @param {?} ve
     * @return {?}
     */
    set validationError(ve) {
        if (ve) {
            this._validationError = ve;
            this.setError(ve);
        }
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.createTableForm();
        this.subscribeToSendColumnNameRecord();
        this.subscribeToValidateTable();
        this.assignMarkAsTouchedFnToControl(this.control);
        // this.setQuotationListToTable();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (!!changes.patchValue && !!changes.patchValue.currentValue) {
            this._patchValue = changes.patchValue.currentValue;
            this.patchValueToTable(this._patchValue);
        }
        if (!!changes.control && !!changes.control.currentValue) {
            this.assignMarkAsTouchedFnToControl(this.control);
        }
    }
    /**
     * @param {?} control
     * @return {?}
     */
    assignMarkAsTouchedFnToControl(control) {
        if (control) {
            control.markAsTouched = (/**
             * @return {?}
             */
            () => {
                this.editableTableForm.markAllAsTouched();
            });
        }
    }
    /**
     * @private
     * @param {?} v
     * @return {?}
     */
    patchValueToTable(v) {
        if (!!v && v.length > 0 && this.editableTableForm) {
            if (!this.editableTableForm) {
                this.createTableForm();
            }
            /** @type {?} */
            let form = ((/** @type {?} */ (this.editableTableForm.controls.table)));
            /** @type {?} */
            let rowIndex;
            v.forEach((/**
             * @param {?} ele
             * @return {?}
             */
            ele => {
                if (ele.tableRowId) {
                    for (const key in form.value) {
                        if (form.value.hasOwnProperty(key)) {
                            /** @type {?} */
                            const tableRow = form.value[key];
                            if (tableRow['tableRowId'] === ele.tableRowId) {
                                rowIndex = key;
                            }
                        }
                    }
                }
                rowIndex = (!rowIndex && ele.rowIndex) ? ele.rowIndex : rowIndex;
                rowIndex ? form.controls[rowIndex].patchValue(ele.patchValue) : console.error('patch row undefined');
            }));
        }
    }
    /**
     * @private
     * @param {?} tableData
     * @return {?}
     */
    createTableWithData(tableData) {
        tableData.forEach((/**
         * @param {?} element
         * @param {?} index
         * @return {?}
         */
        (element, index) => {
            if (index === 0) {
                this.patchValueToTable([{ rowIndex: '0', patchValue: element }]);
                return;
            }
            this.addRow(element);
        }));
    }
    // @Input() private tableData;
    /**
     * @private
     * @return {?}
     */
    subscribeToSendColumnNameRecord() {
        this.etDataHandlerService.sendColumnNameRecord$.subscribe((/**
         * @param {?} formControlName
         * @return {?}
         */
        (formControlName) => {
            if (this.viewOnly) {
                return;
            }
            /** @type {?} */
            let form = ((/** @type {?} */ (this.editableTableForm.controls.table)));
            /** @type {?} */
            let columnValues = [];
            /** @type {?} */
            const tableRecord = form.getRawValue();
            if (formControlName.toLowerCase() === 'all') {
                /** @type {?} */
                let sortedConfigList = this.tableConfig.filter((/**
                 * @param {?} config
                 * @return {?}
                 */
                config => config.isNeedValueChanges));
                sortedConfigList.forEach((/**
                 * @param {?} config
                 * @return {?}
                 */
                (config) => {
                    columnValues = this.collectColumnRecord(tableRecord, config.formControlName);
                    this.etDataHandlerService.emitColumnRecord(columnValues, config.formControlName);
                }));
                return;
            }
            columnValues = this.collectColumnRecord(tableRecord, formControlName);
            this.etDataHandlerService.emitColumnRecord(columnValues, formControlName);
        }));
    }
    /**
     * @private
     * @param {?} tableRecord
     * @param {?} tableRowKey
     * @return {?}
     */
    collectColumnRecord(tableRecord, tableRowKey) {
        /** @type {?} */
        let columnValues = [];
        for (const key in tableRecord) {
            if (tableRecord.hasOwnProperty(key)) {
                /** @type {?} */
                const tableRow = tableRecord[key];
                columnValues.push(tableRow[tableRowKey]);
            }
        }
        return columnValues;
    }
    /**
     * @private
     * @return {?}
     */
    subscribeToValidateTable() {
        /** @type {?} */
        const validateSbscrb = this.etDataHandlerService.validateTable().subscribe((/**
         * @param {?} status
         * @return {?}
         */
        status => {
            if (status) {
                this.editableTableForm.markAllAsTouched();
                if (this.editableTableForm.status === 'VALID' || this.editableTableForm.status === 'DISABLED') {
                    this.etDataHandlerService.sendTableRecord({
                        data: ((/** @type {?} */ (this.editableTableForm.controls.table))).getRawValue(),
                        valid: true
                    });
                    return;
                }
                this.etDataHandlerService.sendTableRecord({
                    data: [],
                    valid: false
                });
            }
        }));
        this.subscription.add(validateSbscrb);
    }
    /**
     * @return {?}
     */
    createTableForm() {
        if (this.editableTableForm) {
            return;
        }
        if (this.etFormArray) {
            this.editableTableForm = this.fb.group({
                table: this.etFormArray
            });
        }
        else {
            this.editableTableForm = this.fb.group({
                table: this.fb.array([])
            });
        }
        if (!this.hideDefaultRow
            && ((/** @type {?} */ (this.editableTableForm.controls.table))).length === 0
            && (this.tableConfig.length > 0 || this.etFormArray.length > 0)) {
            this.addRow();
        }
        this.tableValueChanges.emit(this.editableTableForm.valueChanges);
    }
    /**
     * @param {?=} data
     * @return {?}
     */
    implementsDetailsRow(data) {
        /** @type {?} */
        const fg = this.fb.group(this._formGroupForTableRow);
        if (!!data && !this.viewOnly) {
            fg.patchValue(data);
        }
        if (this.viewOnly) {
            fg.disable();
            !!data ? fg.patchValue(data, { onlySelf: true, emitEvent: false }) : null;
        }
        fg.addControl('tableRowId', this.fb.control(uuid.v4()));
        this.tableConfig.forEach((/**
         * @param {?} ele
         * @return {?}
         */
        (ele) => {
            // fg.valueCha nges
            if (ele.inputField === 'autocomplete' || ele.isNeedValueChanges) {
                fg.controls[ele.formControlName].valueChanges.subscribe((/**
                 * @param {?} searchValue
                 * @return {?}
                 */
                searchValue => {
                    this.searchValueChanges.emit({ key: searchValue, config: ele, tableRow: fg.getRawValue() });
                    this.dirtyAutocompleteColumn = Object.assign({}, ele);
                }));
            }
        }));
        return fg;
    }
    /**
     * @private
     * @param {?} value
     * @param {?} options
     * @param {?} key
     * @return {?}
     */
    _filter(value, options, key) {
        /** @type {?} */
        let filterValue;
        if (!!value || value === "") {
            return options;
        }
        if (typeof value === 'string') {
            filterValue = value.toLowerCase();
        }
        if (typeof value === 'object') {
            filterValue = value[key].toLowerCase();
        }
        return options.filter((/**
         * @param {?} option
         * @return {?}
         */
        option => option[key].toLowerCase().indexOf(filterValue) === 0));
    }
    /**
     * @return {?}
     */
    submitData() {
    }
    /**
     * @param {?=} row
     * @return {?}
     */
    addRow(row) {
        if (this.etFormArray) {
            this.editableTableForm.controls.table = this.etFormArray;
            return;
        }
        /** @type {?} */
        let implementsDetails = (/** @type {?} */ (this.editableTableForm.controls.table));
        implementsDetails.push(this.implementsDetailsRow(row));
    }
    /**
     * @param {?=} row
     * @return {?}
     */
    resetTable(row) {
        /** @type {?} */
        let implementsDetails = (/** @type {?} */ (this.editableTableForm.controls.table));
        implementsDetails.clear();
        implementsDetails.push(this.implementsDetailsRow(row));
    }
    /**
     * @return {?}
     */
    deleteRow() {
        /** @type {?} */
        let implementsDetails = (/** @type {?} */ (this.editableTableForm.controls.table));
        /** @type {?} */
        let deleteRecordList = [];
        /** @type {?} */
        let nonSelected = implementsDetails.controls.filter((/**
         * @param {?} machinery
         * @return {?}
         */
        (machinery) => {
            if (machinery.value.isSelected) {
                deleteRecordList.push(machinery.getRawValue());
            }
            return !machinery.value.isSelected;
        }));
        this.deletedTableRecords.emit(deleteRecordList);
        implementsDetails.clear();
        nonSelected.forEach((/**
         * @param {?} el
         * @return {?}
         */
        el => implementsDetails.push(el)));
    }
    /**
     * @private
     * @param {?} config
     * @return {?}
     */
    generateAutocompleteDisplayFn(config) {
        /** @type {?} */
        const key = config.patchKey || config.displayKey;
        /** @type {?} */
        const displayWith = (/**
         * @param {?} user
         * @return {?}
         */
        (user) => {
            return (user && typeof user === 'object') ? user[key] : user;
        })
        // return of(displayWith);
        ;
        // return of(displayWith);
        this.matAutocompleteDisplayFn.next(displayWith);
        this._changeDetectorRef.detectChanges();
    }
    /**
     * @param {?} config
     * @param {?} formGroup
     * @return {?}
     */
    autocompleteClicked(config, formGroup) {
        this.filteredOptions[config.formControlName] = [];
        this.generateAutocompleteDisplayFn(config);
        if (formGroup.controls[config.formControlName].value) {
            this.searchValueChanges.emit({ key: formGroup.controls[config.formControlName].value, config: config, tableRow: formGroup.getRawValue() });
            this.dirtyAutocompleteColumn = Object.assign({}, config);
        }
    }
    /**
     * @param {?} event
     * @param {?} rowIndex
     * @param {?} rowFormGroup
     * @param {?} config
     * @return {?}
     */
    valueSelectedFromAutocomplete(event, rowIndex, rowFormGroup, config) {
        /** @type {?} */
        let option = (event && typeof event.option.value === 'object') ? Object.assign({}, event.option.value) : event.option.value || '';
        this.optionSelected.emit(Object.assign({ option }, { rowIndex, tableRow: rowFormGroup.getRawValue(), config }));
    }
    /**
     * @param {?} event
     * @return {?}
     */
    selectionChanged(event) {
    }
    /**
     * @param {?} value
     * @param {?} rowFormGroup
     * @param {?} formControlName
     * @return {?}
     */
    contenteditableValuechange(value, rowFormGroup, formControlName) {
        rowFormGroup.controls[formControlName].patchValue(value);
    }
    /**
     * @private
     * @param {?} ve
     * @return {?}
     */
    setError(ve) {
        /** @type {?} */
        const formArray = ((/** @type {?} */ (this.editableTableForm.get('table'))));
        /** @type {?} */
        const formIndex = formArray.getRawValue().findIndex((/**
         * @param {?} formValue
         * @return {?}
         */
        (formValue) => {
            if (formValue.tableRowId === ve.tableRowId) {
                return true;
            }
        }));
        /** @type {?} */
        const tableRowFG = formArray.at(formIndex);
        if (tableRowFG) {
            if (ve.isRemove) {
                tableRowFG.get(ve.formControlName).setErrors(null);
                return;
            }
            tableRowFG.get(ve.formControlName).setErrors(Object.assign({ msg: ve.msg }, ve.type));
        }
    }
    /**
     * @param {?} val
     * @return {?}
     */
    writeValue(val) {
        if (val && val.resetForm) {
            this.editableTableForm.reset();
            this.resetTable();
            return;
        }
        val && val.length > 0 && this.createTableWithData(val);
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        fn && this.editableTableForm.controls.table.valueChanges.pipe(map((/**
         * @param {?} value
         * @return {?}
         */
        (value) => ((/** @type {?} */ (this.editableTableForm.controls.table))).getRawValue()))).subscribe(fn);
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        fn && (this.onTouched = fn);
    }
    /**
     * @param {?} isDisabled
     * @return {?}
     */
    setDisabledState(isDisabled) {
        isDisabled ? this.editableTableForm.controls.table.disable() : this.editableTableForm.controls.table.enable();
        // throw new Error("Method not implemented.");
    }
    /**
     * @param {?} control
     * @return {?}
     */
    validate(control) {
        return this.editableTableForm.controls.table.valid ? null : { invalidForm: { valid: false, message: "editableTableForm fields are invalid" } };
    }
    /* ==================================================================== */
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
EditableTableComponent.decorators = [
    { type: Component, args: [{
                selector: 'editable-table',
                template: "<div id=\"divshow\">\n  <form [formGroup]=\"editableTableForm\">\n    <div>\n      <div class=\"table-responsive\">\n        <table class=\"table table-bordered table-sm\" formArrayName=\"table\">\n          <thead class=\"thead-light\">\n            <tr>\n              <th scope=\"col\" style=\"width: 10px;\" *ngFor=\"let config of tableConfig\">{{config.title}}</th>\n            </tr>\n          </thead>\n          <tbody>\n            <ng-container *ngFor=\"let rowFormGroup of editableTableForm['controls'].table['controls']; let i = index\"\n              [formGroupName]=\"i\">\n              <tr>\n                <ng-container *ngFor=\"let config of tableConfig\">\n                  <td *ngIf=\"config.inputField !== 'contenteditable'\">\n                    <mat-checkbox *ngIf=\"config.inputField === 'checkbox'\" color=\"basic\"\n                      class=\"form-control form-control-sm\" [formControlName]=\"config.formControlName\" [checked]=\"false\">\n                    </mat-checkbox>\n                    <mat-form-field class=\"example-full-width\" *ngIf=\"config.inputField === 'input'\"\n                      appearance=\"outline\" [ngClass]=\"{pb_0:!rowFormGroup.get(config.formControlName).errors}\">\n                      <input matInput [placeholder]=\"config.title\" [formControlName]=\"config.formControlName\">\n                      <mat-error\n                        *ngIf=\"rowFormGroup.get(config.formControlName).errors && rowFormGroup.get(config.formControlName).errors.msg &&!rowFormGroup.get(config.formControlName).errors.required\">\n                        {{rowFormGroup.get(config.formControlName).errors.msg}}</mat-error>\n                      <mat-error\n                        *ngIf=\"rowFormGroup.get(config.formControlName).errors && !rowFormGroup.get(config.formControlName).errors.msg &&rowFormGroup.get(config.formControlName).errors.required\">\n                        This field is required</mat-error>\n                    </mat-form-field>\n                    <mat-form-field *ngIf=\"config.inputField === 'autocomplete'\" class=\"w-100\"\n                      [ngClass]=\"{pb_0:!rowFormGroup.get(config.formControlName).errors}\" appearance=\"outline\">\n                      <input matInput [formControlName]=\"config.formControlName\" [matAutocomplete]=\"autoAmcNumber\"\n                        (focus)=\"autocompleteClicked(config, rowFormGroup)\" (keyup.enter)=\"$event.preventDefault();\">\n                      <!-- (blur)=\"resetFormControlNotHavingObject(searchAMCForm.controls.amcNumber, 'amcNumber')\" -->\n                      <mat-autocomplete #autoAmcNumber=\"matAutocomplete\"\n                        [displayWith]=\"matAutocompleteDisplayFn | async\"\n                        (optionSelected)=\"valueSelectedFromAutocomplete($event, i, rowFormGroup, config)\">\n                        <mat-option *ngFor=\"let option of filteredOptions[config.formControlName]\" [value]=\"option\">\n                          <!-- (click)=\"valueSelectedFromAutocomplete(option)\" -->\n                          {{option[config.displayKey]? option[config.displayKey] : option}}\n                        </mat-option>\n                      </mat-autocomplete>\n                      <mat-icon matSuffix>search</mat-icon>\n                      <mat-error\n                        *ngIf=\"rowFormGroup.get(config.formControlName).errors && rowFormGroup.get(config.formControlName).errors.msg &&!rowFormGroup.get(config.formControlName).errors.required\">\n                        {{rowFormGroup.get(config.formControlName).errors.msg}}</mat-error>\n                      <mat-error\n                        *ngIf=\"rowFormGroup.get(config.formControlName).errors && !rowFormGroup.get(config.formControlName).errors.msg &&rowFormGroup.get(config.formControlName).errors.required\">\n                        This field is required</mat-error>\n                    </mat-form-field>\n                    <!-- <span *ngIf=\"config.inputField === 'autocomplete'\">\n                      {{matAutocompleteDisplayFn | async}}\n                    </span> -->\n                    <mat-form-field *ngIf=\"config.inputField === 'select'\" appearance=\"outline\" class=\"pb_0\">\n                      <mat-label>Select {{config.title}}</mat-label>\n                      <mat-select [formControlName]=\"config.formControlName\"\n                        (selectionChange)=\"selectionChanged($event)\">\n                        <mat-option *ngFor=\"let option of selectOptions[config.formControlName]\" [value]=\"option\">\n                          {{option[config.displayKey]? option[config.displayKey] : option}}\n                        </mat-option>\n                      </mat-select>\n                      <mat-error\n                      *ngIf=\"rowFormGroup.get(config.formControlName).errors && rowFormGroup.get(config.formControlName).errors.msg &&!rowFormGroup.get(config.formControlName).errors.required\">\n                      {{rowFormGroup.get(config.formControlName).errors.msg}}</mat-error>\n                      <mat-error\n                        *ngIf=\"rowFormGroup.get(config.formControlName).errors && !rowFormGroup.get(config.formControlName).errors.msg &&rowFormGroup.get(config.formControlName).errors.required\">\n                        This field is required</mat-error>\n                    </mat-form-field>\n                  </td>\n                  <td *ngIf=\"config.inputField === 'contenteditable'\"\n                    [attr.contenteditable]=\"config.inputField !== 'contenteditable' || config.isNeedValueChanges\"\n                    [textContent]=\"rowFormGroup.controls[config.formControlName].value\"\n                    (input)=\"contenteditableValuechange($event.target.textContent,rowFormGroup,config.formControlName)\"\n                    #contenteditableRef></td>\n                </ng-container>\n              </tr>\n              <!-- <ng-content select=\"tr\"></ng-content> -->\n            </ng-container>\n          </tbody>\n          <tfoot>\n            <ng-content select=\"ng-container.table_footer\"></ng-content>\n          </tfoot>\n        </table>\n      </div>\n    </div>\n  </form>\n  <div class=\" text-center mt-2\" *ngIf=\"!hideActionBtn && !viewOnly \">\n    <button type=\"button\" class=\"btn btn-sm btn_primary \" (click)=\"addRow()\"> Add\n      Row</button>\n    <button type=\"button\" class=\"btn btn-sm btn_secondary ml-2 \" (click)=\"deleteRow()\"> Delete Row </button>\n  </div>\n</div>",
                providers: [
                    {
                        provide: NG_VALUE_ACCESSOR,
                        useExisting: forwardRef((/**
                         * @return {?}
                         */
                        () => EditableTableComponent)),
                        multi: true
                    },
                    {
                        provide: NG_VALIDATORS,
                        useExisting: forwardRef((/**
                         * @return {?}
                         */
                        () => EditableTableComponent)),
                        multi: true
                    }
                ],
                styles: [""]
            }] }
];
/** @nocollapse */
EditableTableComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: EtDataHandlerService },
    { type: ChangeDetectorRef }
];
EditableTableComponent.propDecorators = {
    viewOnly: [{ type: Input }],
    hideActionBtn: [{ type: Input }],
    hideDefaultRow: [{ type: Input }],
    assignListToSelect: [{ type: Input }],
    tableConfig: [{ type: Input }],
    getFormData: [{ type: Input }],
    getTableRecordChange: [{ type: Output }],
    optionSelected: [{ type: Output }],
    assignListToAutocomplete: [{ type: Input }],
    patchValue: [{ type: Input }],
    patchValueToSelect: [{ type: Input }],
    tableData: [{ type: Input, args: ['tableData',] }],
    searchValueChanges: [{ type: Output }],
    tableValueChanges: [{ type: Output }],
    formGroupForTableRow: [{ type: Input }],
    deletedTableRecords: [{ type: Output }],
    matAutocompleteDisplayFn: [{ type: Input }],
    etFormArray: [{ type: Input }],
    control: [{ type: Input }],
    validationError: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    EditableTableComponent.prototype.editableTableForm;
    /** @type {?} */
    EditableTableComponent.prototype.isEdit;
    /** @type {?} */
    EditableTableComponent.prototype.isView;
    /** @type {?} */
    EditableTableComponent.prototype.filteredOptions;
    /** @type {?} */
    EditableTableComponent.prototype.selectOptions;
    /** @type {?} */
    EditableTableComponent.prototype.subscription;
    /** @type {?} */
    EditableTableComponent.prototype.viewOnly;
    /** @type {?} */
    EditableTableComponent.prototype.hideActionBtn;
    /** @type {?} */
    EditableTableComponent.prototype.hideDefaultRow;
    /** @type {?} */
    EditableTableComponent.prototype.tableConfig;
    /** @type {?} */
    EditableTableComponent.prototype.getTableRecordChange;
    /** @type {?} */
    EditableTableComponent.prototype.optionSelected;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype._patchValue;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype.dirtyAutocompleteColumn;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype._tableData;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype._formGroupForTableRow;
    /** @type {?} */
    EditableTableComponent.prototype.searchValueChanges;
    /** @type {?} */
    EditableTableComponent.prototype.tableValueChanges;
    /** @type {?} */
    EditableTableComponent.prototype.deletedTableRecords;
    /** @type {?} */
    EditableTableComponent.prototype.matAutocompleteDisplayFn;
    /** @type {?} */
    EditableTableComponent.prototype.etFormArray;
    /** @type {?} */
    EditableTableComponent.prototype.control;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype._validationError;
    /** @type {?} */
    EditableTableComponent.prototype.onTouched;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype.fb;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype.etDataHandlerService;
    /**
     * @type {?}
     * @private
     */
    EditableTableComponent.prototype._changeDetectorRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWRpdGFibGUtdGFibGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWRpdGFibGUtdGFibGUvIiwic291cmNlcyI6WyJsaWIvZWRpdGFibGUtdGFibGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQWlCLE1BQU0sRUFBRSxZQUFZLEVBQTRCLFVBQVUsRUFBYSxpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNsSyxPQUFPLEVBQUUsU0FBUyxFQUFjLFdBQVcsRUFBOEMsaUJBQWlCLEVBQUUsYUFBYSxFQUFxQyxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNsTSxPQUFPLEVBQWtCLE9BQU8sRUFBRSxZQUFZLEVBQW1CLE1BQU0sTUFBTSxDQUFDO0FBQzlFLE9BQU8sRUFBRSxHQUFHLEVBQWEsTUFBTSxnQkFBZ0IsQ0FBQztBQUNoRCxPQUFPLEVBQWlGLGlCQUFpQixFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFFM0ksT0FBTyxLQUFLLElBQUksTUFBTSxNQUFNLENBQUM7QUFDN0IsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFvQmpFLE1BQU0sT0FBTyxzQkFBc0I7Ozs7OztJQTZFakMsWUFDVSxFQUFlLEVBQ2Ysb0JBQTBDLEVBQzFDLGtCQUFxQztRQUZyQyxPQUFFLEdBQUYsRUFBRSxDQUFhO1FBQ2YseUJBQW9CLEdBQXBCLG9CQUFvQixDQUFzQjtRQUMxQyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW1CO1FBNUUvQyxvQkFBZSxHQUFHLEVBQUUsQ0FBQztRQUNyQixrQkFBYSxHQUFHLEVBQUUsQ0FBQztRQUNuQixpQkFBWSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDekIsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNqQixrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUN0QixtQkFBYyxHQUFZLEtBQUssQ0FBQztRQVV6QyxnQkFBVyxHQUFHLG1CQUFBLEVBQUUsRUFBc0IsQ0FBQztRQVE3Qix5QkFBb0IsR0FBRyxJQUFJLFlBQVksRUFBVSxDQUFDO1FBQ2xELG1CQUFjLEdBQUcsSUFBSSxZQUFZLEVBQXVDLENBQUM7UUFpQ3pFLHVCQUFrQixHQUFHLElBQUksWUFBWSxFQUFtQyxDQUFDO1FBQ3pFLHNCQUFpQixHQUFHLElBQUksWUFBWSxFQUFVLENBQUM7UUFJL0Msd0JBQW1CLEdBQUcsSUFBSSxZQUFZLEVBQWlCLENBQUM7UUFDekQsNkJBQXdCLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQzs7UUFpUTNDLGNBQVM7OztRQUFlLEdBQUcsRUFBRTtZQUNsQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUM1QyxDQUFDLEVBQUM7SUFyUEUsQ0FBQzs7Ozs7SUF2RUwsSUFDSSxrQkFBa0IsQ0FBQyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUNQLENBQUMsQ0FBQyxPQUFPOzs7O1lBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQ2QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQztZQUNyRCxDQUFDLEVBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7Ozs7SUFHRCxJQUFhLFdBQVcsQ0FBQyxDQUFDO1FBQ3hCLENBQUMsQ0FBQyxTQUFTOzs7O1FBQUMsR0FBRyxDQUFDLEVBQUU7WUFDaEIsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFO2dCQUNULElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxtQkFBQSxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBYSxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQzthQUNwRztRQUNILENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFHRCxJQUNJLHdCQUF3QixDQUFDLENBQUM7UUFDNUIsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxVQUFVLEtBQUssY0FBYyxFQUFFO1lBQy9DLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFBO1NBQ3hHO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxJQUFhLFVBQVUsQ0FBQyxDQUFvQjtRQUMxQyxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztJQUN2QixDQUFDOzs7OztJQUNELElBQWEsa0JBQWtCLENBQUMsQ0FBb0I7UUFDbEQsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTs7Z0JBQzdCLElBQUksR0FBRyxDQUFDLG1CQUFBLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFhLENBQUM7WUFDL0QsQ0FBQyxDQUFDLE9BQU87Ozs7WUFBQyxHQUFHLENBQUMsRUFBRTtnQkFDZCxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUMvQixJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2lCQUN4RDtZQUNILENBQUMsRUFBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDOzs7OztJQUdELElBQ1csU0FBUyxDQUFDLENBQUM7UUFDcEI7WUFDRSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3ZCLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO2dCQUNwQixJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUE7YUFDNUI7U0FDRjtJQUNILENBQUM7Ozs7O0lBSUQsSUFBb0Isb0JBQW9CLENBQUMsRUFBRTtRQUN6QyxJQUFJLENBQUMscUJBQXFCLHFCQUFRLEVBQUUsQ0FBRSxDQUFDO0lBQ3pDLENBQUM7Ozs7O0lBTUQsSUFBb0IsZUFBZSxDQUFDLEVBQW9CO1FBQ3RELElBQUksRUFBRSxFQUFFO1lBQ04sSUFBSSxDQUFDLGdCQUFnQixHQUFHLEVBQUUsQ0FBQztZQUMzQixJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFBO1NBQ2xCO0lBQ0gsQ0FBQzs7OztJQU9ELFFBQVE7UUFDTixJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLCtCQUErQixFQUFFLENBQUM7UUFDdkMsSUFBSSxDQUFDLHdCQUF3QixFQUFFLENBQUM7UUFDaEMsSUFBSSxDQUFDLDhCQUE4QixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNsRCxrQ0FBa0M7SUFDcEMsQ0FBQzs7Ozs7SUFDRCxXQUFXLENBQUMsT0FBc0I7UUFDaEMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxZQUFZLEVBQUU7WUFDN0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxPQUFPLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQztZQUNuRCxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1NBQzFDO1FBQ0QsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU8sSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUU7WUFDdkQsSUFBSSxDQUFDLDhCQUE4QixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUNuRDtJQUNILENBQUM7Ozs7O0lBQ0QsOEJBQThCLENBQUMsT0FBdUI7UUFDcEQsSUFBSSxPQUFPLEVBQUU7WUFDWCxPQUFPLENBQUMsYUFBYTs7O1lBQUcsR0FBRyxFQUFFO2dCQUMzQixJQUFJLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztZQUM1QyxDQUFDLENBQUEsQ0FBQztTQUNIO0lBQ0gsQ0FBQzs7Ozs7O0lBRU8saUJBQWlCLENBQUMsQ0FBQztRQUN6QixJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLGlCQUFpQixFQUFFO1lBQ2pELElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUU7Z0JBQzNCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQzthQUN4Qjs7Z0JBQ0csSUFBSSxHQUFHLENBQUMsbUJBQUEsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQWEsQ0FBQzs7Z0JBQzNELFFBQVE7WUFDWixDQUFDLENBQUMsT0FBTzs7OztZQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUNkLElBQUksR0FBRyxDQUFDLFVBQVUsRUFBRTtvQkFDbEIsS0FBSyxNQUFNLEdBQUcsSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO3dCQUM1QixJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFOztrQ0FDNUIsUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDOzRCQUNoQyxJQUFJLFFBQVEsQ0FBQyxZQUFZLENBQUMsS0FBSyxHQUFHLENBQUMsVUFBVSxFQUFFO2dDQUM3QyxRQUFRLEdBQUcsR0FBRyxDQUFDOzZCQUNoQjt5QkFDRjtxQkFDRjtpQkFDRjtnQkFDRCxRQUFRLEdBQUcsQ0FBQyxDQUFDLFFBQVEsSUFBSSxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQztnQkFDakUsUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUN2RyxDQUFDLEVBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQzs7Ozs7O0lBQ08sbUJBQW1CLENBQUMsU0FBUztRQUNuQyxTQUFTLENBQUMsT0FBTzs7Ozs7UUFBQyxDQUFDLE9BQU8sRUFBRSxLQUFLLEVBQUUsRUFBRTtZQUNuQyxJQUFJLEtBQUssS0FBSyxDQUFDLEVBQUU7Z0JBQ2YsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxRQUFRLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pFLE9BQU87YUFDUjtZQUNELElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDdkIsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7Ozs7SUFFTywrQkFBK0I7UUFDckMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLHFCQUFxQixDQUFDLFNBQVM7Ozs7UUFBQyxDQUFDLGVBQWUsRUFBRSxFQUFFO1lBQzVFLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtnQkFDakIsT0FBTzthQUNSOztnQkFDRyxJQUFJLEdBQUcsQ0FBQyxtQkFBQSxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBYSxDQUFDOztnQkFDM0QsWUFBWSxHQUFHLEVBQUU7O2tCQUNmLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ3RDLElBQUksZUFBZSxDQUFDLFdBQVcsRUFBRSxLQUFLLEtBQUssRUFBRTs7b0JBQ3ZDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTTs7OztnQkFBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsRUFBQztnQkFDbkYsZ0JBQWdCLENBQUMsT0FBTzs7OztnQkFBQyxDQUFDLE1BQU0sRUFBRSxFQUFFO29CQUNsQyxZQUFZLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUE7b0JBQzVFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLEVBQUUsTUFBTSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUNuRixDQUFDLEVBQUMsQ0FBQztnQkFDSCxPQUFPO2FBQ1I7WUFDRCxZQUFZLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxlQUFlLENBQUMsQ0FBQTtZQUNyRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFLGVBQWUsQ0FBQyxDQUFDO1FBQzVFLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7OztJQUNPLG1CQUFtQixDQUFDLFdBQTBCLEVBQUUsV0FBbUI7O1lBQ3JFLFlBQVksR0FBRyxFQUFFO1FBQ3JCLEtBQUssTUFBTSxHQUFHLElBQUksV0FBVyxFQUFFO1lBQzdCLElBQUksV0FBVyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTs7c0JBQzdCLFFBQVEsR0FBRyxXQUFXLENBQUMsR0FBRyxDQUFDO2dCQUNqQyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFBO2FBQ3pDO1NBQ0Y7UUFDRCxPQUFPLFlBQVksQ0FBQztJQUN0QixDQUFDOzs7OztJQUNPLHdCQUF3Qjs7Y0FDeEIsY0FBYyxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxTQUFTOzs7O1FBQUMsTUFBTSxDQUFDLEVBQUU7WUFDbEYsSUFBSSxNQUFNLEVBQUU7Z0JBQ1YsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGdCQUFnQixFQUFFLENBQUM7Z0JBQzFDLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sS0FBSyxPQUFPLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sS0FBSyxVQUFVLEVBQUU7b0JBQzdGLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxlQUFlLENBQUM7d0JBQ3hDLElBQUksRUFBRSxDQUFDLG1CQUFBLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFhLENBQUMsQ0FBQyxXQUFXLEVBQUU7d0JBQ3hFLEtBQUssRUFBRSxJQUFJO3FCQUNaLENBQUMsQ0FBQztvQkFDSCxPQUFPO2lCQUNSO2dCQUNELElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxlQUFlLENBQUM7b0JBQ3hDLElBQUksRUFBRSxFQUFFO29CQUNSLEtBQUssRUFBRSxLQUFLO2lCQUNiLENBQUMsQ0FBQzthQUNKO1FBQ0gsQ0FBQyxFQUFDO1FBQ0YsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDeEMsQ0FBQzs7OztJQUNELGVBQWU7UUFDYixJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUMxQixPQUFPO1NBQ1I7UUFDRCxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDcEIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO2dCQUNyQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFdBQVc7YUFDeEIsQ0FBQyxDQUFDO1NBQ0o7YUFBTTtZQUNMLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztnQkFDckMsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQzthQUN6QixDQUFDLENBQUM7U0FDSjtRQUNELElBQ0UsQ0FBQyxJQUFJLENBQUMsY0FBYztlQUNqQixDQUFDLG1CQUFBLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFhLENBQUMsQ0FBQyxNQUFNLEtBQUssQ0FBQztlQUNqRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsRUFDL0Q7WUFDQSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7U0FDZjtRQUNELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ25FLENBQUM7Ozs7O0lBQ0Qsb0JBQW9CLENBQUMsSUFBYTs7Y0FDMUIsRUFBRSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQztRQUNwRCxJQUFJLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQzVCLEVBQUUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDckI7UUFDRCxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakIsRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ2IsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7U0FDM0U7UUFDRCxFQUFFLENBQUMsVUFBVSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFBO1FBQ3ZELElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTzs7OztRQUFDLENBQUMsR0FBZ0IsRUFBRSxFQUFFO1lBQzVDLG1CQUFtQjtZQUNuQixJQUFJLEdBQUcsQ0FBQyxVQUFVLEtBQUssY0FBYyxJQUFJLEdBQUcsQ0FBQyxrQkFBa0IsRUFBRTtnQkFDL0QsRUFBRSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUMsWUFBWSxDQUFDLFNBQVM7Ozs7Z0JBQUMsV0FBVyxDQUFDLEVBQUU7b0JBQ3BFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLEVBQUUsV0FBVyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsUUFBUSxFQUFFLEVBQUUsQ0FBQyxXQUFXLEVBQUUsRUFBRSxDQUFDLENBQUM7b0JBQzVGLElBQUksQ0FBQyx1QkFBdUIscUJBQVEsR0FBRyxDQUFFLENBQUM7Z0JBQzVDLENBQUMsRUFBQyxDQUFDO2FBQ0o7UUFDSCxDQUFDLEVBQUMsQ0FBQztRQUNILE9BQU8sRUFBRSxDQUFDO0lBQ1osQ0FBQzs7Ozs7Ozs7SUFDTyxPQUFPLENBQUMsS0FBc0IsRUFBRSxPQUErQixFQUFFLEdBQUc7O1lBQ3RFLFdBQVc7UUFDZixJQUFJLENBQUMsQ0FBQyxLQUFLLElBQUksS0FBSyxLQUFLLEVBQUUsRUFBRTtZQUMzQixPQUFPLE9BQU8sQ0FBQztTQUNoQjtRQUNELElBQUksT0FBTyxLQUFLLEtBQUssUUFBUSxFQUFFO1lBQzdCLFdBQVcsR0FBRyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDbkM7UUFDRCxJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsRUFBRTtZQUM3QixXQUFXLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3hDO1FBQ0QsT0FBTyxPQUFPLENBQUMsTUFBTTs7OztRQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQUMsQ0FBQztJQUN4RixDQUFDOzs7O0lBRUQsVUFBVTtJQUNWLENBQUM7Ozs7O0lBQ0QsTUFBTSxDQUFDLEdBQVk7UUFDakIsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ3BCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7WUFDekQsT0FBTztTQUNSOztZQUNHLGlCQUFpQixHQUFHLG1CQUFBLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFhO1FBQzFFLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztJQUN6RCxDQUFDOzs7OztJQUNELFVBQVUsQ0FBQyxHQUFJOztZQUNULGlCQUFpQixHQUFHLG1CQUFBLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFhO1FBQzFFLGlCQUFpQixDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzFCLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztJQUN6RCxDQUFDOzs7O0lBQ0QsU0FBUzs7WUFDSCxpQkFBaUIsR0FBRyxtQkFBQSxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBYTs7WUFDdEUsZ0JBQWdCLEdBQUcsRUFBRTs7WUFDckIsV0FBVyxHQUFHLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxNQUFNOzs7O1FBQUMsQ0FBQyxTQUFvQixFQUFFLEVBQUU7WUFDM0UsSUFBSSxTQUFTLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRTtnQkFDOUIsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO2FBQ2hEO1lBQ0QsT0FBTyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFBO1FBQ3BDLENBQUMsRUFBQztRQUNGLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUNoRCxpQkFBaUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQTtRQUN6QixXQUFXLENBQUMsT0FBTzs7OztRQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFDLENBQUE7SUFDdkQsQ0FBQzs7Ozs7O0lBRU8sNkJBQTZCLENBQUMsTUFBbUI7O2NBQ2pELEdBQUcsR0FBRyxNQUFNLENBQUMsUUFBUSxJQUFJLE1BQU0sQ0FBQyxVQUFVOztjQUMxQyxXQUFXOzs7O1FBQUcsQ0FBQyxJQUFJLEVBQXNCLEVBQUU7WUFDL0MsT0FBTyxDQUFDLElBQUksSUFBSSxPQUFPLElBQUksS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDL0QsQ0FBQyxDQUFBO1FBQ0QsMEJBQTBCOztRQUExQiwwQkFBMEI7UUFDMUIsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNoRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDMUMsQ0FBQzs7Ozs7O0lBQ0QsbUJBQW1CLENBQUMsTUFBbUIsRUFBRSxTQUFvQjtRQUMzRCxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsR0FBRyxFQUFFLENBQUM7UUFDbEQsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzNDLElBQUksU0FBUyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUMsS0FBSyxFQUFFO1lBQ3BELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLEVBQUUsU0FBUyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLFNBQVMsQ0FBQyxXQUFXLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDM0ksSUFBSSxDQUFDLHVCQUF1QixxQkFBUSxNQUFNLENBQUUsQ0FBQztTQUM5QztJQUNILENBQUM7Ozs7Ozs7O0lBQ0QsNkJBQTZCLENBQUMsS0FBbUMsRUFBRSxRQUFRLEVBQUUsWUFBdUIsRUFBRSxNQUFtQjs7WUFDbkgsTUFBTSxHQUFHLENBQUMsS0FBSyxJQUFJLE9BQU8sS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxtQkFBTSxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRyxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLElBQUksRUFBRTtRQUNySCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksaUJBQUcsTUFBTSxJQUFLLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxZQUFZLENBQUMsV0FBVyxFQUFFLEVBQUUsTUFBTSxFQUFFLEVBQUcsQ0FBQztJQUN0RyxDQUFDOzs7OztJQUNELGdCQUFnQixDQUFDLEtBQUs7SUFFdEIsQ0FBQzs7Ozs7OztJQUNELDBCQUEwQixDQUFDLEtBQUssRUFBRSxZQUF1QixFQUFFLGVBQWU7UUFDeEUsWUFBWSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7SUFFM0QsQ0FBQzs7Ozs7O0lBQ08sUUFBUSxDQUFDLEVBQXFCOztjQUM5QixTQUFTLEdBQUcsQ0FBRSxtQkFBQSxJQUFJLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxFQUFhLENBQUM7O2NBQy9ELFNBQVMsR0FBRyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsU0FBUzs7OztRQUFDLENBQUMsU0FBUyxFQUFDLEVBQUU7WUFDL0QsSUFBSSxTQUFTLENBQUMsVUFBVSxLQUFLLEVBQUUsQ0FBQyxVQUFVLEVBQUU7Z0JBQzFDLE9BQU8sSUFBSSxDQUFDO2FBQ2I7UUFDSCxDQUFDLEVBQUM7O2NBQ0ksVUFBVSxHQUFHLFNBQVMsQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDO1FBQzFDLElBQUksVUFBVSxFQUFFO1lBQ2QsSUFBSSxFQUFFLENBQUMsUUFBUSxFQUFFO2dCQUNmLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLGVBQWUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbkQsT0FBTzthQUNSO1lBQ0QsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsZUFBZSxDQUFDLENBQUMsU0FBUyxpQkFDMUMsR0FBRyxFQUFDLEVBQUUsQ0FBQyxHQUFHLElBQ1AsRUFBRSxDQUFDLElBQUksRUFDVixDQUFDO1NBQ0o7SUFDSCxDQUFDOzs7OztJQU1ELFVBQVUsQ0FBQyxHQUFRO1FBQ2pCLElBQUksR0FBRyxJQUFJLEdBQUcsQ0FBQyxTQUFTLEVBQUU7WUFDeEIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssRUFBRSxDQUFDO1lBQy9CLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUNsQixPQUFPO1NBQ1I7UUFDRCxHQUFHLElBQUksR0FBRyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3pELENBQUM7Ozs7O0lBQ0QsZ0JBQWdCLENBQUMsRUFBTztRQUN0QixFQUFFLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLElBQUksQ0FDM0QsR0FBRzs7OztRQUFDLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFDLG1CQUFBLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFhLENBQUMsQ0FBQyxXQUFXLEVBQUUsRUFBQyxDQUNuRixDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUNsQixDQUFDOzs7OztJQUNELGlCQUFpQixDQUFDLEVBQU87UUFDdkIsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUM5QixDQUFDOzs7OztJQUNELGdCQUFnQixDQUFFLFVBQW1CO1FBQ25DLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQzlHLDhDQUE4QztJQUNoRCxDQUFDOzs7OztJQUNELFFBQVEsQ0FBQyxPQUF3QjtRQUMvQixPQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLFdBQVcsRUFBRSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLHNDQUFzQyxFQUFFLEVBQUUsQ0FBQztJQUNqSixDQUFDOzs7OztJQUdELFdBQVc7UUFDVCxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ2xDLENBQUM7OztZQW5YRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGdCQUFnQjtnQkFDMUIsdzVNQUE4QztnQkFFOUMsU0FBUyxFQUFFO29CQUNUO3dCQUNFLE9BQU8sRUFBRSxpQkFBaUI7d0JBQzFCLFdBQVcsRUFBRSxVQUFVOzs7d0JBQUMsR0FBRyxFQUFFLENBQUMsc0JBQXNCLEVBQUM7d0JBQ3JELEtBQUssRUFBRSxJQUFJO3FCQUNaO29CQUNEO3dCQUNFLE9BQU8sRUFBRSxhQUFhO3dCQUN0QixXQUFXLEVBQUUsVUFBVTs7O3dCQUFDLEdBQUcsRUFBRSxDQUFDLHNCQUFzQixFQUFDO3dCQUNyRCxLQUFLLEVBQUUsSUFBSTtxQkFDWjtpQkFDRjs7YUFDRjs7OztZQXpCK0IsV0FBVztZQU1sQyxvQkFBb0I7WUFQNEYsaUJBQWlCOzs7dUJBa0N2SSxLQUFLOzRCQUNMLEtBQUs7NkJBQ0wsS0FBSztpQ0FDTCxLQUFLOzBCQVFMLEtBQUs7MEJBRUwsS0FBSzttQ0FPTCxNQUFNOzZCQUNOLE1BQU07dUNBQ04sS0FBSzt5QkFPTCxLQUFLO2lDQUdMLEtBQUs7d0JBWUwsS0FBSyxTQUFDLFdBQVc7aUNBVWpCLE1BQU07Z0NBQ04sTUFBTTttQ0FDTixLQUFLO2tDQUdMLE1BQU07dUNBQ04sS0FBSzswQkFDTCxLQUFLO3NCQUNMLEtBQUs7OEJBRUwsS0FBSzs7OztJQXRFTixtREFBNEI7O0lBQzVCLHdDQUFnQjs7SUFDaEIsd0NBQWdCOztJQUNoQixpREFBcUI7O0lBQ3JCLCtDQUFtQjs7SUFDbkIsOENBQWtDOztJQUNsQywwQ0FBMEI7O0lBQzFCLCtDQUErQjs7SUFDL0IsZ0RBQXlDOztJQVN6Qyw2Q0FDdUM7O0lBUXZDLHNEQUE0RDs7SUFDNUQsZ0RBQW1GOzs7OztJQU9uRiw2Q0FBdUM7Ozs7O0lBY3ZDLHlEQUE2Qzs7Ozs7SUFDN0MsNENBQW1COzs7OztJQVVuQix1REFBOEI7O0lBQzlCLG9EQUFtRjs7SUFDbkYsbURBQXlEOztJQUl6RCxxREFBa0U7O0lBQ2xFLDBEQUFrRDs7SUFDbEQsNkNBQWdDOztJQUNoQyx5Q0FBOEI7Ozs7O0lBQzlCLGtEQUEyQzs7SUE4UDNDLDJDQUVFOzs7OztJQXhQQSxvQ0FBdUI7Ozs7O0lBQ3ZCLHNEQUFrRDs7Ozs7SUFDbEQsb0RBQTZDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBBZnRlclZpZXdJbml0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgT25DaGFuZ2VzLCBTaW1wbGVDaGFuZ2VzLCBmb3J3YXJkUmVmLCBPbkRlc3Ryb3ksIENoYW5nZURldGVjdG9yUmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3JtQXJyYXksIFZhbGlkYXRvcnMsIEZvcm1CdWlsZGVyLCBGb3JtR3JvdXAsIENvbnRyb2xWYWx1ZUFjY2Vzc29yLCBWYWxpZGF0b3IsIE5HX1ZBTFVFX0FDQ0VTU09SLCBOR19WQUxJREFUT1JTLCBBYnN0cmFjdENvbnRyb2wsIFZhbGlkYXRpb25FcnJvcnMsIEZvcm1Db250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgb2YsIE9ic2VydmFibGUsIFN1YmplY3QsIFN1YnNjcmlwdGlvbiwgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBtYXAsIHN0YXJ0V2l0aCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IFRhYmxlQ29uZmlnLCBQYXRjaFZhbHVlLCBFdFRhYmxlVmFsdWVDaGFuZ2VFdmVudCwgRXRBdXRvY29tcGxldGVTZWxlY3RlZEV2ZW50LCBFVFZhbGlkYXRpb25FcnJvciB9IGZyb20gJy4vZWRpdGFibGUtdGFibGUtY29uZmlnJztcbmltcG9ydCB7IE1hdEF1dG9jb21wbGV0ZVNlbGVjdGVkRXZlbnQgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9hdXRvY29tcGxldGUnO1xuaW1wb3J0ICogYXMgdXVpZCBmcm9tICd1dWlkJztcbmltcG9ydCB7IEV0RGF0YUhhbmRsZXJTZXJ2aWNlIH0gZnJvbSAnLi9ldC1kYXRhLWhhbmRsZXIuc2VydmljZSc7XG5cblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZWRpdGFibGUtdGFibGUnLFxuICB0ZW1wbGF0ZVVybDogJy4vZWRpdGFibGUtdGFibGUuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9lZGl0YWJsZS10YWJsZS5jb21wb25lbnQuc2NzcyddLFxuICBwcm92aWRlcnM6IFtcbiAgICB7XG4gICAgICBwcm92aWRlOiBOR19WQUxVRV9BQ0NFU1NPUixcbiAgICAgIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IEVkaXRhYmxlVGFibGVDb21wb25lbnQpLFxuICAgICAgbXVsdGk6IHRydWVcbiAgICB9LFxuICAgIHtcbiAgICAgIHByb3ZpZGU6IE5HX1ZBTElEQVRPUlMsXG4gICAgICB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBFZGl0YWJsZVRhYmxlQ29tcG9uZW50KSxcbiAgICAgIG11bHRpOiB0cnVlXG4gICAgfVxuICBdXG59KVxuZXhwb3J0IGNsYXNzIEVkaXRhYmxlVGFibGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSwgT25DaGFuZ2VzLCBDb250cm9sVmFsdWVBY2Nlc3NvciwgVmFsaWRhdG9yIHtcbiAgZWRpdGFibGVUYWJsZUZvcm06IEZvcm1Hcm91cFxuICBpc0VkaXQ6IGJvb2xlYW47XG4gIGlzVmlldzogYm9vbGVhbjtcbiAgZmlsdGVyZWRPcHRpb25zID0ge307XG4gIHNlbGVjdE9wdGlvbnMgPSB7fTtcbiAgc3Vic2NyaXB0aW9uID0gbmV3IFN1YnNjcmlwdGlvbigpO1xuICBASW5wdXQoKSB2aWV3T25seSA9IGZhbHNlO1xuICBASW5wdXQoKSBoaWRlQWN0aW9uQnRuID0gZmFsc2U7XG4gIEBJbnB1dCgpIGhpZGVEZWZhdWx0Um93OiBib29sZWFuID0gZmFsc2U7XG4gIEBJbnB1dCgpXG4gIHNldCBhc3NpZ25MaXN0VG9TZWxlY3Qodikge1xuICAgIGlmICghIXYpIHtcbiAgICAgIHYuZm9yRWFjaChlbGUgPT4ge1xuICAgICAgICB0aGlzLnNlbGVjdE9wdGlvbnNbZWxlLmZvcm1Db250cm9sTmFtZV0gPSBlbGUubGlzdDtcbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuICBASW5wdXQoKVxuICB0YWJsZUNvbmZpZyA9IFtdIGFzIEFycmF5PFRhYmxlQ29uZmlnPjtcbiAgQElucHV0KCkgc2V0IGdldEZvcm1EYXRhKHYpIHtcbiAgICB2LnN1YnNjcmliZShyZXMgPT4ge1xuICAgICAgaWYgKCEhcmVzKSB7XG4gICAgICAgIHRoaXMuZ2V0VGFibGVSZWNvcmRDaGFuZ2UuZW1pdCgodGhpcy5lZGl0YWJsZVRhYmxlRm9ybS5jb250cm9scy50YWJsZSBhcyBGb3JtQXJyYXkpLmdldFJhd1ZhbHVlKCkpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG4gIEBPdXRwdXQoKSBnZXRUYWJsZVJlY29yZENoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8b2JqZWN0PigpO1xuICBAT3V0cHV0KCkgb3B0aW9uU2VsZWN0ZWQgPSBuZXcgRXZlbnRFbWl0dGVyPEV0QXV0b2NvbXBsZXRlU2VsZWN0ZWRFdmVudDxvYmplY3Q+PigpO1xuICBASW5wdXQoKVxuICBzZXQgYXNzaWduTGlzdFRvQXV0b2NvbXBsZXRlKHYpIHtcbiAgICBpZiAodiAmJiB2LmNvbmZpZy5pbnB1dEZpZWxkID09PSAnYXV0b2NvbXBsZXRlJykge1xuICAgICAgdGhpcy5maWx0ZXJlZE9wdGlvbnNbdi5jb25maWcuZm9ybUNvbnRyb2xOYW1lXSA9IHRoaXMuX2ZpbHRlcih2LnNlYXJjaEtleSwgdi5saXN0LCB2LmNvbmZpZy5kaXNwbGF5S2V5KVxuICAgIH1cbiAgfVxuICBwcml2YXRlIF9wYXRjaFZhbHVlOiBBcnJheTxQYXRjaFZhbHVlPjtcbiAgQElucHV0KCkgc2V0IHBhdGNoVmFsdWUodjogQXJyYXk8UGF0Y2hWYWx1ZT4pIHtcbiAgICB0aGlzLl9wYXRjaFZhbHVlID0gdjtcbiAgfVxuICBASW5wdXQoKSBzZXQgcGF0Y2hWYWx1ZVRvU2VsZWN0KHY6IEFycmF5PFBhdGNoVmFsdWU+KSB7XG4gICAgaWYgKCEhdiAmJiB0aGlzLmVkaXRhYmxlVGFibGVGb3JtKSB7XG4gICAgICBsZXQgZm9ybSA9ICh0aGlzLmVkaXRhYmxlVGFibGVGb3JtLmNvbnRyb2xzLnRhYmxlIGFzIEZvcm1BcnJheSlcbiAgICAgIHYuZm9yRWFjaChlbGUgPT4ge1xuICAgICAgICBpZiAoZm9ybS5jb250cm9sc1tlbGUucm93SW5kZXhdKSB7XG4gICAgICAgICAgZm9ybS5jb250cm9sc1tlbGUucm93SW5kZXhdLnBhdGNoVmFsdWUoZWxlLnBhdGNoVmFsdWUpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG4gIH1cbiAgcHJpdmF0ZSBkaXJ0eUF1dG9jb21wbGV0ZUNvbHVtbjogVGFibGVDb25maWc7XG4gIHByaXZhdGUgX3RhYmxlRGF0YTtcbiAgQElucHV0KCd0YWJsZURhdGEnKVxuICBwdWJsaWMgc2V0IHRhYmxlRGF0YSh2KSB7XG4gICAge1xuICAgICAgaWYgKCEhdiAmJiB2Lmxlbmd0aCA+IDApIHtcbiAgICAgICAgdGhpcy5fdGFibGVEYXRhID0gdjtcbiAgICAgICAgdGhpcy5jcmVhdGVUYWJsZVdpdGhEYXRhKHYpXG4gICAgICB9XG4gICAgfVxuICB9XG4gIHByaXZhdGUgX2Zvcm1Hcm91cEZvclRhYmxlUm93O1xuICBAT3V0cHV0KCkgc2VhcmNoVmFsdWVDaGFuZ2VzID0gbmV3IEV2ZW50RW1pdHRlcjxFdFRhYmxlVmFsdWVDaGFuZ2VFdmVudDxvYmplY3Q+PigpO1xuICBAT3V0cHV0KCkgdGFibGVWYWx1ZUNoYW5nZXMgPSBuZXcgRXZlbnRFbWl0dGVyPG9iamVjdD4oKTtcbiAgQElucHV0KCkgcHVibGljIHNldCBmb3JtR3JvdXBGb3JUYWJsZVJvdyhmYikge1xuICAgIHRoaXMuX2Zvcm1Hcm91cEZvclRhYmxlUm93ID0geyAuLi5mYiB9O1xuICB9XG4gIEBPdXRwdXQoKSBkZWxldGVkVGFibGVSZWNvcmRzID0gbmV3IEV2ZW50RW1pdHRlcjxBcnJheTxvYmplY3Q+PigpO1xuICBASW5wdXQoKSBtYXRBdXRvY29tcGxldGVEaXNwbGF5Rm4gPSBuZXcgU3ViamVjdCgpO1xuICBASW5wdXQoKSBldEZvcm1BcnJheTogRm9ybUFycmF5O1xuICBASW5wdXQoKSBjb250cm9sOiBGb3JtQ29udHJvbDtcbiAgcHJpdmF0ZSBfdmFsaWRhdGlvbkVycm9yOkVUVmFsaWRhdGlvbkVycm9yO1xuICBASW5wdXQoKSBwdWJsaWMgc2V0IHZhbGlkYXRpb25FcnJvcih2ZTpFVFZhbGlkYXRpb25FcnJvcil7XG4gICAgaWYgKHZlKSB7XG4gICAgICB0aGlzLl92YWxpZGF0aW9uRXJyb3IgPSB2ZTtcbiAgICAgIHRoaXMuc2V0RXJyb3IodmUpXG4gICAgfVxuICB9XG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgZmI6IEZvcm1CdWlsZGVyLFxuICAgIHByaXZhdGUgZXREYXRhSGFuZGxlclNlcnZpY2U6IEV0RGF0YUhhbmRsZXJTZXJ2aWNlLFxuICAgIHByaXZhdGUgX2NoYW5nZURldGVjdG9yUmVmOiBDaGFuZ2VEZXRlY3RvclJlZixcbiAgKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLmNyZWF0ZVRhYmxlRm9ybSgpO1xuICAgIHRoaXMuc3Vic2NyaWJlVG9TZW5kQ29sdW1uTmFtZVJlY29yZCgpO1xuICAgIHRoaXMuc3Vic2NyaWJlVG9WYWxpZGF0ZVRhYmxlKCk7XG4gICAgdGhpcy5hc3NpZ25NYXJrQXNUb3VjaGVkRm5Ub0NvbnRyb2wodGhpcy5jb250cm9sKTtcbiAgICAvLyB0aGlzLnNldFF1b3RhdGlvbkxpc3RUb1RhYmxlKCk7XG4gIH1cbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQge1xuICAgIGlmICghIWNoYW5nZXMucGF0Y2hWYWx1ZSAmJiAhIWNoYW5nZXMucGF0Y2hWYWx1ZS5jdXJyZW50VmFsdWUpIHtcbiAgICAgIHRoaXMuX3BhdGNoVmFsdWUgPSBjaGFuZ2VzLnBhdGNoVmFsdWUuY3VycmVudFZhbHVlO1xuICAgICAgdGhpcy5wYXRjaFZhbHVlVG9UYWJsZSh0aGlzLl9wYXRjaFZhbHVlKTtcbiAgICB9XG4gICAgaWYgKCEhY2hhbmdlcy5jb250cm9sICYmICEhY2hhbmdlcy5jb250cm9sLmN1cnJlbnRWYWx1ZSkge1xuICAgICAgdGhpcy5hc3NpZ25NYXJrQXNUb3VjaGVkRm5Ub0NvbnRyb2wodGhpcy5jb250cm9sKTtcbiAgICB9XG4gIH1cbiAgYXNzaWduTWFya0FzVG91Y2hlZEZuVG9Db250cm9sKGNvbnRyb2w6QWJzdHJhY3RDb250cm9sKXtcbiAgICBpZiAoY29udHJvbCkge1xuICAgICAgY29udHJvbC5tYXJrQXNUb3VjaGVkID0gKCkgPT4ge1xuICAgICAgICB0aGlzLmVkaXRhYmxlVGFibGVGb3JtLm1hcmtBbGxBc1RvdWNoZWQoKTtcbiAgICAgIH07XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBwYXRjaFZhbHVlVG9UYWJsZSh2KSB7XG4gICAgaWYgKCEhdiAmJiB2Lmxlbmd0aCA+IDAgJiYgdGhpcy5lZGl0YWJsZVRhYmxlRm9ybSkge1xuICAgICAgaWYgKCF0aGlzLmVkaXRhYmxlVGFibGVGb3JtKSB7XG4gICAgICAgIHRoaXMuY3JlYXRlVGFibGVGb3JtKCk7XG4gICAgICB9XG4gICAgICBsZXQgZm9ybSA9ICh0aGlzLmVkaXRhYmxlVGFibGVGb3JtLmNvbnRyb2xzLnRhYmxlIGFzIEZvcm1BcnJheSlcbiAgICAgIGxldCByb3dJbmRleDtcbiAgICAgIHYuZm9yRWFjaChlbGUgPT4ge1xuICAgICAgICBpZiAoZWxlLnRhYmxlUm93SWQpIHtcbiAgICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiBmb3JtLnZhbHVlKSB7XG4gICAgICAgICAgICBpZiAoZm9ybS52YWx1ZS5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG4gICAgICAgICAgICAgIGNvbnN0IHRhYmxlUm93ID0gZm9ybS52YWx1ZVtrZXldO1xuICAgICAgICAgICAgICBpZiAodGFibGVSb3dbJ3RhYmxlUm93SWQnXSA9PT0gZWxlLnRhYmxlUm93SWQpIHtcbiAgICAgICAgICAgICAgICByb3dJbmRleCA9IGtleTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByb3dJbmRleCA9ICghcm93SW5kZXggJiYgZWxlLnJvd0luZGV4KSA/IGVsZS5yb3dJbmRleCA6IHJvd0luZGV4O1xuICAgICAgICByb3dJbmRleCA/IGZvcm0uY29udHJvbHNbcm93SW5kZXhdLnBhdGNoVmFsdWUoZWxlLnBhdGNoVmFsdWUpIDogY29uc29sZS5lcnJvcigncGF0Y2ggcm93IHVuZGVmaW5lZCcpO1xuICAgICAgfSk7XG4gICAgfVxuICB9XG4gIHByaXZhdGUgY3JlYXRlVGFibGVXaXRoRGF0YSh0YWJsZURhdGEpIHtcbiAgICB0YWJsZURhdGEuZm9yRWFjaCgoZWxlbWVudCwgaW5kZXgpID0+IHtcbiAgICAgIGlmIChpbmRleCA9PT0gMCkge1xuICAgICAgICB0aGlzLnBhdGNoVmFsdWVUb1RhYmxlKFt7IHJvd0luZGV4OiAnMCcsIHBhdGNoVmFsdWU6IGVsZW1lbnQgfV0pO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICB0aGlzLmFkZFJvdyhlbGVtZW50KTtcbiAgICB9KTtcbiAgfVxuICAvLyBASW5wdXQoKSBwcml2YXRlIHRhYmxlRGF0YTtcbiAgcHJpdmF0ZSBzdWJzY3JpYmVUb1NlbmRDb2x1bW5OYW1lUmVjb3JkKCkge1xuICAgIHRoaXMuZXREYXRhSGFuZGxlclNlcnZpY2Uuc2VuZENvbHVtbk5hbWVSZWNvcmQkLnN1YnNjcmliZSgoZm9ybUNvbnRyb2xOYW1lKSA9PiB7XG4gICAgICBpZiAodGhpcy52aWV3T25seSkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG4gICAgICBsZXQgZm9ybSA9ICh0aGlzLmVkaXRhYmxlVGFibGVGb3JtLmNvbnRyb2xzLnRhYmxlIGFzIEZvcm1BcnJheSk7XG4gICAgICBsZXQgY29sdW1uVmFsdWVzID0gW107XG4gICAgICBjb25zdCB0YWJsZVJlY29yZCA9IGZvcm0uZ2V0UmF3VmFsdWUoKTtcbiAgICAgIGlmIChmb3JtQ29udHJvbE5hbWUudG9Mb3dlckNhc2UoKSA9PT0gJ2FsbCcpIHtcbiAgICAgICAgbGV0IHNvcnRlZENvbmZpZ0xpc3QgPSB0aGlzLnRhYmxlQ29uZmlnLmZpbHRlcihjb25maWcgPT4gY29uZmlnLmlzTmVlZFZhbHVlQ2hhbmdlcyk7XG4gICAgICAgIHNvcnRlZENvbmZpZ0xpc3QuZm9yRWFjaCgoY29uZmlnKSA9PiB7XG4gICAgICAgICAgY29sdW1uVmFsdWVzID0gdGhpcy5jb2xsZWN0Q29sdW1uUmVjb3JkKHRhYmxlUmVjb3JkLCBjb25maWcuZm9ybUNvbnRyb2xOYW1lKVxuICAgICAgICAgIHRoaXMuZXREYXRhSGFuZGxlclNlcnZpY2UuZW1pdENvbHVtblJlY29yZChjb2x1bW5WYWx1ZXMsIGNvbmZpZy5mb3JtQ29udHJvbE5hbWUpO1xuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgY29sdW1uVmFsdWVzID0gdGhpcy5jb2xsZWN0Q29sdW1uUmVjb3JkKHRhYmxlUmVjb3JkLCBmb3JtQ29udHJvbE5hbWUpXG4gICAgICB0aGlzLmV0RGF0YUhhbmRsZXJTZXJ2aWNlLmVtaXRDb2x1bW5SZWNvcmQoY29sdW1uVmFsdWVzLCBmb3JtQ29udHJvbE5hbWUpO1xuICAgIH0pO1xuICB9XG4gIHByaXZhdGUgY29sbGVjdENvbHVtblJlY29yZCh0YWJsZVJlY29yZDogQXJyYXk8b2JqZWN0PiwgdGFibGVSb3dLZXk6IHN0cmluZykge1xuICAgIGxldCBjb2x1bW5WYWx1ZXMgPSBbXVxuICAgIGZvciAoY29uc3Qga2V5IGluIHRhYmxlUmVjb3JkKSB7XG4gICAgICBpZiAodGFibGVSZWNvcmQuaGFzT3duUHJvcGVydHkoa2V5KSkge1xuICAgICAgICBjb25zdCB0YWJsZVJvdyA9IHRhYmxlUmVjb3JkW2tleV07XG4gICAgICAgIGNvbHVtblZhbHVlcy5wdXNoKHRhYmxlUm93W3RhYmxlUm93S2V5XSlcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGNvbHVtblZhbHVlcztcbiAgfVxuICBwcml2YXRlIHN1YnNjcmliZVRvVmFsaWRhdGVUYWJsZSgpIHtcbiAgICBjb25zdCB2YWxpZGF0ZVNic2NyYiA9IHRoaXMuZXREYXRhSGFuZGxlclNlcnZpY2UudmFsaWRhdGVUYWJsZSgpLnN1YnNjcmliZShzdGF0dXMgPT4ge1xuICAgICAgaWYgKHN0YXR1cykge1xuICAgICAgICB0aGlzLmVkaXRhYmxlVGFibGVGb3JtLm1hcmtBbGxBc1RvdWNoZWQoKTtcbiAgICAgICAgaWYgKHRoaXMuZWRpdGFibGVUYWJsZUZvcm0uc3RhdHVzID09PSAnVkFMSUQnIHx8IHRoaXMuZWRpdGFibGVUYWJsZUZvcm0uc3RhdHVzID09PSAnRElTQUJMRUQnKSB7XG4gICAgICAgICAgdGhpcy5ldERhdGFIYW5kbGVyU2VydmljZS5zZW5kVGFibGVSZWNvcmQoe1xuICAgICAgICAgICAgZGF0YTogKHRoaXMuZWRpdGFibGVUYWJsZUZvcm0uY29udHJvbHMudGFibGUgYXMgRm9ybUFycmF5KS5nZXRSYXdWYWx1ZSgpLFxuICAgICAgICAgICAgdmFsaWQ6IHRydWVcbiAgICAgICAgICB9KTtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5ldERhdGFIYW5kbGVyU2VydmljZS5zZW5kVGFibGVSZWNvcmQoe1xuICAgICAgICAgIGRhdGE6IFtdLFxuICAgICAgICAgIHZhbGlkOiBmYWxzZVxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICB0aGlzLnN1YnNjcmlwdGlvbi5hZGQodmFsaWRhdGVTYnNjcmIpO1xuICB9XG4gIGNyZWF0ZVRhYmxlRm9ybSgpIHtcbiAgICBpZiAodGhpcy5lZGl0YWJsZVRhYmxlRm9ybSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBpZiAodGhpcy5ldEZvcm1BcnJheSkge1xuICAgICAgdGhpcy5lZGl0YWJsZVRhYmxlRm9ybSA9IHRoaXMuZmIuZ3JvdXAoe1xuICAgICAgICB0YWJsZTogdGhpcy5ldEZvcm1BcnJheVxuICAgICAgfSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuZWRpdGFibGVUYWJsZUZvcm0gPSB0aGlzLmZiLmdyb3VwKHtcbiAgICAgICAgdGFibGU6IHRoaXMuZmIuYXJyYXkoW10pXG4gICAgICB9KTtcbiAgICB9XG4gICAgaWYgKFxuICAgICAgIXRoaXMuaGlkZURlZmF1bHRSb3dcbiAgICAgICYmICh0aGlzLmVkaXRhYmxlVGFibGVGb3JtLmNvbnRyb2xzLnRhYmxlIGFzIEZvcm1BcnJheSkubGVuZ3RoID09PSAwXG4gICAgICAmJiAodGhpcy50YWJsZUNvbmZpZy5sZW5ndGggPiAwIHx8IHRoaXMuZXRGb3JtQXJyYXkubGVuZ3RoID4gMClcbiAgICApIHtcbiAgICAgIHRoaXMuYWRkUm93KCk7XG4gICAgfVxuICAgIHRoaXMudGFibGVWYWx1ZUNoYW5nZXMuZW1pdCh0aGlzLmVkaXRhYmxlVGFibGVGb3JtLnZhbHVlQ2hhbmdlcyk7XG4gIH1cbiAgaW1wbGVtZW50c0RldGFpbHNSb3coZGF0YT86IG9iamVjdCkge1xuICAgIGNvbnN0IGZnID0gdGhpcy5mYi5ncm91cCh0aGlzLl9mb3JtR3JvdXBGb3JUYWJsZVJvdyk7XG4gICAgaWYgKCEhZGF0YSAmJiAhdGhpcy52aWV3T25seSkge1xuICAgICAgZmcucGF0Y2hWYWx1ZShkYXRhKTtcbiAgICB9XG4gICAgaWYgKHRoaXMudmlld09ubHkpIHtcbiAgICAgIGZnLmRpc2FibGUoKTtcbiAgICAgICEhZGF0YSA/IGZnLnBhdGNoVmFsdWUoZGF0YSwgeyBvbmx5U2VsZjogdHJ1ZSwgZW1pdEV2ZW50OiBmYWxzZSB9KSA6IG51bGw7XG4gICAgfVxuICAgIGZnLmFkZENvbnRyb2woJ3RhYmxlUm93SWQnLCB0aGlzLmZiLmNvbnRyb2wodXVpZC52NCgpKSlcbiAgICB0aGlzLnRhYmxlQ29uZmlnLmZvckVhY2goKGVsZTogVGFibGVDb25maWcpID0+IHtcbiAgICAgIC8vIGZnLnZhbHVlQ2hhIG5nZXNcbiAgICAgIGlmIChlbGUuaW5wdXRGaWVsZCA9PT0gJ2F1dG9jb21wbGV0ZScgfHwgZWxlLmlzTmVlZFZhbHVlQ2hhbmdlcykge1xuICAgICAgICBmZy5jb250cm9sc1tlbGUuZm9ybUNvbnRyb2xOYW1lXS52YWx1ZUNoYW5nZXMuc3Vic2NyaWJlKHNlYXJjaFZhbHVlID0+IHtcbiAgICAgICAgICB0aGlzLnNlYXJjaFZhbHVlQ2hhbmdlcy5lbWl0KHsga2V5OiBzZWFyY2hWYWx1ZSwgY29uZmlnOiBlbGUsIHRhYmxlUm93OiBmZy5nZXRSYXdWYWx1ZSgpIH0pO1xuICAgICAgICAgIHRoaXMuZGlydHlBdXRvY29tcGxldGVDb2x1bW4gPSB7IC4uLmVsZSB9O1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICByZXR1cm4gZmc7XG4gIH1cbiAgcHJpdmF0ZSBfZmlsdGVyKHZhbHVlOiBzdHJpbmcgfCBvYmplY3QsIG9wdGlvbnM6IEFycmF5PE9iamVjdCB8IHN0cmluZz4sIGtleSk6IEFycmF5PE9iamVjdCB8IHN0cmluZz4ge1xuICAgIGxldCBmaWx0ZXJWYWx1ZTtcbiAgICBpZiAoISF2YWx1ZSB8fCB2YWx1ZSA9PT0gXCJcIikge1xuICAgICAgcmV0dXJuIG9wdGlvbnM7XG4gICAgfVxuICAgIGlmICh0eXBlb2YgdmFsdWUgPT09ICdzdHJpbmcnKSB7XG4gICAgICBmaWx0ZXJWYWx1ZSA9IHZhbHVlLnRvTG93ZXJDYXNlKCk7XG4gICAgfVxuICAgIGlmICh0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnKSB7XG4gICAgICBmaWx0ZXJWYWx1ZSA9IHZhbHVlW2tleV0udG9Mb3dlckNhc2UoKTtcbiAgICB9XG4gICAgcmV0dXJuIG9wdGlvbnMuZmlsdGVyKG9wdGlvbiA9PiBvcHRpb25ba2V5XS50b0xvd2VyQ2FzZSgpLmluZGV4T2YoZmlsdGVyVmFsdWUpID09PSAwKTtcbiAgfVxuXG4gIHN1Ym1pdERhdGEoKSB7XG4gIH1cbiAgYWRkUm93KHJvdz86IG9iamVjdCkge1xuICAgIGlmICh0aGlzLmV0Rm9ybUFycmF5KSB7XG4gICAgICB0aGlzLmVkaXRhYmxlVGFibGVGb3JtLmNvbnRyb2xzLnRhYmxlID0gdGhpcy5ldEZvcm1BcnJheTtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgbGV0IGltcGxlbWVudHNEZXRhaWxzID0gdGhpcy5lZGl0YWJsZVRhYmxlRm9ybS5jb250cm9scy50YWJsZSBhcyBGb3JtQXJyYXlcbiAgICBpbXBsZW1lbnRzRGV0YWlscy5wdXNoKHRoaXMuaW1wbGVtZW50c0RldGFpbHNSb3cocm93KSk7XG4gIH1cbiAgcmVzZXRUYWJsZShyb3c/KSB7XG4gICAgbGV0IGltcGxlbWVudHNEZXRhaWxzID0gdGhpcy5lZGl0YWJsZVRhYmxlRm9ybS5jb250cm9scy50YWJsZSBhcyBGb3JtQXJyYXlcbiAgICBpbXBsZW1lbnRzRGV0YWlscy5jbGVhcigpO1xuICAgIGltcGxlbWVudHNEZXRhaWxzLnB1c2godGhpcy5pbXBsZW1lbnRzRGV0YWlsc1Jvdyhyb3cpKTtcbiAgfVxuICBkZWxldGVSb3coKSB7XG4gICAgbGV0IGltcGxlbWVudHNEZXRhaWxzID0gdGhpcy5lZGl0YWJsZVRhYmxlRm9ybS5jb250cm9scy50YWJsZSBhcyBGb3JtQXJyYXk7XG4gICAgbGV0IGRlbGV0ZVJlY29yZExpc3QgPSBbXTtcbiAgICBsZXQgbm9uU2VsZWN0ZWQgPSBpbXBsZW1lbnRzRGV0YWlscy5jb250cm9scy5maWx0ZXIoKG1hY2hpbmVyeTogRm9ybUdyb3VwKSA9PiB7XG4gICAgICBpZiAobWFjaGluZXJ5LnZhbHVlLmlzU2VsZWN0ZWQpIHtcbiAgICAgICAgZGVsZXRlUmVjb3JkTGlzdC5wdXNoKG1hY2hpbmVyeS5nZXRSYXdWYWx1ZSgpKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiAhbWFjaGluZXJ5LnZhbHVlLmlzU2VsZWN0ZWRcbiAgICB9KTtcbiAgICB0aGlzLmRlbGV0ZWRUYWJsZVJlY29yZHMuZW1pdChkZWxldGVSZWNvcmRMaXN0KTtcbiAgICBpbXBsZW1lbnRzRGV0YWlscy5jbGVhcigpXG4gICAgbm9uU2VsZWN0ZWQuZm9yRWFjaChlbCA9PiBpbXBsZW1lbnRzRGV0YWlscy5wdXNoKGVsKSlcbiAgfVxuXG4gIHByaXZhdGUgZ2VuZXJhdGVBdXRvY29tcGxldGVEaXNwbGF5Rm4oY29uZmlnOiBUYWJsZUNvbmZpZykge1xuICAgIGNvbnN0IGtleSA9IGNvbmZpZy5wYXRjaEtleSB8fCBjb25maWcuZGlzcGxheUtleTtcbiAgICBjb25zdCBkaXNwbGF5V2l0aCA9ICh1c2VyKTogc3RyaW5nIHwgdW5kZWZpbmVkID0+IHtcbiAgICAgIHJldHVybiAodXNlciAmJiB0eXBlb2YgdXNlciA9PT0gJ29iamVjdCcpID8gdXNlcltrZXldIDogdXNlcjtcbiAgICB9XG4gICAgLy8gcmV0dXJuIG9mKGRpc3BsYXlXaXRoKTtcbiAgICB0aGlzLm1hdEF1dG9jb21wbGV0ZURpc3BsYXlGbi5uZXh0KGRpc3BsYXlXaXRoKTtcbiAgICB0aGlzLl9jaGFuZ2VEZXRlY3RvclJlZi5kZXRlY3RDaGFuZ2VzKCk7XG4gIH1cbiAgYXV0b2NvbXBsZXRlQ2xpY2tlZChjb25maWc6IFRhYmxlQ29uZmlnLCBmb3JtR3JvdXA6IEZvcm1BcnJheSkge1xuICAgIHRoaXMuZmlsdGVyZWRPcHRpb25zW2NvbmZpZy5mb3JtQ29udHJvbE5hbWVdID0gW107XG4gICAgdGhpcy5nZW5lcmF0ZUF1dG9jb21wbGV0ZURpc3BsYXlGbihjb25maWcpO1xuICAgIGlmIChmb3JtR3JvdXAuY29udHJvbHNbY29uZmlnLmZvcm1Db250cm9sTmFtZV0udmFsdWUpIHtcbiAgICAgIHRoaXMuc2VhcmNoVmFsdWVDaGFuZ2VzLmVtaXQoeyBrZXk6IGZvcm1Hcm91cC5jb250cm9sc1tjb25maWcuZm9ybUNvbnRyb2xOYW1lXS52YWx1ZSwgY29uZmlnOiBjb25maWcsIHRhYmxlUm93OiBmb3JtR3JvdXAuZ2V0UmF3VmFsdWUoKSB9KTtcbiAgICAgIHRoaXMuZGlydHlBdXRvY29tcGxldGVDb2x1bW4gPSB7IC4uLmNvbmZpZyB9O1xuICAgIH1cbiAgfVxuICB2YWx1ZVNlbGVjdGVkRnJvbUF1dG9jb21wbGV0ZShldmVudDogTWF0QXV0b2NvbXBsZXRlU2VsZWN0ZWRFdmVudCwgcm93SW5kZXgsIHJvd0Zvcm1Hcm91cDogRm9ybUdyb3VwLCBjb25maWc6IFRhYmxlQ29uZmlnKSB7XG4gICAgbGV0IG9wdGlvbiA9IChldmVudCAmJiB0eXBlb2YgZXZlbnQub3B0aW9uLnZhbHVlID09PSAnb2JqZWN0JykgPyB7IC4uLmV2ZW50Lm9wdGlvbi52YWx1ZSB9IDogZXZlbnQub3B0aW9uLnZhbHVlIHx8ICcnO1xuICAgIHRoaXMub3B0aW9uU2VsZWN0ZWQuZW1pdCh7IG9wdGlvbiwgLi4ueyByb3dJbmRleCwgdGFibGVSb3c6IHJvd0Zvcm1Hcm91cC5nZXRSYXdWYWx1ZSgpLCBjb25maWcgfSB9KTtcbiAgfVxuICBzZWxlY3Rpb25DaGFuZ2VkKGV2ZW50KSB7XG5cbiAgfVxuICBjb250ZW50ZWRpdGFibGVWYWx1ZWNoYW5nZSh2YWx1ZSwgcm93Rm9ybUdyb3VwOiBGb3JtR3JvdXAsIGZvcm1Db250cm9sTmFtZSkge1xuICAgIHJvd0Zvcm1Hcm91cC5jb250cm9sc1tmb3JtQ29udHJvbE5hbWVdLnBhdGNoVmFsdWUodmFsdWUpO1xuXG4gIH1cbiAgcHJpdmF0ZSBzZXRFcnJvcih2ZTogRVRWYWxpZGF0aW9uRXJyb3Ipe1xuICAgIGNvbnN0IGZvcm1BcnJheSA9ICggdGhpcy5lZGl0YWJsZVRhYmxlRm9ybS5nZXQoJ3RhYmxlJykgYXMgRm9ybUFycmF5KTtcbiAgICBjb25zdCBmb3JtSW5kZXggPSBmb3JtQXJyYXkuZ2V0UmF3VmFsdWUoKS5maW5kSW5kZXgoKGZvcm1WYWx1ZSk9PntcbiAgICAgIGlmIChmb3JtVmFsdWUudGFibGVSb3dJZCA9PT0gdmUudGFibGVSb3dJZCkge1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICBjb25zdCB0YWJsZVJvd0ZHID0gZm9ybUFycmF5LmF0KGZvcm1JbmRleCk7XG4gICAgaWYgKHRhYmxlUm93RkcpIHtcbiAgICAgIGlmICh2ZS5pc1JlbW92ZSkge1xuICAgICAgICB0YWJsZVJvd0ZHLmdldCh2ZS5mb3JtQ29udHJvbE5hbWUpLnNldEVycm9ycyhudWxsKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgdGFibGVSb3dGRy5nZXQodmUuZm9ybUNvbnRyb2xOYW1lKS5zZXRFcnJvcnMoe1xuICAgICAgICBtc2c6dmUubXNnLFxuICAgICAgICAuLi52ZS50eXBlXG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICAvKiA9PT09PT09PT09PT09PT09ICBOR19WQUxVRV9BQ0NFU1NPUiAmJiBOR19WQUxJREFUT1JTID09PT09PT09PT09PT09PSovXG4gIHB1YmxpYyBvblRvdWNoZWQ6ICgpID0+IHZvaWQgPSAoKSA9PiB7XG4gICAgdGhpcy5lZGl0YWJsZVRhYmxlRm9ybS5tYXJrQWxsQXNUb3VjaGVkKCk7XG4gIH07XG4gIHdyaXRlVmFsdWUodmFsOiBhbnkpOiB2b2lkIHtcbiAgICBpZiAodmFsICYmIHZhbC5yZXNldEZvcm0pIHtcbiAgICAgIHRoaXMuZWRpdGFibGVUYWJsZUZvcm0ucmVzZXQoKTtcbiAgICAgIHRoaXMucmVzZXRUYWJsZSgpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB2YWwgJiYgdmFsLmxlbmd0aCA+IDAgJiYgdGhpcy5jcmVhdGVUYWJsZVdpdGhEYXRhKHZhbCk7XG4gIH1cbiAgcmVnaXN0ZXJPbkNoYW5nZShmbjogYW55KTogdm9pZCB7XG4gICAgZm4gJiYgdGhpcy5lZGl0YWJsZVRhYmxlRm9ybS5jb250cm9scy50YWJsZS52YWx1ZUNoYW5nZXMucGlwZShcbiAgICAgIG1hcCgodmFsdWUpID0+ICh0aGlzLmVkaXRhYmxlVGFibGVGb3JtLmNvbnRyb2xzLnRhYmxlIGFzIEZvcm1BcnJheSkuZ2V0UmF3VmFsdWUoKSlcbiAgICApLnN1YnNjcmliZShmbik7XG4gIH1cbiAgcmVnaXN0ZXJPblRvdWNoZWQoZm46IGFueSk6IHZvaWQge1xuICAgIGZuICYmICh0aGlzLm9uVG91Y2hlZCA9IGZuKTtcbiAgfVxuICBzZXREaXNhYmxlZFN0YXRlPyhpc0Rpc2FibGVkOiBib29sZWFuKTogdm9pZCB7XG4gICAgaXNEaXNhYmxlZCA/IHRoaXMuZWRpdGFibGVUYWJsZUZvcm0uY29udHJvbHMudGFibGUuZGlzYWJsZSgpIDogdGhpcy5lZGl0YWJsZVRhYmxlRm9ybS5jb250cm9scy50YWJsZS5lbmFibGUoKTtcbiAgICAvLyB0aHJvdyBuZXcgRXJyb3IoXCJNZXRob2Qgbm90IGltcGxlbWVudGVkLlwiKTtcbiAgfVxuICB2YWxpZGF0ZShjb250cm9sOiBBYnN0cmFjdENvbnRyb2wpOiBWYWxpZGF0aW9uRXJyb3JzIHtcbiAgICByZXR1cm4gdGhpcy5lZGl0YWJsZVRhYmxlRm9ybS5jb250cm9scy50YWJsZS52YWxpZCA/IG51bGwgOiB7IGludmFsaWRGb3JtOiB7IHZhbGlkOiBmYWxzZSwgbWVzc2FnZTogXCJlZGl0YWJsZVRhYmxlRm9ybSBmaWVsZHMgYXJlIGludmFsaWRcIiB9IH07XG4gIH1cbiAgLyogPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0gKi9cblxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcbiAgICB0aGlzLnN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xuICB9XG59XG4iXX0=